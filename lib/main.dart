import 'package:aquabot/WebServices/models/gateway.dart';

import 'package:aquabot/screens/homescreen/aquabothome.dart';
import 'package:aquabot/screens/homescreen/gateway_details/gateway_details.dart';
import 'package:aquabot/screens/nextaqua_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FirebaseAuth _auth = FirebaseAuth.instance;

    return MaterialApp(
      //  builder: (BuildContext context, Widget child) {
      //   return MediaQuery(
      //       data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      //       child: child);
      // },
      debugShowCheckedModeBanner: false,

      initialRoute: '/', // home
      routes: {
        '/': (context) =>
            _auth.currentUser != null ? AquaBotHome() : NextAquaLogin(),
        AquaBotHome.pageRoute: (context) => AquaBotHome(),
        GatewayDetails.pageRoute: (context) => GatewayDetails(),
      },
    );
  }
}
