import 'dart:io';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/http_server/firebase_utils.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/excel.dart';
import 'package:aquabot/WebServices/models/gateway.dart';
import 'package:aquabot/WebServices/models/live_total_feed.dart';
import 'package:aquabot/screens/homescreen/gateway_details/gateway_details.dart';
import 'package:aquabot/screens/homescreen/search_widget.dart';
import 'package:aquabot/screens/nextaqua_login.dart';
import 'package:aquabot/utils/aqua_utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher.dart';

class AquaBotHome extends StatefulWidget {
  static String pageRoute = "/AquaBotHome";

  @override
  _AquaBotHomeState createState() => _AquaBotHomeState();
}

class _AquaBotHomeState extends State<AquaBotHome> {
  LiveTotalFeed feedDetails = LiveTotalFeed.getInstance();
  var total_count;
  var harvested_count;
  var selectedvalue = 0;
  List searchallGateways;
  String query = "Farm";
  bool searchIsActive = false;
  var _appVersion = 1.4;
  var isLatestVersion = false;
  var isLoading = true;
  bool recievedVersion = false;
  List<Gateway> allGateways;
  List<Gateway> excelData;
  List<Gateway> all_data;
  List<Gateway> harvested_data = [];
   List<Map> total_list = [];
   var total_data;
  @override
  void initState() {
    super.initState();
    
    fetchUserProfile();
    checkAppVersion();
    
  }

  checkAppVersion() async {
    var firebaseAppVersion;
    try {
      var firebaseData = await FirebaseFirestore.instance
          .collection('server')
          .doc('AquaBot')
          .get()
          .then((res) {
        //print(res.data());
        var firebaseResponse = res.data();
        firebaseAppVersion = firebaseResponse['appversion'];
        // setState(() {
        //   isLoading = false;
        // });
      });
    } catch (error) {
      //print(error);
      // setState(() {
      //   isLoading = false;
      // });
    }
    //print(firebaseAppVersion);
    if (_appVersion >= firebaseAppVersion) {
      setState(() {
        isLatestVersion = true;
        recievedVersion = true;
      });
    } else {
      setState(() {
        isLatestVersion = false;
        recievedVersion = true;
      });
    }
  }

  fetchUserProfile() {
    FireBaseUtils fUtils = FireBaseUtils();
    fUtils.fetchUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    AquaUser user = AquaUser.getInstance();
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: [
                  getHeader(),
                  
                  user.phoneNumber == '9985796749' ||
                          user.userName == 'NextAqua'
                      ? Row(
                        children: [
                          getSearchBar(),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.green),
                                        onPressed: () async {
                                          excelData = [];
                                          total_list = [];
                                          total_data = [];
                                          var totalTripsCount = 0;
                                          var tripsTotal = {};
                                          var totalFeed;
                                          var todayFeed;
                                           var weightToDisburse;
              var disburse = [];
              var emptyTrips = [];
                                          excelData =
        user.gatewaysMap.entries.map((entry) => entry.value).toList();
                                          excelData.map((index) {
      if(index.isWifi == true && index.aquaBotList != null && index.aquaBotList.isNotEmpty) {
        index.botDevices.map((element) {
            if (feedDetails.gatewaysFeedDetails[index.gateWayMacId] != null) {
              totalTripsCount = 0;
              totalFeed = 0;
              todayFeed = 0;
              weightToDisburse = [];
              disburse = [];
              emptyTrips = [];
          for (var i = 0;
              i <
                  (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                            ['totalFeed'])
                      .length;
              i++) {
            if (checkDateTodayOrNot(
                    feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                        ['totalFeed'][i]['lut']) ==
                true) {
              if (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                      ['totalFeed'][i]['macID'] ==
                  element.macId) {
                    todayFeed = feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                              ['totalFeed'][i]['todayFeed'];
                    totalFeed = feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                              ['totalFeed'][i]['totalFeed'];
                    
                totalTripsCount = convertTripWiseMapToList(
                        (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                              ['totalFeed'][i]['tripWiseFeedObjs']))
                    .length;
                tripsTotal['${index.gateWayMacId}'] = totalTripsCount;
                weightToDisburse = convertTripWiseMapToList(
                        (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                              ['totalFeed'][i]['tripWiseFeedObjs']));
                               if(weightToDisburse != null || weightToDisburse.isNotEmpty){
                                for(var i = 0; i < weightToDisburse.length; i++){
                                 if(weightToDisburse[i].containsKey('tripFeed')){
                                    if(weightToDisburse[i]['tripFeed'] != 0.00 || weightToDisburse[i]['tripFeed'] != 0){
                                    disburse.add(weightToDisburse[i]['tripFeed']);
                                    }else{
                                      emptyTrips.add(weightToDisburse[i]['tripFeed']);
                                    }
                                  }
                                 }
                               }
                              
              }
            }
          }
        }
          index.liveDeviceMap.forEach((key, value) {
            if(element.macId == value.deviceId) {
              
              total_list.add({
                'FarmName': index.farmName,
                'Name': "${element.deviceName} (${value.deviceId.replaceAll(RegExp(r'[^0-9]'), '')})",
                'gatewayid': index.gateWayMacId,
                'id': value.deviceId,
                'totalTripsCount': totalTripsCount,
                'todayFeed': todayFeed,
                'totalFeed': totalFeed,
                'weightToDisburse': disburse.length,
                'emptyTrips': emptyTrips.length
              });
            }
            
          });
          
        }).toList();
      }
    }).toList();
    total_data = total_list
        .map((index) => [
              // tot_serial = tot_serial + 1,
              index['FarmName'],
              index['gatewayid'],
              index['Name'],
              index['weightToDisburse'],
              index['emptyTrips'],
              index['totalTripsCount'],
              index['todayFeed'],
              // index['totalFeed'],
              
              

              //  "${prepareLutMsg(index['Last Packet'].toInt())} (${DateFormat('d MMM, hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(index['Last Packet'].toInt() * 1000))})",
              //  index['Last Recharge'],
              
            ])
        .toList();

                                          Excel_sheet.createExcel(total_data);
                                        },
                                        child: Text(
                                          "Excel",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'BrandonReg'),
                                        )),
                          ),
                        ],
                      )
                      : SizedBox(),
                      
                  isLatestVersion == true
                      ? searchIsActive == true && searchallGateways.length == 0
                          ? Text("No sites avaliable")
                          : StreamBuilder(
                              stream: homeBloc.homeRefreshStream,
                              builder: (context, AsyncSnapshot snapshot) {
                                //print(snapshot);
                                if (snapshot.hasData) {
                                  bool status = snapshot.data;
                                  return Expanded(
                                      child: Column(
                                    children: [
                                      user.phoneNumber == '9985796749' ||
                                              user.userName == 'NextAqua'
                                          ? Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Row(
                                                  children: [
                                                    SizedBox(
                                                      // height: 20,
                                                      width: 20,
                                                      child: Radio(
                                                          materialTapTargetSize:
                                                              MaterialTapTargetSize
                                                                  .shrinkWrap,
                                                          value: 0,
                                                          groupValue:
                                                              selectedvalue,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              selectedvalue =
                                                                  value;
                                                              //if (searchIsActive == false || query == '') {
                                                              if (selectedvalue ==
                                                                  0) {
                                                                allGateways =
                                                                    all_data;
                                                                searchallGateways =
                                                                    all_data;
                                                              } /*else if (selectedvalue ==
                                                                1) {
                                                              allGateways =
                                                                  offline_data;
                                                              searchallGateways =
                                                                  offline_data;
                                                            }*/
                                                              else if (selectedvalue ==
                                                                  2) {
                                                                allGateways =
                                                                    harvested_data;
                                                                searchallGateways =
                                                                    harvested_data;
                                                              }
                                                            });
                                                          }),
                                                    ),
                                                    Text(" All($total_count)",
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'BrandonReg')),
                                                  ],
                                                ),
                                                //  SizedBox(width: 5),

                                                Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 20,
                                                      child: Radio(
                                                          materialTapTargetSize:
                                                              MaterialTapTargetSize
                                                                  .shrinkWrap,
                                                          value: 2,
                                                          groupValue:
                                                              selectedvalue,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              selectedvalue =
                                                                  value;
                                                              if (selectedvalue ==
                                                                  0) {
                                                                allGateways =
                                                                    all_data;
                                                                searchallGateways =
                                                                    all_data;
                                                              } /*else if (selectedvalue ==
                                                                1) {
                                                              allGateways =
                                                                  offline_data;
                                                              searchallGateways =
                                                                  offline_data;
                                                            }*/
                                                              else if (selectedvalue ==
                                                                  2) {
                                                                allGateways =
                                                                    harvested_data;
                                                                searchallGateways =
                                                                    harvested_data;
                                                              }
                                                            });
                                                          }),
                                                    ),
                                                    Text(
                                                        " Harvested($harvested_count)",
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'BrandonReg')),
                                                  ],
                                                ),
                                                //SizedBox(width: 5),
                                              ],
                                            )
                                          : SizedBox(),
                                      getListView(),
                                    ],
                                  ));
                                } else if (snapshot.hasError) {
                                  //print(snapshot.error);
                                  return Text(snapshot.error);
                                }
                                return Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.80,
                                    child: Center(
                                        child: SpinKitCubeGrid(
                                      size: 51.0,
                                      color: Color(0xFF0170d0),
                                    )));
                              })

                      /* StreamBuilder(
                      stream: homeBloc.homeRefreshStream,
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          bool status = snapshot.data;
                          return getListView();
                        } else if (snapshot.hasError) {
                          // //print(snapshot.error);
                          return Text(snapshot.error);
                        }
                        return Container(
                            alignment: Alignment.center,
                            child: Center(
                                child: SpinKitCubeGrid(
                              size: 51.0,
                              color: Color(0xFF0170d0),
                            )));
                      })*/
                      : Container(
                          height: MediaQuery.of(context).size.height * 0.80,
                          child: Center(
                              child: recievedVersion == true
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text("Deprecated Version!"),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text("Please update App to "),
                                            InkWell(
                                              child: Text("latest version",
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      fontFamily: 'Brandon',
                                                      decoration: TextDecoration
                                                          .underline,
                                                      color: Colors.blue)),
                                              onTap: () {
                                                redirectToAppstore();
                                              },
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  : SpinKitCubeGrid(
                                      size: 51.0,
                                      color: Color(0xFF0170d0),
                                    )),
                        )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  redirectToAppstore() async {
    var url;
    if (Platform.isAndroid) {
      url =
          'https://play.google.com/store/apps/details?id=com.nextaqua.aquabot';
    } else {
      url = 'https://apps.apple.com/tc/app/aquabot-1-0/id1563036495';
    }
    if (!await launch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch';
    }
  }

  Widget getSearchBar() => SearchWidget(
        hintText: 'Search Farms',
        text: query,
        onChanged: searchFarm,
      );

  void searchFarm(String query) {
    AquaUser user = AquaUser.getInstance();
    if (selectedvalue == 0) {
      setState(() {
        searchallGateways = all_data;
        searchIsActive = true;
      });
    } /*else if (selectedvalue == 1) {
      setState(() {
        searchallGateways = offline_data;
        searchIsActive = true;
      });
    }*/
    else if (selectedvalue == 2) {
      setState(() {
        searchallGateways = harvested_data;
        searchIsActive = true;
      });
    }
    // setState(() {
    //   searchallGateways =
    //       user.gatewaysMap.entries.map((entry) => entry.value).toList();
    //   searchIsActive = true;
    // });
    final farms = searchallGateways.where((farm) {
      final farmNameLower = farm.farmName.toLowerCase();
      final searchLower = query.toLowerCase();
      return farmNameLower.contains(searchLower);
    }).toList();
    setState(() {
      searchallGateways = farms;
      query = query;
    });
    ////print(searchallGateways[0].farmName);
  }

  Widget getHeader() {
    AquaUser user = AquaUser.getInstance();

    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          SizedBox(
            height: 44,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 40,
                    child: Row(children: [
                      SizedBox(width: 10),
                      Text(
                        "NEXT",
                        style: TextStyle(
                            color: Color.fromRGBO(45, 45, 45, 1),
                            fontSize: 30,
                            fontFamily: 'BrandonReg',
                            fontWeight: FontWeight.bold),
                      ),
                      Text("AQUA",
                          style: TextStyle(
                              color: Color(0xFF0170d0),
                              fontSize: 30,
                              fontFamily: 'BrandonReg',
                              fontWeight: FontWeight.bold)),
                      SizedBox(
                          width: 30,
                          height: 35,
                          child: Image.asset('images/app_icon-trns.png')),
                    ]),
                  ),
                  PopupMenuButton(
                    padding: EdgeInsets.only(top: 8, bottom: 8),
                    child: Center(child: Icon(Icons.more_vert)),
                    itemBuilder: (context) {
                      return List.generate(1, (index) {
                        return PopupMenuItem(
                          child: Column(
                            children: [
                              Container(
                                  alignment: Alignment.center,
                                  child: Column(children: [
                                    Text(
                                      "${user.userName}",
                                      style:
                                          TextStyle(fontFamily: 'BrandonReg'),
                                    ),
                                    Text(
                                      "${user.phoneNumber}",
                                      style:
                                          TextStyle(fontFamily: 'BrandonReg'),
                                    )
                                  ])),
                              Divider(
                                thickness: 2,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return logoutalert();
                                        });
                                    //  logoutClicked();
                                  },
                                  child: Text(
                                    'Logout',
                                    style: TextStyle(fontFamily: 'BrandonReg'),
                                  )),
                              Divider(
                                thickness: 2,
                              ),
                              Text(
                                "V-1.5(3)",
                                style: TextStyle(fontFamily: 'BrandonReg'),
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              GestureDetector(
                                  onTap: () {
                                    Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              super.widget),
                                    );
                                    /* Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AquaBotHome()),
                                        (Route<dynamic> route) => false);*/
                                    // setState(() {});
                                    //  homeBloc.refreshHomeList();
                                  },
                                  child: Text(
                                    "Refresh",
                                    style: TextStyle(fontFamily: 'BrandonReg'),
                                  )),
                            ],
                          ),
                          /* child: GestureDetector(
                              onTap: () {
                                logoutClicked();
                              },
                              child: Text('Logout')),*/
                        );
                      });
                    },
                  ),
                ]),
          ),
          Divider()
        ]);
  }

  logoutClicked() async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    await _auth.signOut();
    AquaUser user = AquaUser.getInstance();
    user.releseAllData();
    Navigator.pushNamed(context, NextAquaLogin.pageRoute, arguments: null);
  }

  convertTripWiseMapToList(data) {
    var list = [];
    if (data != null) {
      data.forEach((ele, val) => list.add(val));
    }
    return list;
  }

  checkDateTodayOrNot(date) {
    if (date != null) {
      var recievedDate = DateTime.fromMillisecondsSinceEpoch(date * 1000).day;
      var recievedMonth =
          DateTime.fromMillisecondsSinceEpoch(date * 1000).month;
      var recievedYear = DateTime.fromMillisecondsSinceEpoch(date * 1000).year;
      var currentDate = DateTime.now().day;
      var currentMonth = DateTime.now().month;
      var currentYear = DateTime.now().year;
      if ((currentDate == recievedDate) &&
          (currentMonth == recievedMonth) &&
          (currentYear == recievedYear)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Widget getListView() {
    AquaUser user = AquaUser.getInstance();
    FireBaseUtils fUtils = FireBaseUtils();
    var total_devices = 0;
    var live_devices = 0;
    var harvested_devices = 0;
    var tripsTotal = {};
    all_data = user.gatewaysMap.entries.map((entry) => entry.value).toList();
    all_data.map((index) {
      var totalTripsCount = 0;
      // if () {
      //  total_gateways = total_gateways + 1;
      index.botDevices.map((element) {
        // index.liveDeviceMap.forEach((key1, value) {
        // if (element.macId == value.deviceId) {
        if (feedDetails.gatewaysFeedDetails[index.gateWayMacId] != null) {
          for (var i = 0;
              i <
                  (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                          ['totalFeed'])
                      .length;
              i++) {
            if (checkDateTodayOrNot(
                    feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                        ['totalFeed'][i]['lut']) ==
                true) {
              if (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                      ['totalFeed'][i]['macID'] ==
                  element.macId) {
                totalTripsCount += convertTripWiseMapToList(
                        (feedDetails.gatewaysFeedDetails[index.gateWayMacId]
                            ['totalFeed'][i]['tripWiseFeedObjs']))
                    .length;
                tripsTotal['${index.gateWayMacId}'] = totalTripsCount;
              }
            }
          }
        }
        total_devices = total_devices + 1;
        // bool isoffline = checkOffline(value.epochTime);
        if (element.isHarvested == false) {
          // if (isoffline == true) {
          //   /*  //print(offline_data);
          //   offline_data.isNotEmpty
          //       ? offline_data.removeWhere(
          //           (i) => i.gateWayMacId == index.gateWayMacId)
          //       : "";
          //   offline_data.add(index);
          //   //print(offline_data);*/
          // } else {
          //   live_devices = live_devices + 1;
          //   live_data.isNotEmpty
          //       ? live_data.removeWhere(
          //           (i) => i.gateWayMacId == index.gateWayMacId)
          //       : "";

          //   live_data.add(index);
          // }
        } else {
          harvested_devices = harvested_devices + 1;
          harvested_data.isNotEmpty
              ? harvested_data
                  .removeWhere((i) => i.gateWayMacId == index.gateWayMacId)
              : "";

          harvested_data.add(index);
        }
        // }
      }).toList();
      // }).toList();
      // }
    }).toList();
    total_count = total_devices;
    harvested_count = harvested_devices;
    if (searchIsActive == false) {
      if (selectedvalue == 0) {
        allGateways = all_data;
      } /*else if (selectedvalue == 1) {
        allGateways = offline_data;
      }*/
      else if (selectedvalue == 2) {
        allGateways = harvested_data;
      }
      /* allGateways =
          user.linemon_gatewaysMap.entries.map((entry) => entry.value).toList();*/
      //print(allGateways);
    } else if (searchIsActive == true && query == '') {
      if (selectedvalue == 0) {
        allGateways = all_data;
      } /*else if (selectedvalue == 1) {
        allGateways = offline_data;
      }*/
      else if (selectedvalue == 2) {
        allGateways = harvested_data;
      }
    } else {
      allGateways = searchallGateways;
    }
    allGateways
      ..sort((productA, productB) =>
          productA.farmName.compareTo(productB.farmName));

    /*  List<Gateway> allGateways =
        user.gatewaysMap.entries.map((entry) => entry.value).toList();*/
        
    var listview = ListView.builder(
      physics: AlwaysScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: allGateways.length,
      itemBuilder: (BuildContext context, int index) {
        if (allGateways[index].aquaBotList.isNotEmpty &&
            allGateways[index].aquaBotList != null) {
          return GestureDetector(
            onTap: () {
              //   //print("This is length ${allGateways[index].aquaBotList.length}");

              gatewayClicked(allGateways[index]);
            },
            child: Container(
              height: 100,
              child: Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Container(
                  /* decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.black,
                  ),*/
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                    /*   boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 7,
                          // offset: Offset(0, 3))
                        )
                      ] */ /*BorderRadius.all(Radius.circular(
                            8.0) */ //                 <--- border radius here
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 8, right: 8),
                        decoration: BoxDecoration(
                          color: AquaUtils.getColorForIndex(index),

                          // border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                        ),
                        //  color: AquaUtils.getColorForIndex(index),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              /*  SizedBox(
                                width: 8,
                              ),*/
                              SizedBox(
                                  height: 30,
                                  child: Center(
                                      child: Text(
                                    allGateways[index].farmName,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'BrandonReg'),
                                  ))),
                              Text(
                                "Total : ${allGateways[index].aquaBotList.length}",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'BrandonReg'),
                              )
                            ]),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                        child: Text("Aquabots Parked", style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'BrandonReg')),
                      )
                      // Container(
                      //     margin: EdgeInsets.only(left: 10, right: 10, top: 7),
                      //     padding: EdgeInsets.all(8),
                      //     child: Column(
                      //       children: [
                      //         Row(
                      //           mainAxisAlignment:
                      //               MainAxisAlignment.spaceBetween,
                      //           children: [
                      //             Text(
                      //                 tripsTotal['${allGateways[index].gateWayMacId}'] !=
                      //                         null
                      //                     ? "Trips: ${tripsTotal['${allGateways[index].gateWayMacId}']}"
                      //                     : "Trips: 0",
                      //                 style: TextStyle(
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.bold,
                      //                     fontFamily: 'BrandonReg')),
                      //             Text(
                      //                 feedDetails.gatewaysFeedDetails[
                      //                             '${allGateways[index].gateWayMacId}'] !=
                      //                         null
                      //                     ? "Feed: ${feedDetails.gatewaysFeedDetails['${allGateways[index].gateWayMacId}']['todayFeed'].toStringAsFixed(2)} kg"
                      //                     : "Feed: 0.0 kg",
                      //                 style: TextStyle(
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.bold,
                      //                     fontFamily: 'BrandonReg')),
                      //           ],
                      //         ),

                      //         /*Text('GatewayId : ' +
                      //             allGateways[index].gateWayMacId)*/
                      //       ],
                      //     ))
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          return SizedBox(
            height: 0,
          );
        }
      },
    );

    return Expanded(child: listview);
  }

  gatewayClicked(Gateway selGateway) {
    AquaUser user = AquaUser.getInstance();
    user.selGateway = selGateway;
    Navigator.pushNamed(context, GatewayDetails.pageRoute, arguments: null);
  }

  Widget logoutalert() {
    return Container(
      child: Center(
          child: AlertDialog(
        title: Text("Are you sure, you want to Logout ?",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                fontFamily: 'BrandonReg')),
        content: SizedBox(
          width: 100,
          child: Container(
            height: 35,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        child: Text("Cancel",
                            style: TextStyle(fontFamily: 'BrandonReg')),
                        onTap: () {
                          Navigator.pop(context, false);
                        }),
                    GestureDetector(
                      child: Text("Ok",
                          style: TextStyle(fontFamily: 'BrandonReg')),
                      onTap: () {
                        logoutClicked();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
