import 'dart:async';
import 'dart:typed_data';

import 'dart:ui';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/http_server/live_map_server.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:aquabot/WebServices/models/tripinfo_map_getx.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/mapconfig/mapconfig.dart';
import 'package:aquabot/utils/aqua_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class TripMapView extends StatefulWidget {
  // const MapView({Key key}) : super(key: key);
  // final BotModel selDevice;
  final int index;
  final tripid;
  var controller = Get.put(Trip_Map());

  TripMapView({this.index, this.tripid});

  @override
  _TripMapViewState createState() => _TripMapViewState();
}

class _TripMapViewState extends State<TripMapView> {
  LatLng _initialcameraposition;
  GoogleMapController _controller;
  Location _location = Location();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};

  // Set<Polyline> _polyline = {};
  // Set<Polyline> _livePolyline = {};
  AquaUser user = AquaUser.getInstance();
  // var device_Id;

  //Validations
  int botCount = 0;
  int postCount = 0;
  int lineCount = 0;
  void dispose() {
    try {
      _controller?.dispose();
    } catch (err) {
      //  //print(err);
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    widget.controller.clearAll();
    updateLocation();
    addMarkers();
    _addPolyline();
    _addLivePolyline();

    // homeBloc.refreshBotDetailsView();
    // device_Id = user.selGateway.botDevices[widget.index].macId;
  }

  void addMarkers() async {
    //  //print("This is tripid in map screen : ${widget.tripid}");
    //Polylines
    // _addPolyline();

    //line names
    // lineCount = botCount = user.selGateway.botDevices.length;
    /*  user.selGateway.botDevices.forEach((element) {
      _addNameMarkers(element.routePoints[0], element);
    });*/

    var bot = user.selGateway.botDevices[widget.index];
    ////print(data[0]);
    _addNameMarkers(bot);

    //Markers
    //  user.selGateway.botDevices.forEach((device) {
    int j = 0;
    user.selGateway.botDevices[widget.index].routePoints.forEach((bMarker) {
      //  //print(device.routePoints);

      //  postCount = postCount + 1;
      // BotMarker nextMarker = null;
      // if (device.routePoints.length > j + 1) {
      //   nextMarker = device.routePoints[j];
      // }
      _addMarkers(bMarker, j);
      j = j + 1;
    });
  }

  void updateLocation() {
    if (widget.index != null) {
      double lat = user.selGateway.botDevices[widget.index].lat;
      double long = user.selGateway.botDevices[widget.index].long;
      _initialcameraposition = LatLng(lat, long);

      if (_controller != null) {
        _controller.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: _initialcameraposition, zoom: 18),
        ));
      }
    }
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    /* _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
        ),
      );
    });*/
  }

  @override
  Widget build(BuildContext context) {
    // widget.controller.clearAll();

    // updateLocation();
    return Container(
        color: Colors.white,
        child: GetX<Trip_Map>(builder: (snapshot) => getMapView(snapshot)));
  }

  /* Widget getMapContainerView() {
    Widget mapWid = /*StreamBuilder(
        stream: homeBloc.botDetailsRefreshStream,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            bool status = snapshot.data;
            return getMapView();
          }
          return getMapView();
        });
    return mapWid;*/
  }*/

  Widget getMapView(snapshot) {
    //  _addLivePolyline();
    Widget mapWid = GoogleMap(
      initialCameraPosition:
          CameraPosition(target: _initialcameraposition, zoom: 18),
      gestureRecognizers: Set()
        ..add(Factory<OneSequenceGestureRecognizer>(
            () => new EagerGestureRecognizer()))
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      mapType: MapType.satellite,
      zoomControlsEnabled: false,
      mapToolbarEnabled: false,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      markers: Set<Marker>.of(snapshot.markers.values),
      polylines: Set<Polyline>.of(snapshot.polylines.values),
      onLongPress: (latlang) {
        // _addMarkerLongPressed(latlang);
        /*   showBarModalBottomSheet(
          barrierColor: Color.fromRGBO(0, 0, 0, 0.3),
          context: context,
          builder: (context) => Container(
            height: 400,
            child: MapConfig(
              selDevice: widget.selDevice,
              selLatLong: latlang,
            ),
          ),
        );*/
        //we will call this function when pressed on the map
      },
    );
    // //print(_livePolyline);
    // //print(_polyline);
    return mapWid;
  }

  Future _addMarkers(BotMarker botmarker, int title) async {
    final MarkerId markerId = MarkerId(AquaUtils().getRandomString(5));
    LatLng ll = botmarker.latlang;
    // //print(botmarker.latlang);

    /* if (nextBotmarker != null) {
      ll = AquaUtils()
          .computeCentroid([botmarker.latlang, nextBotmarker.latlang]);
    }*/
    final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/grey_home.png', 45);

    BitmapDescriptor bitmapDescriptor = title == 0
        ? BitmapDescriptor.fromBytes(markerIcon)
        : await AquaUtils().getClusterMarker(
            title.toString(), Color(0xFFBDBDBD), 20, Color(0xFFBDBDBD));
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      visible: true,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Marker here",
        snippet: 'This looks good',
      ),
      icon: bitmapDescriptor,
    );
    widget.controller.addmarkers(markerId, marker);
    //  markers[markerId] = marker;
    // setState(() {});
  }

  Future _addNameMarkers(BotModel element) async {
    final MarkerId markerId = MarkerId(AquaUtils().getRandomString(5));
    double lat = element.lat;
    double long = element.long;
    LatLng ll = LatLng(lat, long);

    /* if (element.routePoints.length >= 2) {
      ll = AquaUtils().computeCentroid(
          [element.routePoints[0].latlang, element.routePoints.last.latlang]);*/

    BitmapDescriptor bitmapDescriptor = await AquaUtils().getClusterMarkerName(
      element.deviceName,
      Colors.white,
      40,
    );
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Marker here",
        snippet: 'This ssss good',
      ),
      icon: bitmapDescriptor,
    );
    widget.controller.addmarkers(markerId, marker);
    // markers[markerId] = marker;
    /* if (markers.length == (postCount + botCount)) {
      setState(() {});
    }*/
    //  setState(() {});
  }

  _addPolyline() {
    //  _polyline = {};
    //Polylines
    //  final PolylineId polylineId = PolylineId(AquaUtils().getRandomString(5));

    int i = 0;
    //  user.selGateway.botDevices.forEach((element) {
    List<LatLng> points = [];
    user.selGateway.botDevices[widget.index].routePoints.forEach((marker) {
      //  //print(marker.latlang);
      points.add(marker.latlang);
    });
    //  //print(points);

    // if (element.afType != 'Line' && element.routePoints.length > 0) {
    // //print(element.routePoints[0].latlang);
    points.add(points[0]);
    //  }
    //   //print(points);
    final PolylineId polylineId = PolylineId(AquaUtils().getRandomString(5));

    var polyline = Polyline(
      polylineId: polylineId,
      // PolylineId(AquaUtils().getRandomString(5)),
      color: Color(0xFFBDBDBD),
      points: points,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      visible: true,
      width: 3,
      patterns: [PatternItem.dot, PatternItem.gap(3)],
    );
    i = i + 1;
    widget.controller.addpolyline(polylineId, polyline);
    //  _polyline.add(polyline);
    //  polylines[polylineId] = polyline;

    // //print(points);
  }

  _addLivePolyline() {
    var device_Id = user.selGateway.botDevices[widget.index].macId;

    int i = 0;
    preparePath(data) {
      List<LatLng> paths_array = [];

      if (data["hits"].length > 0) {
        List<LatLng> pathsArr = [];
        for (var i = 0; i < data["hits"].length - 1; i++) {
          var val = data["hits"][i]['_source'];

          var path = {
            'latitude': val['gpsLat1'],
            'longitude': val['gpsLong1'],
          };
          pathsArr.add(LatLng(
              double.parse(path['latitude']), double.parse(path['longitude'])));
        }

        paths_array.addAll(pathsArr);
      }
      return paths_array;
    }

    Live_server server = Live_server();
    // List<LatLng> paths_array = [];
    //  //print(device_Id);

    user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
      if (device_Id == element.deviceId) {
        List<LatLng> points = [];

        var server_data = await server.fetch_live_map(
            widget.tripid, element.deviceId, element.siteId);
        //print("Server data : $server_data");

        List<LatLng> new_points = preparePath(server_data);
        if (new_points.length > 0) {
          points.addAll(new_points);
        }
        List<LatLng> paths = [];

        if (server_data["hits"].length > 0) {
          //  List<LatLng> pathsArr = [];
          for (var i = 0; i < server_data["hits"].length - 1; i++) {
            List<LatLng> pathsArr = [];

            var val = server_data["hits"][i]['_source'];
            var path = {
              'latitude': val['gpsLat1'],
              'longitude': val['gpsLong1'],
            };
            var spin = server_data["hits"][i]['_source']['spinCounter'];
            LatLng last = paths.isNotEmpty ? paths.removeLast() : null;

            paths.add(LatLng(double.parse(path['latitude']),
                double.parse(path['longitude'])));
            last != null ? pathsArr.add(last) : [];

            pathsArr.add(LatLng(double.parse(path['latitude']),
                double.parse(path['longitude'])));
            //print(spin);
            print(pathsArr);

            final PolylineId polylineId =
                PolylineId(AquaUtils().getRandomString(5));

            var polyline = Polyline(
              polylineId: polylineId,
              //PolylineId(AquaUtils().getRandomString(5)),
              color: spin > 0 ? Colors.blue : Color(0xFFFFC107),
              points: pathsArr,
              visible: true,
              startCap: Cap.roundCap,
              endCap: Cap.roundCap,
              width: 3,
              patterns: [PatternItem.dot, PatternItem.gap(3)],
            );
            i = i + 1;
            widget.controller.addpolyline(polylineId, polyline);
            // _livePolyline.add(polyline);
            //  polylines[polylineId] = polyline;
          }
        }
      }
    });
    // setState(() {});
  }
}
