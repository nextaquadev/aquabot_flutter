import 'dart:convert';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/map_gateway.dart';
import 'package:aquabot/WebServices/models/route_create_controller.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/caommand_alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/multialert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:math';

class MapConfig extends StatefulWidget {
  final BotModel selDevice;
  final LatLng selLatLong;
  final allpoints;
  var route_controller = Get.find<Route_Controller>();
  MapConfig({this.selDevice, this.selLatLong, this.allpoints});
  @override
  _MapConfigState createState() => _MapConfigState();
}

class _MapConfigState extends State<MapConfig> {
  var deviceId;
  AquaUser user = AquaUser.getInstance();
  var postPoints = {};
  var displayToast = false;
  var toastColor = Colors.green;
  var toastName = "";
  String _chosenValue = 'Home';
  List routePositions;
  List linePositions;
  var enteredApi;
  var enteredData;
  var data;
  var selectedposition = 0;
  var selectedPost;
  var selectedlat;
  var selectedlong;
  var isloading = false;
  var initialRegion;
  var siteData;
  var loading = false;
  var takemetherespped = 8000;
  var chooedlatlohg = '0';
  var isSwitched = true;
  var touchedLat = 0;
  var touchedLong = 0;
  var currentDevicelat = 0;
  var currentDevicelohg = 0;
  bool recievedData = true;
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  var pointscontroller = TextEditingController();
  @override
  void initState() {
    super.initState();
    // setState(() {
    deviceId = widget.selDevice.macId;
    // });
    widget.allpoints.forEach((key, value) {
      var lat = double.parse(value['m_lat']);
      var lng = double.parse(value['m_lng']);
      if (lat != 0.0 && lng != 0.0) {
        postPoints[key] = {"lat": lat, "lng": lng};
      }
    });
    //   print(postPoints);
    //  postPoints = widget.selDevice.regions["$deviceId"];
    print(postPoints);

    routePositions = [
      "Home",
      "Label",
      "Post-1",
      "Post-2",
      "Post-3",
      "Post-4",
      "Post-5",
      "Post-6",
      "Post-7",
      "Post-8",
      "Post-9",
      "Post-10",
      "Post-11",
      "Post-12",
      "Post-13",
      "Post-14",
      "Post-15",
      "Post-16",
      "Post-17",
      "Post-18",
      "Post-19",
      "Post-20",
      "Post-21",
      "Post-22",
      "Post-23",
      "Post-24",
      "Post-25",
      "Post-26",
      "Post-27",
      "Post-28",
      "Post-29",
      "Post-30",
      "Post-31",
      "Post-32",
      "Post-33",
      "Post-34",
      "Post-35",
      "Post-36",
      "Post-37",
      "Post-38",
      "Post-39",
      "Post-40",
      "Post-41",
      "Post-42",
      "Post-43",
      "Post-44",
      "Post-45",
      "Post-46",
      "Post-47",
      "Post-48",
      "Post-49",
      "Post-50",
    ];
    linePositions = [
      "Home",
      "Label",
      "Post-1",
      "Post-2",
    ];
    // homeBloc.refreshCurrentLocation();
  }

/*  sendCommandDirectlyToDeviceHome(command, methodName) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/$deviceId/rpc/setPost?access_token=LRoyC90hJjxuoklY7rPAH5Q');
    setState(() {
      enteredApi = path;
    });
    //print(enteredApi);
    //print(command);
    var encodedCommand = jsonEncode(command);
    //print(encodedCommand);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    var convertedData = jsonDecode(response.body);
    //print(convertedData);
    //print(response.statusCode);
    return response;
  }

  sendCommandDirectlyToDevice(command, methodName) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/device452/rpc/setPost?access_token=LRoyC90hJjxuoklY7rPAH5Q');
    setState(() {
      enteredApi = path;
    });
    //print(enteredApi);
    //print(command);
    var encodedCommand = jsonEncode(command);
    //print(encodedCommand);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    var convertedData = jsonDecode(response.body);
    //print(convertedData);
    //print(response.statusCode);
    return response;
  }*/

  saveDetailsToRedux(prevData) {
    var sites = [];
    var gatewayMacId = prevData['gateWayMacId'];
    var farmName = prevData['farmName'];
    var farmerSiteId = prevData['farmerSiteId'];
    var layoutJson = [];
    var starterConnectedDeviceList = [];
    var starterList = [];
    // var gateway = Gateway(
    //     gatewayMacId,
    //     farmName,
    //     [],
    //     [],
    //     starterList,
    //     farmerSiteId,
    //     layoutJson,
    //     prevData['siteName'],
    //     prevData['aquaBotList'],
    //     prevData['feeds'],
    // );

    // var data = siteData.siteData;
    // for (var index = 0; index < data.length; index++) {
    //     var findIn = data[index].gateways.findIndex((e){ return e.gatewayMacId == prevData['gateWayMacId'];
    //      });
    //     // if (findIn != -1) {
    //     //     data[index].gateways[findIn] = gateway;
    //     // }

    // }
    // dispatch(saveSectionDetails(gateway));

    // dispatch(updateGatwayLiveData(undefined, data[0]));
  }

  convertObjectToarray(aquabotlist) {
    //print(aquabotlist);
    List aquaList = [];
    if (aquabotlist != null) {
      var keys = aquabotlist.keys.toList();
      if (keys.length > 0) {
        keys.forEach((ele) {
          var obj = aquabotlist[ele];
          aquaList.add(obj);
        });
        return aquaList;
      } else {
        return aquaList;
      }
    } else {
      return aquaList;
    }
  }

  filterRegions(botLists) {
    //print(botLists);
    var deviceid = botLists['macId'];
    var regions = botLists['regions'];
    //print("This is regions:$regions");
    var botList = regions['${botLists['macId']}'];
    //print(botList);
    var keys = botList.keys;
    if (keys.length > 0) {
      for (var i = 0; i < keys.length; i++) {
        //print(regions['$deviceid']['$i']['latitude']);
        if (regions['$deviceid']['$i']['latitude'] == 0 &&
            regions['$deviceid']['$i']['longitude'] == 0) {
          regions['$deviceid'].remove('$i');
        }
      }
    }
    return regions;
  }

  /* removeZeroRegions(region) {
    var regions = region;
    //print(regions);
    var keys = region.keys;
    if (keys.length > 0) {
      for (var i = 0; i < keys.length; i++) {
        //var key = keys[i];
        if (regions['$i'].keys.contains('latitude')) {
          if (regions['$i']['latitude'] == 0.0 &&
              regions['$i']['longitude'] == 0.0) {
            regions.remove('$i');
          }
        }
      }
    }
    return regions;
  }*/

  getRegionForCoordinates(points) {
    if (points != null) {
      var cordArr = [];
      var keys = points.keys;
      for (var i = 0; i < keys.length; i++) {
        //var indx = keys[i];
        cordArr.add(points['$i']);
      }

      // points should be an array of { latitude: X, longitude: Y }
      double minX, maxX, minY, maxY;

      // init first point
      ((point) => {
            minX = point['latitude'],
            maxX = point['latitude'],
            minY = point['longitude'],
            maxY = point['longitude']
          })(cordArr[0]);

      // calculate rect
      cordArr.map((point) => {
            minX = min(minX, point['latitude']),
            maxX = max(maxX, point['latitude']),
            minY = min(minY, point['longitude']),
            maxY = max(maxY, point['longitude'])
          });
      var midX = (minX + maxX) / 2;
      var midY = (minY + maxY) / 2;
      var deltaX = (maxX - minX);
      var deltaY = (maxY - minY);

      return {
        'latitude': midX,
        'longitude': midY,
        'latitudeDelta': 0.0001, //deltaX
        'longitudeDelta': 0.0020 //deltaY
      };
    } else {
      return {
        'latitude': 17.4027,
        'longitude': 78.4525,
        'latitudeDelta': 0.0922, //deltaX
        'longitudeDelta': 0.0421 //deltaY
      };
    }
  }

  preparePondsPositionCordsofDevice(points) {
    if (points != null) {
      var cordArr = [];
      var temp = {};
      var keys = points.keys;
      for (var i = 0; i < keys.length; i++) {
        //var indx = keys[i];
        if (i == 0) {
          temp = points['$i'];
        }
        cordArr.add(points['$i']);
      }
      cordArr.add(temp);
      return cordArr;
    } else {
      return [];
    }
  }

  /*takeMeThereBtnHandler() async {
    var selectedMacId = widget.selDevice.macId;
    user.selGateway.botDevices.forEach((index) {
      if (widget.selDevice.macId == index.macId) {
        setState(() {
          data = index;
        });
      }
    });
    setState(() {
      selectedlat = widget.selLatLong.latitude;
      selectedlong = widget.selLatLong.longitude;
      deviceId = data.macId;
      //data = widget.selDevice;
      isloading = true;
    });
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;

    Map config = {
      'frametype': "takeMeThere",
    };
    //prepare control command data object
    Map configCmd = {
      'version': version,
      'deviceId': selectedMacId,
      'm_lat': selectedlat,
      'm_lng': selectedlong,
      'speed': takemetherespped,
    };
    //print("dghagsg");
    //print(configCmd);
    config['commands'] = [configCmd];
    // //print('take me there request object' + jsonDecode(config.toString()));
    try {
      var responseData =
          await sendCommandDirectlyToDevice(configCmd, 'takeMeThere');
      ////print(responseData.body);
      //print('takeMeThere');
      setState(() {
        isloading = false;
        selectedlong = 0;
        selectedlat = 0;
        takemetherespped = 8000;
        chooedlatlohg = "0";
        selectedposition = 0;

        displayToast = true;
        toastColor = Colors.green;
        toastName = "Success";
      });
    } catch (error) {
      //print(error);
      setState(() {
        isloading = false;
        selectedlat = 0.0;
        selectedlong = 0.0;
        takemetherespped = 8000;
        setState(() {
          displayToast = true;
          toastColor = Colors.red;
          toastName = "Failed";
        });
      });
    }
  }*/

  /* saveRegionsToFirebase(postpositions) async {
    var selectedMacId = widget.selDevice.macId;
    var gatewayMacId = user.selGateway.gateWayMacId;
    var positions = postpositions['m_arrLocations'];
    var deviceid = widget.selDevice.macId;
    var deviceType;
    user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
      if (deviceid == element.deviceId) {
        deviceType = element.deviceType;
      }
    });
    //prepare json for configuration,
    var selectedPost = selectedposition;
    var post = {'latitude': selectedlat, 'longitude': selectedlong};
    var details = {};
    details[selectedPost] = post;
    //print("post savePostDetailsToFirebase");
    //print(post);
    //print("post savePostDetailsToFirebase");
    try {
      DocumentReference firestoreData = await FirebaseFirestore.instance
          .collection("gateways")
          .doc("${gatewayMacId}")
          .collection("info")
          .doc("profiledata");
      firestoreData.get().then((DocumentSnapshot documentSnapshot) async {
        if (documentSnapshot.exists) {
          //print(documentSnapshot.data());
          var previousData = documentSnapshot.data() as Map;
          var aquaBotList = previousData['aquaBotList'];
          var regions = {selectedMacId: {}};
          var rgons = [];
          positions.forEach((ele, i) {
            if (deviceType != "Line") {
              var j = i.toString();
              var obj = {
                ["$j"]: {'latitude': ele.m_lat, 'longitude': ele.m_lng}
              };
              // Object.assign(regions[data.selectedDevice.deviceId], obj)
            } else {
              if (i < 3) {
                var j = i.toString();
                var obj = {
                  ["$j"]: {'latitude': ele.m_lat, 'longitude': ele.m_lng}
                };
                // Object.assign(regions[data.selectedDevice.deviceId], obj)
              }
            }
          });
          var finalInitialRegion;
          var pondsPaths = [];
          var pondsInfoarr = [];
          var allRegions = {};
          aquaBotList[selectedMacId]['regions'] = regions;
          // Map botLists = new Map.from(aquaBotList);
          var botLists = jsonDecode(jsonEncode(aquaBotList));
          // cloneDeep(aquaBotList);
          var intitalregions = initialRegion;
          var Deviceregions = data['allRegions'];
          var activeDevices = convertObjectToarray(botLists);
          for (var i = 0; i < activeDevices.length; i++) {
            var device = activeDevices[i].macId;
            // activeDevicesList[device]=deviceList[device];
            var filterRegion = filterRegions(activeDevices[i]);
            //Object.assign(allRegions, filterRegion);
            var inRegion = removeZeroRegions(activeDevices[i].regions[device]);
            var initialRegionCords = inRegion;
            if (i == 0) {
              finalInitialRegion = getRegionForCoordinates(initialRegionCords);
            }
            var postpath =
                preparePondsPositionCordsofDevice(initialRegionCords);
            var obj = {'deviceId': device, 'paths': postpath};
            pondsPaths.add(obj);
            // let pond=pondsDetails[device];
            if (activeDevices[i]) {
              var obj = {
                'lat': activeDevices[i].lat ? activeDevices[i].lat : 0.0,
                'long': activeDevices[i].long ? activeDevices[i].long : 0.0,
                'name': activeDevices[i].deviceName
              };
              pondsInfoarr.add(obj);
            }
          }
          var payload = {
            'initialRegion': finalInitialRegion ? finalInitialRegion : {},

            'currentPost': data['currentPost'],
            'allPosts': pondsPaths != null ? pondsPaths : {},
            'selectedDevice': data['selectedDevice'],
            // allDeviceList: deviceList,
            'allDeviceList': data['allDeviceList'],
            'allRegions': allRegions != null ? allRegions : {},
            'meals': data['meals'],
            'pondsInfo': pondsInfoarr != null ? pondsInfoarr : [],
            'key': data['key']
          };
          //dispatch(updateSelectedDEviceDetails(UPDATE_SELECTED_DEVICE, payload))
          previousData['aquaBotList'] = aquaBotList;
          saveDetailsToRedux(previousData);
          try {
            var firebaseData = await FirebaseFirestore.instance
                .collection("deviceFeedRates")
                .doc("${deviceId}")
                .set(previousData)
                .then((value) {
              //print('Success saved data');
              setState(() {
                isloading = false;
                selectedlong = 0;
                selectedlat = 0;
                selectedposition = 0;
                takemetherespped = 8000;
                chooedlatlohg = '0';
              });
            });
          } catch (error) {
            //print(error);
            //print('Failed to save data');
            setState(() {
              isloading = false;
            });
          }
        }
      });
    } catch (error) {
      setState(() {
        isloading = false;
      });
    }
  }*/

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  deletePostdetails() async {
    if (selectedPost == "Label") {
      Navigator.pop(context);

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CommandAlert("Label cant delete", Colors.red, "AquaBot");
          });
    } else {
      widget.route_controller.removepoint("$selectedposition");
      widget.route_controller.removemarkers('$selectedposition');
      widget.route_controller.clearonly();
      widget.route_controller.updatealerttrue();
      Navigator.pop(context);

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CommandAlert(
                "Deleted successfully", Colors.green, "AquaBot");
          });
    }
  }

  savePostdetails() async {
    if (isSwitched == false) {
      setState(() {
        selectedlat = double.parse("${widget.selDevice.liveModel.gpsLat}");
        selectedlong = double.parse("${widget.selDevice.liveModel.gpsLong}");
      });
    } else {
      setState(() {
        selectedlat = widget.selLatLong.latitude;
        selectedlong = widget.selLatLong.longitude;
      });
    }
    var data = {'m_lat': "$selectedlat", 'm_lng': "$selectedlong"};

    selectedPost == "Label"
        ? widget.route_controller.addlable("label", data)
        : widget.route_controller.addpoints("$selectedposition", data);
    widget.route_controller.updatealerttrue();

    Navigator.pop(context);

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CommandAlert(
              "Position added successfully", Colors.green, "AquaBot");
        });
  }

  /*  savePostDetailsToFirebase() async {
    var selectedMacId = widget.selDevice.macId;
    var gatewayMacId = user.selGateway.gateWayMacId;
    var selectedPost = selectedposition;
    var post = {'latitude': selectedlat, 'longitude': selectedlong};
    var details = {};
    details[selectedposition] = post;
    if (isSwitched == false) {
      setState(() {
        selectedlat = double.parse("${widget.selDevice.liveModel.gpsLat}");
        selectedlong = double.parse("${widget.selDevice.liveModel.gpsLong}");
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    } else {
      setState(() {
        selectedlat = widget.selLatLong.latitude;
        selectedlong = widget.selLatLong.longitude;
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    }
    // setState(() {
    //   selectedlat = widget.selLatLong.latitude;
    //   selectedlong = widget.selLatLong.longitude;
    //   deviceId = data.macId;
    //   //data = widget.selDevice;
    //   isloading = true;
    // });
    //print("post savePostDetailsToFirebase");
    //print(selectedposition);
    //print(post);
    //print("post savePostDetailsToFirebase");
   try {
      var firestoreData = await FirebaseFirestore.instance
          .collection("gateways")
          .doc("${gatewayMacId}")
          .collection("info")
          .doc("profiledata");
      firestoreData.get().then((documentSnapshot) async {
        if (documentSnapshot.exists) {
          //print(documentSnapshot.data());
          var previousData = documentSnapshot.data() as Map;
          var aquaBotList = previousData['aquaBotList'];
          Map<String, dynamic> regions = {};
          // //print(selectedlong);
          // //print(selectedlat);
          regions = aquaBotList[selectedMacId]['regions'];
          if (regions[selectedMacId]['$selectedposition'] != null) {
            regions[selectedMacId]['$selectedposition']['latitude'] =
                selectedlat;
            regions[selectedMacId]['$selectedposition']['longitude'] =
                selectedlong;
          } else {
            var obj = regions;
            obj = {
              //   "$selectedposition": {
              'latitude': selectedlat,
              'longitude': selectedlong
              //   }
            };
            //print(regions);
            //print(selectedposition);
            //regions.addAll(obj);
            //var key = ['$selectedposition']['latitude'];
            regions[selectedMacId].putIfAbsent('$selectedposition', () => obj);
            postPoints.putIfAbsent('$selectedposition', () => obj);
            //regions.putIfAbsent(regions[selectedMacId]['$selectedposition']['longitude'], () => selectedlong);
            //print(regions);
            ////print(regions[selectedMacId]['$selectedposition']['latitude']);
            // regions[selectedMacId].putIfAbsent(['$selectedposition']);
            // regions[selectedMacId]['$selectedposition']['latitude'] =
            //     selectedlat;
            // regions[selectedMacId]['$selectedposition']['longitude'] =
            //     selectedlong;

          }

          //print(regions);
          var finalInitialRegion;
          var pondsPaths = [];
          var pondsInfoarr = [];
          var allRegions = {};
          aquaBotList[selectedMacId]['regions'] = regions;
          // Map botLists = new Map.from(aquaBotList);
          var botLists = jsonDecode(jsonEncode(aquaBotList));
          // cloneDeep(aquaBotList);
          var intitalregions = initialRegion;
          var Deviceregions = data.regions;
          var activeDevices = convertObjectToarray(botLists);
          //print(activeDevices);
          for (var i = 0; i < activeDevices.length; i++) {
            var device = activeDevices[i]['macId'];
            // activeDevicesList[device]=deviceList[device];
            var filterRegion = filterRegions(activeDevices[i]);
            //Object.assign(allRegions, filterRegion);
            var inRegion =
                removeZeroRegions(activeDevices[i]['regions'][device]);
            var initialRegionCords = inRegion;
            if (i == 0) {
              finalInitialRegion = getRegionForCoordinates(initialRegionCords);
            }
            var postpath =
                preparePondsPositionCordsofDevice(initialRegionCords);
            var obj = {'deviceId': device, 'paths': postpath};
            pondsPaths.add(obj);
            // let pond=pondsDetails[device];
            if (activeDevices[i] != null) {
              // var obj = {
              //     'lat': activeDevices[i]['lat'] ? activeDevices[i]['lat'] : 0.0,
              //     'long': activeDevices[i]['long'] ? activeDevices[i]['long'] : 0.0,
              //     'name': activeDevices[i]['deviceName']
              // };
              // pondsInfoarr.add(obj);
            }
          }
          // var payload = {
          //     'initialRegion': finalInitialRegion ? finalInitialRegion : {},

          //     // 'currentPost': data['currentPost'],
          //     // 'allPosts': pondsPaths != null ? pondsPaths : {},
          //     // 'selectedDevice': data['selectedDevice'],
          //     // // allDeviceList: deviceList,
          //     // 'allDeviceList': data['allDeviceList'],
          //     // 'allRegions': allRegions != null ? allRegions : {},
          //     // 'meals': data['meals'],
          //     // 'pondsInfo': pondsInfoarr != null ? pondsInfoarr : [],
          //     // 'key': data['key']
          // };
          //dispatch(updateSelectedDEviceDetails(UPDATE_SELECTED_DEVICE, payload))
          previousData['aquaBotList'] = aquaBotList;
          saveDetailsToRedux(previousData);
          // print("previousData");
          // print(previousData);
          try {
            var firebaseData = await FirebaseFirestore.instance
                .collection("gateways")
                .doc("${gatewayMacId}")
                .collection('info')
                .doc('profiledata')
                .set(previousData, SetOptions(merge: true))
                .then((value) {
              //print('Success saved data');
              //print(deviceId);
              //print(widget.selDevice.macId);
              setState(() {
                isloading = false;
                selectedlong = 0.0;
                selectedlat = 0.0;
                selectedposition = 0;
                takemetherespped = 8000;
                chooedlatlohg = '0';
              });
            });
          } catch (error) {
            //print(error);
            //print('Failed to save data');
            setState(() {
              isloading = false;
            });
          }
        }
      });
    } catch (error) {
      setState(() {
        isloading = false;
      });
    }
  }*/

  /* savePondsLabelToFirebase() async {
    //print(user.selGateway.gateWayMacId);
    user.selGateway.botDevices.forEach((index) {
      if (widget.selDevice.macId == index.macId) {
        setState(() {
          data = index;
        });
      }
    });
    // setState(() {
    //   selectedlat = widget.selLatLong.latitude;
    //   selectedlong = widget.selLatLong.longitude;
    //   deviceId = data.macId;
    //   //data = widget.selDevice;
    //   isloading = true;
    // });
    if (isSwitched == false) {
      setState(() {
        selectedlat = double.parse("${widget.selDevice.liveModel.gpsLat}");
        selectedlong = double.parse("${widget.selDevice.liveModel.gpsLong}");
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    } else {
      setState(() {
        selectedlat = widget.selLatLong.latitude;
        selectedlong = widget.selLatLong.longitude;
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    }
    var selectedMacId = widget.selDevice.macId;
    var gatewayMacId = user.selGateway.gateWayMacId;
    //prepare json for configuration,
    var device = widget.selDevice.macId;

    var info = {'lat': selectedlat, 'long': selectedlong, 'name': ''};
    try {
      var firestoreData = await FirebaseFirestore.instance
          .collection("gateways")
          .doc("$gatewayMacId")
          .collection("info")
          .doc("profiledata")
          .get()
          .then((DocumentSnapshot documentSnapshot) async {
        // firestoreData.get().then((DocumentSnapshot documentSnapshot){
        if (documentSnapshot.exists) {
          //print(documentSnapshot.data());
          var previousData = documentSnapshot.data() as Map;
          var aquaBotList = previousData['aquaBotList'];
          aquaBotList[selectedMacId]['lat'] = selectedlat;
          aquaBotList[selectedMacId]['long'] = selectedlong;
          var regions = {};
          //print(regions);
          // regions = aquaBotList[selectedMacId]['regions'];
          // if (regions[selectedMacId]['$selectedposition'] != null) {
          //   regions[selectedMacId]['$selectedposition']['latitude'] =
          //       selectedlat;
          //   regions[selectedMacId]['$selectedposition']['longitude'] =
          //       selectedlong;
          // } else {
          //   var obj = {
          //     'selectedposition': {
          //       'latitude': selectedlat,
          //       'longitude': selectedlong
          //     }
          //   };
          // }
          // //print(regions);
          var finalInitialRegion;
          var pondsPaths = [];
          var pondsInfoarr = [];
          var allRegions = {};
          //aquaBotList[selectedMacId]['regions'] = regions;
          // Map botLists = new Map.from(aquaBotList);
          var botLists = jsonDecode(jsonEncode(aquaBotList));
          // cloneDeep(aquaBotList);
          //print(initialRegion);
          var intitalregions = initialRegion;
          var Deviceregions = data.regions;
          var activeDevices = convertObjectToarray(botLists);
          //print(activeDevices);
          for (var i = 0; i < activeDevices.length; i++) {
            var device = activeDevices[i]['macId'];
            // activeDevicesList[device]=deviceList[device];
            //print(activeDevices[i]['macId']);
            var filterRegion = filterRegions(activeDevices[i]);
            //Object.assign(allRegions, filterRegion);
            allRegions = filterRegion;
            //print(activeDevices[i]['regions'][device]);
            var inRegion =
                removeZeroRegions(activeDevices[i]['regions'][device]);
            var initialRegionCords = inRegion;
            //print(i);
            if (i == 0) {
              //print(initialRegionCords);
              finalInitialRegion = getRegionForCoordinates(initialRegionCords);
            }
            var postpath =
                preparePondsPositionCordsofDevice(initialRegionCords);
            var obj = {'deviceId': device, 'paths': postpath};
            pondsPaths.add(obj);
            // let pond=pondsDetails[device];
            if (activeDevices[i] != null) {
              var obj = {
                'lat': activeDevices[i]['lat'] != null
                    ? activeDevices[i]['lat']
                    : 0,
                'long': activeDevices[i]['long'] != null
                    ? activeDevices[i]['long']
                    : 0,
                'name': activeDevices[i]['deviceName']
              };
              //print(obj);
              pondsInfoarr.add(obj);
            }
          }
          var payload = {
            'initialRegion':
                finalInitialRegion != null ? finalInitialRegion : {},
            // 'currentPost': data['currentPost'],
            // 'allPosts': pondsPaths != null ? pondsPaths : {},
            'selectedDevice': widget.selDevice,
            // allDeviceList: deviceList,
            //'allDeviceList': widget.,
            // 'allRegions': allRegions != null ? allRegions : {},
            // 'meals': data['meals'],
            // 'pondsInfo': pondsInfoarr != null ? pondsInfoarr : [],
            // 'key': data['key']
          };
          //dispatch(updateSelectedDEviceDetails(UPDATE_SELECTED_DEVICE, payload))
          previousData['aquaBotList'] = aquaBotList;
          var dummyprevData = previousData;
          // saveDetailsToRedux(previousData);
          try {
            var firebaseData = await FirebaseFirestore.instance
                .collection("gateways")
                .doc("${gatewayMacId}")
                .collection('info')
                .doc('profiledata')
                .set(previousData, SetOptions(merge: true))
                .then((value) {
              //print(selectedposition);
              setState(() {
                recievedData = true;
              });
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return Alert("Label saved successfully", Colors.green,
                        "Map Settings");
                  });
              // setState(() {
              //   displayToast = true;
              //   toastColor = Colors.green;
              //   toastName = "Success";
              // });
            });
            // {
            //   //print("Success saved label  data");
            //   setState(() {
            //     isloading = false;
            //     selectedlong = 0;
            //     selectedlat = 0;
            //     selectedposition = "0";
            //   });
            //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Label saved")));
            // });
          } catch (error) {
            //print(error);
            //print("Failed to save label details");
            setState(() {
              recievedData = true;
            });
            /*   ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text("Failed to save Label")));*/
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return Alert(
                      "Sending command failed", Colors.red, "Map Settings");
                });
            // setState(() {
            //   isloading = false;

            //   displayToast = true;
            //   toastColor = Colors.red;
            //   toastName = "Failed";
            // });
          }
        }
      });
    } catch (error) {
      //print(error);
      setState(() {
        isloading = false;
        recievedData = true;
      });
    }
  }*/

  /* Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();
    setState(() {
      recievedData = true;
    });
    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

   setPositiondetails() async {
    setState(() {
      recievedData = false;
    });
    user.selGateway.botDevices.forEach((index) {
      if (widget.selDevice.macId == index.macId) {
        setState(() {
          data = index;
        });
      }
    });
    if (isSwitched == false) {
      setState(() {
        selectedlat = double.parse("${widget.selDevice.liveModel.gpsLat}");
        selectedlong = double.parse("${widget.selDevice.liveModel.gpsLong}");
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    } else {
      setState(() {
        selectedlat = widget.selLatLong.latitude;
        selectedlong = widget.selLatLong.longitude;
        deviceId = data.macId;
        //data = widget.selDevice;
        isloading = true;
      });
    }
    //print(selectedlat);

    //print(selectedlong);
    if (selectedPost == 'Label') {
      //print(data);
      savePondsLabelToFirebase();
    } else {
      //get current time in seconds
      //print("Home");
      //print(data.toString());
      var currentTime = DateTime.now();
      var version = currentTime.millisecondsSinceEpoch ~/ 1000;
      var config = {
        "frametype": "setPost",
      };
      //prepare control command data object
      var configCmd = {
        'version': version,
        'deviceId': data.macId,
        'm_lat': selectedlat,
        'm_lng': selectedlong,
        'postNumber': selectedposition,
      };
      //config['commands'] = [configCmd];
      //print("set position");
      // //print(JSON.stringify(config));
      //print("set position");
      //print(configCmd);
      try {
        //print("DAGHDsddsdgh");
        ////print(sendCommandDirectlyToDevice(configCmd, 'setPost'));
        var finalObj = {'frametype': "setPost", 'commands': configCmd};
        // showOverlay(finalObj);
        var responseData =
            await sendCommandDirectlyToDeviceHome(configCmd, 'setPost');
        // setState(() {
        //   enteredData = configCmd;
        // });
        // setState(() {
        //   isloading = false;
        // });
        //print(responseData);
        //print('setPost');
        ////print(jsonDecode(responseData));
        ////print(responseData.toString());
        // //print('setPost');
        var latLongData = jsonDecode(responseData.body);
        setState(() {
          selectedlong = latLongData['lat'];
          selectedlat = latLongData['lng'];
        });
        savePostDetailsToFirebase();
        ////print(responseData.body);
        //  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Sending Command Success")));
        //  setState(() {
        //     displayToast = true;
        //     toastColor = Colors.green;
        //     toastName = "Success";
        //   });
        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return Alert(
        //           "Command sent successfully", Colors.green, "Map Settings");
        //     });
        if (responseData.statusCode == 200) {
          getResponseBasedOnDevice("", 200);
        } else {
          getResponseBasedOnDevice("", 500);
        }
        print(selectedlat);
      } catch (error) {
        //print(error);
        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return Alert(
        //           "Sending command failed", Colors.red, "Map Settings");
        //     });
        getResponseBasedOnDevice(version, 500);
        print("set position");
        print(error);
        print("set position");
        savePostDetailsToFirebase();
        setState(() {
          isloading = false;
        });
        // setState(() {
        //   // data = {
        //   //   ...data,
        //   //   'isLoading': false
        //   // };
        // });
        // setState(() {
        //   displayToast = true;
        //   toastColor = Colors.red;
        //   toastName = "Failed";
        // });
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Sending Command Failed!")));
      }
    }
  }*/
/*
  sendPoststoDevice(devicePosts) async {
    var deviceId = widget.selDevice.macId;
    var deviceid = widget.selDevice.macId;
    var deviceType;
    user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
      if (deviceid == element.deviceId) {
        deviceType = element.deviceType;
      }
    });
    var cmdObj = {'m_arrLocations': []};
    if (deviceType == "Line") {
      var arr = [];
      for (var i = 0; i < 10; i++) {
        if (i < 3) {
          arr.add(devicePosts['m_arrLocations'][i]);
        } else {
          var val = {'m_lat': "0.000", 'm_lng': "0.000"};
          arr.add(val);
        }
      }
      cmdObj['m_arrLocations'] = arr;
    } else {
      cmdObj['m_arrLocations'] = devicePosts['m_arrLocations'];
    }
    try {
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${deviceId}/rpc/setRoute?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var encodedcmdObj = jsonEncode(cmdObj);
      var getData = await http.post(path,
          body: encodedcmdObj,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (getData.statusCode == 200) {
        var responseData = json.decode(getData.body);
        //print("REsponse Data for Tune:" + responseData);
        saveRegionsToFirebase(devicePosts);
      } else {
        setState(() {
          isloading = false;
          loading = false;
        });
        //print("Exited without status code 200");
      }
    } catch (error) {
      //print(error);
      setState(() {
        isloading = false;
        loading = false;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Selected Device offline")));
    }
  }

  getPreviousDeivicePosition() async {
    setState(() {
      isloading = true;
      loading = true;
    });
    var deviceId = "device421";
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var cmdObj = {'version': version, 'deviceId': deviceId};
    try {
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${deviceId}/rpc/getRoute?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      setState(() {
        enteredApi = path;
      });
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var encodedcmdObj = jsonEncode(cmdObj);
      var getData = await http.post(path,
          body: encodedcmdObj,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (getData.statusCode == 200) {
        var responseData = json.decode(getData.body);
        //print("REsponse Data for Tune:" + responseData);
        sendPoststoDevice(responseData);
        //print(data);
      } else {
        setState(() {
          isloading = false;
          loading = false;
        });
        //print("Exited without status code 200");
      }
    } catch (error) {
      setState(() {
        isloading = false;
        loading = false;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Device offline")));
      //print(error);
    }
  }*/

  renderText(name, fontSize) {
    var text = Text(name,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: fontSize,
            fontFamily: 'BrandonReg',
            color: Colors.black));
    return text;
  }

  setSelectedLatLong(val) {
    //print(val);
    if (val == '0') {
      setState(() {
        chooedlatlohg = val;
        selectedlat = touchedLat;
        selectedlong = touchedLong;
      });
    } else if (val == '1') {
      //print('Current Device');
      setState(() {
        chooedlatlohg = val;
        selectedlat = currentDevicelat;
        selectedlong = currentDevicelohg;
      });
    } else {
      setState(() {
        chooedlatlohg = val;
        selectedlat = currentDevicelat;
        selectedlong = currentDevicelohg;
      });
    }
  }

  switchHandler(value) {
    setState(() {
      isSwitched = value;
    });
    //print(isSwitched);
    setSelectedLatLong(isSwitched == false ? "2" : "0");
  }

  onpressofsend() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultiAlert("Are you sure you want to Delete ?", Colors.black,
              "Are you sure you want to Delete ?", () {
            deletePostdetails();
          }, () {
            Navigator.pop(context);
          });
        });
  }

  onpressofpoints() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultiAlert("Are you sure you want to Update Points ?",
              Colors.black, "Are you sure you want to Update Points ?", () {
            uploadpoints(pointscontroller.text);
          }, () {
            Navigator.pop(context);
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    var deviceName = widget.selDevice.deviceName;
    return recievedData == false
        ? Container(
            height: 350,
            child: Center(
              child: SpinKitThreeBounce(
                size: 30.0,
                color: Color(0xFF0170d0),
              ),
            ))
        : Container(
            padding: EdgeInsets.all(8),
            margin: EdgeInsets.only(top: 10),
            // height: 350,
            child: ListView(children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //getHeader(),
                  Align(
                    alignment: Alignment.center,
                    child: renderText("Settings ($deviceName)", 18.0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      getTouchedLocation(),
                      // Column(
                      //   children: [
                      //     renderText("Touched", 16.0),
                      //     renderText(widget.selLatLong.latitude.toString()+","+widget.selDevice.long.toString(), 10.0),
                      //   ],
                      // ),
                      Align(
                          alignment: Alignment.centerRight,
                          child: CommandButton(
                              "Take me There", Color(0xFF0170d0), () {
                            //  takeMeThereBtnHandler();
                          })),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Divider(
                    thickness: 1,
                    color: Color(0xFFf2f2f2),
                  ),
                  // getBotLocation(),
                  // Container(child: Row(
                  //   children: [
                  //     getBotLocation(),
                  //     ListTile(
                  //       title: getBotLocation(),
                  //       leading: Radio(value: 2, groupValue: -1, onChanged: (value){},),
                  //     )
                  //   ],
                  // )),

                  // SizedBox(
                  //   height: 8,
                  // ),
                  // Divider(
                  //       thickness: 1,
                  //       color: Color(0xFFf2f2f2),
                  //     ),
                  Column(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: renderText("Set Post", 18.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            children: [
                              renderText("Bot Position", 18.0),
                              renderText(
                                  widget.selDevice.liveModel.gpsLat.toString(),
                                  11.0),
                              renderText(
                                  widget.selDevice.liveModel.gpsLong.toString(),
                                  11.0),
                            ],
                          ),
                          Switch(
                            value: isSwitched,
                            onChanged: (value) {
                              switchHandler(value);
                            },
                            activeTrackColor: Color(0xFF0170d0),
                            activeColor: Color(0xFF0170d0),
                          ),
                          Column(
                            children: [
                              renderText("Touched Position", 18.0),
                              renderText(
                                  widget.selLatLong.latitude.toStringAsFixed(6),
                                  11.0),
                              renderText(
                                  widget.selLatLong.longitude
                                      .toStringAsFixed(6),
                                  11.0)
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  //getDeviceLocation(),
                  // SizedBox(
                  //   height: 8,
                  // ),
                  //getTouchedLocation(),
                  SizedBox(
                    height: 8,
                  ),
                  Divider(
                    thickness: 1,
                    color: Color(0xFFf2f2f2),
                  ),
                  getDropDown(),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      displayToast == true
                          ? Container(
                              width: 240,
                              //height: 30,
                              child: RenderToast(toastName, toastColor, () {
                                setState(() {
                                  displayToast = false;
                                });
                              }),
                            )
                          : SizedBox(),
                      Row(
                        children: [
                          Align(
                              alignment: Alignment.centerRight,
                              child: CommandButton("Update", Color(0xFF0170d0),
                                  () {
                                savePostdetails();
                                // setPositiondetails();
                              })),
                          Align(
                              alignment: Alignment.centerRight,
                              child:
                                  CommandButton("Delete", Colors.redAccent, () {
                                onpressofsend();
                                //  deletePostdetails();
                                //  savePostdetails();
                                // setPositiondetails();
                              })),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.only(top: 20, right: 10, bottom: 10),
                    height: 100,
                    //  width: 300,
                    child: TextField(
                      autofocus: false,
                      maxLines: 30,
                      controller: pointscontroller,
                      decoration: InputDecoration(
                          labelText: "Points",
                          hintText: "Enter Points",
                          filled: true),
                    ),
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CommandButton("Update", Color(0xFF0170d0), () {
                        pointscontroller.text != null &&
                                pointscontroller.text.isNotEmpty
                            ? onpressofpoints()
                            : "";
                      })
                    ],
                  )
                ],
              ),
            ]),
          );
  }

  uploadpoints(body) {
    widget.route_controller.clearAll();

    var data = jsonDecode(body);
    if (data['m_arrLocations'] != null) {
      for (int i = 0; i < (data['m_arrLocations'] as List).length; i++) {
        Map e = (data['m_arrLocations'] as List)[i];
        widget.route_controller.addpoints("$i", e);
      }
    }
    widget.route_controller.updatealerttrue();
    Navigator.pop(context);

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CommandAlert("Updated successfully", Colors.green, "AquaBot");
        });
  }

  Widget getHeader() {
    var headerWid = Center(
      child: Text(
        widget.selDevice.deviceName,
        style: TextStyle(
            fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
      ),
    );
    return headerWid;
  }

  Widget getDeviceLocation() {
    LatLng currentPostion;

    var headerWid = StreamBuilder(
      stream: homeBloc.locationRefreshStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          LatLng latLong = snapshot.data;
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Device Position',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
              Text(
                latLong.latitude.toString() + latLong.longitude.toString(),
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
              )
            ],
          );
        }
        return Text('');
      },
    );
    return headerWid;
  }

  Widget getBotLocation() {
    var headerWid = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Bot Position',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            fontFamily: 'BrandonReg',
            color: Colors.black,
          ),
        ),
        Column(
          children: [
            Text(
              widget.selDevice.lat.toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 11,
                fontFamily: 'BrandonReg',
                color: Colors.black,
              ),
            ),
            Text(
              widget.selDevice.long.toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 11,
                fontFamily: 'BrandonReg',
                color: Colors.black,
              ),
            ),
          ],
        )
      ],
    );
    return headerWid;
  }

  Widget getTouchedLocation() {
    var headerWid = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Touched',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            fontFamily: 'BrandonReg',
            color: Colors.black,
          ),
        ),
        Text(
          widget.selLatLong.latitude.toStringAsFixed(6) +
              "," +
              widget.selLatLong.longitude.toStringAsFixed(6),
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 10,
            fontFamily: 'BrandonReg',
            color: Colors.black,
          ),
        )
      ],
    );
    return headerWid;
  }

  Widget getDropDown() {
    //print("sgsggsgg");
    //print(postPoints.length);
    var deviceid = widget.selDevice.macId;
    var deviceType;
    user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
      if (deviceid == element.deviceId) {
        deviceType = element.deviceType;
      }
    });
    var wid = Row(
      children: [
        Text(
          'Select Post Number',
          style: TextStyle(
            fontSize: 14,
            color: Colors.black,
          ),
        ),
        SizedBox(
          width: 8,
        ),
        DropdownButton(
          // hint: Text(
          //   "Please choose a langauage",
          //   style: TextStyle(
          //       color: Colors.black, fontSize: 14, fontWeight: FontWeight.w500),
          // ),
          underline: Container(
            color: Colors.redAccent,
          ),
          value: _chosenValue,
          onChanged: (newValue) {
            setState(() {
              _chosenValue = newValue;
              selectedPost = newValue;
              //print("ghskjgsgjdgdg");
              //print(newValue);
              //print(routePositions.indexOf(newValue));
              if (routePositions.indexOf(newValue) > 1) {
                selectedposition = deviceType == 'Line'
                    ? linePositions.indexOf(newValue) - 1
                    : routePositions.indexOf(newValue) - 1;
              }
            });
            print(selectedposition);
          },
          items: deviceType == 'Line'
              ? linePositions.map((ele) {
                  return DropdownMenuItem(
                    // enabled: ele == "Label" ? true : postPoints.length+1 >= linePositions.indexOf(ele) ? true : false,
                    child: Text(ele),
                    value: ele,
                  );
                }).toList()
              : routePositions.map((ele) {
                  //print(ele);
                  return DropdownMenuItem(
                    enabled: ele == "Label"
                        ? true
                        : postPoints.length + 1 >= routePositions.indexOf(ele)
                            ? true
                            : false,
                    child: Text(ele,
                        style: TextStyle(
                            color: ele == "Label"
                                ? Colors.black
                                : postPoints.length + 1 >=
                                        routePositions.indexOf(ele)
                                    ? Colors.black
                                    : Colors.grey)),
                    value: ele,
                  );
                }).toList(),
        ),
        // DropdownButton(
        //   focusColor: Colors.white,
        //   value: _chosenValue,
        //   //elevation: 5,
        //   style: TextStyle(color: Colors.white),
        //   iconEnabledColor: Colors.black,
        //   items: routePositions.map<DropdownMenuItem>((value) {
        //     return DropdownMenuItem(
        //       value: value,
        //       child: Text(
        //         value,
        //         style: TextStyle(color: Colors.black),
        //       ),
        //     );
        //   }).toList(),
        //   hint: Text(
        //     "Please choose a langauage",
        //     style: TextStyle(
        //         color: Colors.black, fontSize: 14, fontWeight: FontWeight.w500),
        //   ),
        //   onChanged: (value) {
        //     setState(() {
        //       _chosenValue = value;
        //     });
        //   },
        // )
      ],
    );
    return wid;
  }

  Widget getButton(String title, onPressed) {
    Widget wid = Align(
        alignment: Alignment.centerRight,
        child: CommandButton(title, Colors.blueAccent, onPressed)
        // RaisedButton(
        //   shape: StadiumBorder(),
        //   onPressed: () {
        //     onPressed;
        //   },
        //   child: Text(title, style: ,),
        //   color: Colors.blueAccent,
        // ),
        );
    return wid;
  }

  updatePressed() {}
}
