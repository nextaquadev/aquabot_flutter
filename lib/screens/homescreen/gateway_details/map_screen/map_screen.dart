import 'dart:async';

import 'dart:typed_data';

import 'dart:ui';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/http_server/live_map_server.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/live_polyline.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/mapconfig/mapconfig.dart';
import 'package:aquabot/utils/aqua_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:flutter/services.dart';

class MapView extends StatefulWidget {
  // const MapView({Key key}) : super(key: key);
  final BotModel selDevice;
  LatLng initialcameraposition;
  double zoom;
  bool clear;

  MapView({this.selDevice, this.initialcameraposition, this.zoom, this.clear});

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  Live_poly live_poly = Live_poly();
  //LatLng _initialcameraposition = LatLng(17.448227813051307, 78.36215291172266);

  // LatLng _initialcameraposition;
  // double _zoomposition = 19;
  CameraPosition _position;
  // List<LatLng> points = [];

  GoogleMapController _controller;
  Location _location = Location();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};
  //StreamController controller = StreamController();

  // Set<Polyline> _polyline = {};
  // Set<Polyline> _livePolyline = {};
  AquaUser user = AquaUser.getInstance();

  //Validations
  int botCount = 0;
  int postCount = 0;
  int lineCount = 0;

  void dispose() {
    try {
      _controller?.dispose();
    } catch (err) {
      // //print(err);
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    updateLocation();

    // addMarkers();
    homeBloc.refreshBotDetailsView();
  }

  void addMarkers() {
    // updateLocation();
    //Polylines
    _addPolyline();
    //  _addLivePolyline();
    //  _addliveMarkers();
    /*  user.selGateway.botDevices.forEach((element) {
      _addliveMarkers(element);
    });*/

    //line names
    lineCount = botCount = user.selGateway.botDevices.length;
    /*  user.selGateway.botDevices.forEach((element) {
      _addNameMarkers(element.routePoints[0], element);
    });*/

    //Markers
    user.selGateway.botDevices.forEach((device) {
      int j = 0;
      device.routePoints.forEach((bMarker) {
        //  //print(device.routePoints);

        postCount = postCount + 1;
        BotMarker nextMarker = null;
        // if (device.routePoints.length > j + 1) {
        //   nextMarker = device.routePoints[j];
        // }
        //  markers.removeWhere((key, value) => key.value == '$j${device.macId}');

        kIsWeb
            ? _addMarkersforweb(bMarker, nextMarker, j, device.macId)
            : _addMarkers(bMarker, nextMarker, j, device.macId);

        j = j + 1;
      });
    });
  }

  /* void updatecameraLocation(latlang) {
    double lat = latlang.latitude;
    double long = latlang.longitude;
    _initialcameraposition = LatLng(lat, long);

    if (_controller != null) {
      _controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: _initialcameraposition, zoom: 19),
      ));
    }
  }*/

  void updateLocation() {
    /*if (widget.selDevice != null) {
      double lat = widget.selDevice.lat;
      double long = widget.selDevice.long;
      _initialcameraposition = LatLng(lat, long);*/

    if (_controller != null) {
      _controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: widget.initialcameraposition, zoom: widget.zoom),
      ));
    }
    //}
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;

    /*  _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
        ),
      );
   // });*/
  }

  _updatecamera(CameraPosition position) {
    //print(position.target);
    _position = position;

    // setState(() {
    widget.initialcameraposition = position.target;
    widget.zoom = position.zoom;
    // });
    // //print("$_initialcameraposition    This is initial");
  }

  @override
  Widget build(BuildContext context) {
    // updateLocation();
    return Container(
      color: Colors.white,
      child: getMapView(),
    );
  }

  Widget getMapContainerView() {
    Widget mapWid = StreamBuilder(
        stream: homeBloc.botDetailsRefreshStream,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            bool status = snapshot.data;
            return getMapView();
          }
          return getMapView();
        });
    return mapWid;
  }

  Widget getMapView() {
    //print("This is live clear:${widget.clear}");

    updateLocation();

    addMarkers();

    _addLivePolyline();
    //  addMarkers();

    user.selGateway.botDevices.forEach((index) {
      //   markers.removeWhere((key, value) => key.value == '${index.macId}');

      //  markers.removeWhere((key, value) => key.value == '${index.macId}');

      user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
        if (index.macId == element.deviceId) {
          /*  markers
              .removeWhere((key, value) => key.value == '${element.deviceId}');*/

          kIsWeb ? _addliveMarkersforweb(element) : _addliveMarkers(element);
        }
      });
    });
    user.selGateway.botDevices.forEach((element) {
      if (element.routePoints.isNotEmpty) {
        _addNameMarkers(element.routePoints[0], element);
      }
    });

    Widget mapWid = GoogleMap(
      onCameraMove: _updatecamera,
      gestureRecognizers: Set()
        ..add(Factory<OneSequenceGestureRecognizer>(
            () => new EagerGestureRecognizer()))
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      initialCameraPosition: CameraPosition(
          target: widget.initialcameraposition, zoom: widget.zoom),
      mapType: MapType.satellite,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      zoomGesturesEnabled: true,
      zoomControlsEnabled: false,
      myLocationButtonEnabled: false,
      mapToolbarEnabled: false,
      markers: Set<Marker>.of(markers.values),
      polylines: Set<Polyline>.of(polylines.values),
      onLongPress: (latlang) {
        /*  _addMarkerLongPressed(latlang);
        //  updatecameraLocation(latlang);
        /* CameraPosition(
            target: LatLng(latlang.latitude, latlang.longitude), zoom: 21);*/

        showBarModalBottomSheet(
          barrierColor: Color.fromRGBO(0, 0, 0, 0.3),
          context: context,
          builder: (context) => Container(
            color: Color(0xFFE8EAF6),
            height: MediaQuery.of(context).size.height * 0.5,
            child: MapConfig(
              selDevice: widget.selDevice,
              selLatLong: latlang,
            ),
          ),
        ).then((value) {
          //print("showmodel");
          setState(() {
            markers.removeWhere((key, value) => key.value == 'touched');
          });
          //  markers.removeWhere((key, value) => key.value == 'touched');
          // updateLocation();
        });*/

        //we will call this function when pressed on the map
      },
    );
    // //print(_livePolyline);
    // //print(_polyline);
    return mapWid;
  }

  _addMarkerLongPressed(LatLng latlang) {
    double lat = latlang.latitude;
    double long = latlang.longitude;
    //   _initialcameraposition = LatLng(lat, long);

    // setState(() {
    // updatecamera(_position);
//loadAddress(latlang);
    // var _latLng = latlang;

    //  _initialcameraposition = _position.target;
    //  _zoomposition = _position.zoom;
    /*  CameraUpdate.newCameraPosition(
        CameraPosition(target: _initialcameraposition, zoom: 5),
      );*/

    MarkerId markerId = MarkerId("touched");
    //print("This is marker:$markerId     $latlang");

    Marker marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: markerId,
      draggable: false,
      visible: true,

      position: latlang,
      infoWindow: InfoWindow(
        title: "Touched",
        //  snippet: '5 Star Rating',
      ),
      icon: BitmapDescriptor.defaultMarker,
    );
    /* Position newmarker =
        Position(longitude: latlang.longitude, latitude: latlang.latitude);
    markers[markerId] = marker.copyWith(
        positionParam: LatLng(newmarker.latitude, newmarker.longitude));
    setState(() {});*/

    // markers[markerId] = marker;

    /* if (markers.length == (postCount + botCount)) {
      setState(() {
        markers[markerId] = marker;
      });
    }*/
    // if (markers.length == (postCount + botCount)) {

    setState(() {
      markers[markerId] = marker;
    });

    //  }
    //   markers.removeWhere((key, value) => key.value == 'touched');
    //  });
    // markers.removeWhere((key, value) => key.value == 'touched');
  }

  Future _addMarkersforweb(
      BotMarker botmarker, BotMarker nextBotmarker, int title, macid) async {
    final MarkerId markerId = MarkerId("$title$macid");
    LatLng ll = botmarker.latlang;
    // //print(botmarker.latlang);

    if (nextBotmarker != null) {
      ll = AquaUtils()
          .computeCentroid([botmarker.latlang, nextBotmarker.latlang]);
    }
    /* final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/yellow_home.png', 45);*/

    BitmapDescriptor bitmapDescriptor = title == 0
        ? await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5, size: Size(30, 30)),
            'images/yellow_home.png')
        : await AquaUtils().getClusterMarker(
            title.toString(), Color(0xFFFFC107), 20, Color(0xFFFFC107));
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      visible: true,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Marker here",
        snippet: title == 0 ? "Home" : 'Post: $title',
      ),
      icon: bitmapDescriptor,
    );

    markers[markerId] = marker;

    if (markers.length == (postCount + botCount)) {
      setState(() {});
    }
  }

  Future _addMarkers(
      BotMarker botmarker, BotMarker nextBotmarker, int title, macid) async {
    // markers.removeWhere((key, value) => key.value == 'touched');

    final MarkerId markerId = MarkerId("$title$macid");
    LatLng ll = botmarker.latlang;
    // //print(botmarker.latlang);

    if (nextBotmarker != null) {
      ll = AquaUtils()
          .computeCentroid([botmarker.latlang, nextBotmarker.latlang]);
    }
    final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/yellow_home.png', 45);

    BitmapDescriptor bitmapDescriptor = title == 0
        ? BitmapDescriptor.fromBytes(markerIcon)
        : await AquaUtils().getClusterMarker(
            title.toString(), Color(0xFFFFC107), 20, Color(0xFFFFC107));
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      visible: true,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Marker here",
        snippet: title == 0 ? "Home" : 'Post: $title',
      ),
      icon: bitmapDescriptor,
    );

    markers[markerId] = marker;

    if (markers.length == (postCount + botCount)) {
      setState(() {});
    }
  }

  Future _addNameMarkers(BotMarker botmarker, BotModel element) async {
    MarkerId markerId = MarkerId("label${element.macId}");
    // LatLng ll = botmarker.latlang;
    double lat = element.lat;
    double long = element.long;
    LatLng ll = LatLng(lat, long);
    //print("This is name:$ll");
    /*  final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/aeroplane.png', 50);*/

    /*  if (element.routePoints.length >= 2) {
      ll = AquaUtils().computeCentroid(
          [element.routePoints[0].latlang, element.routePoints.last.latlang]);
    }*/
    BitmapDescriptor bitmapDescriptor = await AquaUtils().getClusterMarkerName(
      element.deviceName,
      Colors.white,
      40,
    );
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Label here",
        //  snippet: 'This ssss good',
      ),
      icon: bitmapDescriptor, /* BitmapDescriptor.fromBytes(markerIcon),*/
    );

    markers[markerId] = marker;

    if (markers.length == (postCount + botCount)) {
      setState(() {});
    }
  }

  Future _addliveMarkersforweb(LiveBotModel element) async {
    MarkerId markerId = MarkerId("${element.deviceId}");
    //  MarkerId markerId = MarkerId('$i');

    //print("Marker id =$markerId");

    // user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
    //  if (widget.selDevice.macId == element.deviceId) {
    // final MarkerId markerId = MarkerId('old');
    //  LatLng ll;
    List<LatLng> points = [];
    var cog = element.cog != null || element.cog != '' ? element.cog : 0.0;

    element.liveLatLongs.isNotEmpty
        ? element.liveLatLongs.forEach((point) async {
            //  if (index.macId == element.deviceId) {
            points.clear();
            points.add(point);
          })
        : null;
    LatLng ll = points != null && points.isNotEmpty
        ? points.removeLast()
        : LatLng(0.0, 0.0);
    //  //print("This is latlong : $ll");

    // LatLng ll = botmarker.latlang;
    // double lat = element.lat;
    // double long = element.long;
    // LatLng ll = LatLng(lat, long);
    /*  final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/aeroplane.png', 60);*/

    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      rotation: cog.toDouble(),

      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Current Position",
        //  snippet: 'This ssss good',
      ),
      icon: await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5, size: Size(30, 30)),
          'images/aeroplane.png'),
    );
    //  markers.removeWhere((key, value) => key.value == '0');
    //  setState(() {
    //  markers[markerId] = marker;
    //  });

    //  markers[markerId] = marker;
    /*  Position newmarker =
        Position(longitude: ll.longitude, latitude: ll.latitude);*/
    // markers[markerId] = marker;
    /* if (mounted) {
      setState(() {
        //   markers.removeWhere((key, value) => key.value == '${element.deviceId}');

        markers[markerId] = marker;

         markers[markerId] = marker.copyWith(
              positionParam: LatLng(newmarker.latitude, newmarker.longitude));
      
      });
    }*/

    // markers[markerId] = marker;
    //  }
    // });
    /* markers[markerId] = marker.copyWith(
        positionParam: LatLng(newmarker.latitude, newmarker.longitude));*/
    markers[markerId] = marker;

    if (markers.length == (postCount + botCount)) {
      setState(() {
        markers[markerId] = marker;
      });
    }
    //}

    //  markers[markerId] = marker;
    //  });
    //markers[markerId] = marker;
    //  });
  }

  Future _addliveMarkers(LiveBotModel element) async {
    //  markers.removeWhere((key, value) => key.value == '${element.deviceId}');

    //int i = 0;
    // user.selGateway.botDevices.forEach((index) {
    // markers.removeWhere((key, value) => key.value == '${index.macId}');

    MarkerId markerId = MarkerId("${element.deviceId}");
    //  MarkerId markerId = MarkerId('$i');

    //print("Marker id =$markerId");

    // user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
    //  if (widget.selDevice.macId == element.deviceId) {
    // final MarkerId markerId = MarkerId('old');
    //  LatLng ll;

    var cog = element.cog != null || element.cog != '' ? element.cog : 0.0;

    /* element.liveLatLongs.isNotEmpty
        ? element.liveLatLongs.forEach((point) async {
            //  if (index.macId == element.deviceId) {
            points.clear();
            points.add(point);
          })
        : null;*/

    LatLng point = element.gpsLat != null && element.gpsLong != null
        ? LatLng(double.parse(element.gpsLat), double.parse(element.gpsLong))
        : LatLng(0.0, 0.0);
    //  //print("This is latlong : $ll");

    // LatLng ll = botmarker.latlang;
    // double lat = element.lat;
    // double long = element.long;
    // LatLng ll = LatLng(lat, long);
    final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/aeroplane.png', 60);

    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      rotation: cog.toDouble(),

      position:
          point, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Current Position",
        //  snippet: 'This ssss good',
      ),
      icon: BitmapDescriptor.fromBytes(markerIcon),
    );
    //  markers.removeWhere((key, value) => key.value == '0');
    //  setState(() {
    //  markers[markerId] = marker;
    //  });

    //  markers[markerId] = marker;
    /*  Position newmarker =
        Position(longitude: ll.longitude, latitude: ll.latitude);*/
    // markers[markerId] = marker;
    /* if (mounted) {
      setState(() {
        //   markers.removeWhere((key, value) => key.value == '${element.deviceId}');

        markers[markerId] = marker;

         markers[markerId] = marker.copyWith(
              positionParam: LatLng(newmarker.latitude, newmarker.longitude));
      
      });
    }*/

    // markers[markerId] = marker;
    //  }
    // });
    /* markers[markerId] = marker.copyWith(
        positionParam: LatLng(newmarker.latitude, newmarker.longitude));*/
    markers[markerId] = marker;

    if (markers.length == (postCount + botCount)) {
      setState(() {
        markers[markerId] = marker;
      });
    }
    //}

    //  markers[markerId] = marker;
    //  });
    //markers[markerId] = marker;
    //  });
  }

  _addPolyline() {
    //  _polyline = {};
    //Polylines
    //  final PolylineId polylineId = PolylineId(AquaUtils().getRandomString(5));

    int i = 0;
    user.selGateway.botDevices.forEach((element) {
      List<LatLng> points = [];
      element.routePoints.forEach((marker) {
        //  //print(marker.latlang);
        points.add(marker.latlang);
      });
      //  //print(points);

      if (element.afType != 'Line' && element.routePoints.length > 0) {
        // //print(element.routePoints[0].latlang);
        points.add(element.routePoints[0].latlang);
      }
      //   //print(points);
      PolylineId polylineId = PolylineId("${element.macId}");
      //PolylineId(AquaUtils().getRandomString(5));

      var polyline = Polyline(
        polylineId: polylineId,
        // PolylineId(AquaUtils().getRandomString(5)),
        color: Color(0xFFBDBDBD),
        points: points,
        startCap: Cap.roundCap,
        endCap: Cap.roundCap,
        visible: true,
        width: 3,
        patterns: [PatternItem.dot, PatternItem.gap(5)],
      );
      i = i + 1;
      //  _polyline.add(polyline);
      polylines[polylineId] = polyline;

      // //print(points);
    });
  }

  _addLivePolyline() {
    int i = 0;

    int k = 0;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) async {
      List<LatLng> points = [];

      // _addliveMarkers(element);

      user.selGateway.gateWayMacId == element.siteId
          /*&&
                  element.tripStatus == 3 ||
              element.tripStatus == 6*/
          ? element.liveLatLongs.forEach((point) {
              points.add(point);
            })
          : points.clear();
      //print("This is set $points\n\n\n\n");

      widget.clear ? points.clear() : "";

      widget.clear ? element.liveLatLongs.clear() : [];

      widget.clear
          ? polylines.removeWhere(
              (key, value) => key.value.startsWith(RegExp('[0-9]')))
          : "";

      widget.clear = false;
      // lastpoint.isNotEmpty ? element.liveLatLongs.add(lastpoint[0]) : [];

      //  lastpoint.isNotEmpty ? points.add(lastpoint[0]) : [];

      final PolylineId polylineId = PolylineId("$k${element.deviceId}");

      var polyline = Polyline(
        polylineId: polylineId,
        //PolylineId(AquaUtils().getRandomString(5)),
        color: Color(0xFFFFC107),
        points: points,
        visible: true,
        startCap: Cap.roundCap,
        endCap: Cap.roundCap,
        width: 3,
        patterns: [PatternItem.dot, PatternItem.gap(5)],
      );
      i = i + 1;
      k = k + 1;
      // _livePolyline.add(polyline);
      polylines[polylineId] = polyline;
    });
  }
  /*int i = 0;
    List<LatLng> oldpoints = [];

    // user.selGateway.liveDeviceMap.values.toList().forEach((element) async {

    user.selGateway.gateWayMacId == widget.selDevice.liveModel.siteId &&
                widget.selDevice.liveModel.tripStatus == 3 ||
            widget.selDevice.liveModel.tripStatus == 6
        ? widget.selDevice.liveModel.liveLatLongs.forEach((point) {
            widget.selDevice.livepoints.add(point);
            // paths_array.add(point);
          })
        : widget.selDevice.liveModel.liveLatLongs.forEach((point) {
            oldpoints.add(point);
            // paths_array.add(point);
          });

    final PolylineId polylineId = PolylineId("${}");

    var polyline = Polyline(
      polylineId: polylineId,
      //PolylineId(AquaUtils().getRandomString(5)),
      color: Color(0xFFFFC107),
      points: widget.selDevice.livepoints,
      visible: true,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      width: 3,
      patterns: [PatternItem.dot, PatternItem.gap(5)],
    );
    i = i + 1;
    // _livePolyline.add(polyline);
    polylines[polylineId] = polyline;
    // });
  }*/

}
