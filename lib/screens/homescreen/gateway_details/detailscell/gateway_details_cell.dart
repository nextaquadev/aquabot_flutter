import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/drive_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/end_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/feed_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/home_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/pause.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/schedule.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/settings.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/spin_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/start_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/trip_info_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/tune_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/watch_sheet.dart';
import 'package:aquabot/screens/homescreen/gateway_details/detailscell/cellonview.dart';
import 'package:battery_indicator/battery_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/parser.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:rxdart/streams.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GatewayDetailsCell extends StatefulWidget {
  // const GatewayDetailsCell({Key key}) : super(key: key);
  BotModel cellDevice;
  final bool isSelected;

  final int index;
  bool isExpand;

  GatewayDetailsCell(
      {this.isSelected, this.cellDevice, this.index, this.isExpand});

  @override
  _GatewayDetailsCellState createState() => _GatewayDetailsCellState();
}

class _GatewayDetailsCellState extends State<GatewayDetailsCell> {
  AquaUser user = AquaUser.getInstance();
  //bool isExpand = false;
  var deviceId = "";
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return /*SizedBox(
      height: isExpand ? 180 : 90,
      child: Container(
        padding: EdgeInsets.fromLTRB(8, 0, 8, 10), //(bottom: 10),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey[300]),
            borderRadius: BorderRadius.all(
                Radius.circular(8.0) //                 <--- border radius here
                ),
          ),
          child:*/
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [getTopView(), getExpandView()]);
    // ),
    //  ),
    //);
  }

  Widget getTopView() {
    LiveBotModel lModel = widget.cellDevice.liveModel;
    var tripstatus;
    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (user.selGateway.botDevices[widget.index].macId == element.deviceId) {
        tripstatus = element.tripStatus;
      }

      // //print(element.deviceType);
    });

    // //print("This is tripstatus : ${tripstatus}");

    var wid = Row(mainAxisAlignment: MainAxisAlignment.start, children: [
      SizedBox(
        width: 50,
        child: Column(children: [
          Text(
            user.selGateway.botDevices[widget.index].deviceName,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                fontFamily: 'BrandonReg',
                color: widget.isSelected ? Color(0xFFFFC107) : Colors.black),
          ),
          add_epoch_time(user.selGateway.botDevices[widget.index].macId),
          add_version(user.selGateway.botDevices[widget.index].macId),
        ]),
      ),
      SizedBox(
        width: 8,
      ),
      Container(
          width: 1, color: Colors.grey, height: 50, child: VerticalDivider()),
      Container(
        child: StreamBuilder(
            stream: homeBloc.botDetailsRefreshStream,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              return getOnOffView();
            }),
      ),
      Container(
          width: 1, color: Colors.grey, height: 50, child: VerticalDivider()),
      SizedBox(width: 5),
      tripstatus == 3 || tripstatus == 6
          ? SizedBox(
              // width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 5),
                          Text(
                            'Kg',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              fontFamily: 'BrandonReg',
                            ),
                          ),
                          Icon(widget.isExpand
                              ? Icons.arrow_drop_up
                              : Icons.arrow_drop_down),
                        ],
                      ),
                      add_weight(user.selGateway.botDevices[widget.index].macId)
                    ],
                  ),
                ],
              ),
            )
          : SizedBox(
              // width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            'Last Trip',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              fontFamily: 'BrandonReg',
                            ),
                          ),
                          Icon(widget.isExpand
                              ? Icons.arrow_drop_up
                              : Icons.arrow_drop_down),
                        ],
                      ),
                      add_last_time(
                          user.selGateway.botDevices[widget.index].macId),
                      add_weight(user.selGateway.botDevices[widget.index].macId)
                    ],
                  ),
                ],
              ),
            ),
      SizedBox(
        width: 8,
      ),
    ]);
    return wid;
  }

  Widget getOnOffView() {
    LiveBotModel lModel = widget.cellDevice.liveModel;
    if (lModel != null && lModel.tripStatus != null) {
      //  //print(lModel.tripStatus);
      return lModel.tripStatus != 3 && lModel.tripStatus != 6
          ? lModel.tripStatus != 1 && lModel.tripStatus != 2
              ? getDeviceOffView()
              : Expanded(
                  child: Text(
                      "  Trip started by ${lModel.tripStatus == 1 ? "App" : "Dev"}\n  Trip will start in ${lModel.nextTripTime > 0 ? lModel.nextTripTime : ""} Min",
                      style: TextStyle(
                        fontFamily: 'BrandonReg',
                        fontWeight: FontWeight.bold,
                        // fontSize: 13,
                      )))
          : lModel.tripStatus != 6
              ? CellDeviceOnView(
                  selDevice: widget.cellDevice,
                )
              : Expanded(
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                        width: 70,
                        height: 30,
                        child: ElevatedButton(
                            child: Text(
                              "Resume",
                              style: TextStyle(
                                fontFamily: 'BrandonReg',
                                fontSize: 11,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                                primary: Colors.orangeAccent,
                                shape: StadiumBorder()),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                      child: Center(
                                          child: AlertDialog(
                                        title: Text(
                                            "Are you sure, you want to Resume ?",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15,
                                                fontFamily: 'BrandonReg')),
                                        content: SizedBox(
                                          width: 100,
                                          child: Container(
                                            height: 35,
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    GestureDetector(
                                                        child: Text("Cancel",
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'BrandonReg')),
                                                        onTap: () {
                                                          Navigator.pop(
                                                              context, false);
                                                        }),
                                                    GestureDetector(
                                                      child: Text("Ok",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'BrandonReg')),
                                                      onTap: () {
                                                        sendresume(
                                                            user
                                                                .selGateway
                                                                .botDevices[
                                                                    widget
                                                                        .index]
                                                                .macId,
                                                            "resumeTrip");
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )),
                                    );
                                    ;
                                  });
                            }))
                  ],
                ));
    }
    return getDeviceOffView();
  }

  onPressOfOk(message, color) {
    Navigator.pop(context);
    var alert = showDialog(
        context: context,
        builder: (BuildContext context) {
          return Alert(message, color, "Resume Settings");
        });
    return alert;
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();

    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  sendresume(deviceId, methodName) async {
    var data;
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/${deviceId}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
    var body = "";
    try {
      var response = await http.post(path,
          body: "", headers: header, encoding: Encoding.getByName("utf-8"));
      setState(() {
        data = response;
      });

      // onPressOfOk("Command sent successfully", Colors.green);
      if (response.statusCode == 200) {
        getResponseBasedOnDevice("", 200);
      } else {
        getResponseBasedOnDevice("", 500);
      }
    } catch (error) {
      //print(error);
      getResponseBasedOnDevice("", 500);
      // onPressOfOk("Sending command failed", Colors.red);
    }
    return data;
  }

//Device Off View
  Widget getDeviceOffView() {
    var wid = Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          getButton('START', () {
            onTapOfStart();
          }),
          getButton('HOME', () {
            onTapOfHome();
          }),
          /* Column(
            children: [
              /*  BatteryIndicator(
                style:BatteryIndicatorStyle.values[skeumorphism],
                colorful:Colors.green,

                
              ),*/
              /*  CircularPercentIndicator(
                  radius: 35.0,
                  lineWidth: 5.0,
                  percent: value,
                  center: percent == null || percent == ""
                      ? Text("")
                      : Text(
                          "$percent%",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 10,
                              fontFamily: 'BrandonReg',
                              color: Colors.black),
                        ),
                  backgroundColor: Colors.grey,
                  progressColor: Colors.green),*/
              /*  Icon(
                Icons.battery_std,
                color: Colors.green,
              ),*/*/
          add_battery_status(user.selGateway.botDevices[widget.index].macId),
          //   ],
          //  ),
          // add_battery_status(user.selGateway.botDevices[widget.index].macId),
        ],
      ),
    );
    return wid;
  }

  onTapOfStart() {
    var openBootomSheet = showModalBottomSheet(
        //height:400,
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        context: context,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        builder: (context) => Start(widget.index));
    return openBootomSheet;
  }

  onTapOfHome() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return HomeSheet(widget.index);
        });
  }

  Widget getButton(String title, onPressed) {
    Widget wid = SizedBox(
        width: 70,
        height: 30,
        child: ElevatedButton(
          child: Text(
            title,
            style: TextStyle(
              fontFamily: 'BrandonReg',
              fontSize: 11,
            ),
          ),
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              primary: Color(0xFF00E676), shape: StadiumBorder()),
        ));

    /* RaisedButton(
      shape: StadiumBorder(),
      onPressed: () {},
      child: Text(title),
      color: Color.fromRGBO(0, 255, 200, 1),
    );*/
    return wid;
  }

//List Bottom View
  Widget getExpandView() {
    var user_role = user.userRole;

    Widget wid = widget.isSelected && widget.cellDevice.isExpand
        ? SizedBox(
            height: 100,
            child: Column(
              children: [Divider(color: Colors.grey), getListView()],
            ),
          )
        : SizedBox();
    return wid;
  }

  Widget getListView() {
    var device_type;
    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (user.selGateway.botDevices[widget.index].macId == element.deviceId) {
        device_type = element.deviceType;
      }

      // //print(element.deviceType);
    });
    // //print(device_type);

    List<Map> listItems1 = [
      {
        'title': Trip_info(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/tripinfo.png',
        'index': 'tripinfo'
      },
      {
        'title': Feed(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/feed.png',
        'index': 'feed'
      },
      device_type == "Line"
          ? {
              'title': Settings(
                index: widget.index,
                isSelected: widget.isSelected,
                cellDevice: widget.cellDevice,
              ),
              'asset': 'images/settings.png',
              'index': 'line'
            }
          : {},
       user.phoneNumber == '9985796749' ||
                                      user.userName == 'NextAqua'
                                  ? {
        'title': Tune(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/tune.png',
        'index': 'tune'
      } : {},
      {
        'title': Watch(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/watch.png',
        'index': 'watch'
      },
      {
        'title': Drive(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/drive.png',
        'index': 'drive'
      },
      {
        'title': Schedule(widget.index),
        'asset': 'images/schedule_icon.png',
        'index': 'schedule'
      },
      {
        'title': Spin(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/spin.png',
        'index': 'spin'
      },
      {
        'title': Pause(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/Pause_icon.png',
        'index': 'pause'
      },
      {
        'title': End(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/end.png',
        'index': 'end'
      },
    ];
    List<Map> listItems2 = [
      {
        'title': Trip_info(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/tripinfo.png',
        'index': 'tripinfo'
      },
      device_type == "Line"
          ? {
              'title': Settings(
                index: widget.index,
                isSelected: widget.isSelected,
                cellDevice: widget.cellDevice,
              ),
              'asset': 'images/settings.png',
              'index': 'settings'
            }
          : {},
      {
        'title': Drive(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/drive.png',
        'index': 'drive'
      },
      {
        'title': Schedule(widget.index),
        'asset': 'images/schedule_icon.png',
        'index': 'schedule'
      },
      {
        'title': Spin(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/spin.png',
        'index': 'spin'
      },
      {
        'title': Pause(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/Pause_icon.png',
        'index': 'pause'
      },
      {
        'title': End(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/end.png',
        'index': 'end'
      },
    ];
    List<Map> listItems3 = [
      {
        'title': Drive(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/drive.png',
        'index': 'drive'
      },
      {
        'title': Pause(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/Pause_icon.png',
        'index': 'pause'
      },
      {
        'title': End(
          index: widget.index,
          isSelected: widget.isSelected,
          cellDevice: widget.cellDevice,
        ),
        'asset': 'images/end.png',
        'index': 'end'
      },
    ];
    var listview = user.userRole == "Owner"
        ? Wrap(
            alignment: WrapAlignment.spaceBetween,
            spacing: 5.0,
            children: listItems1.map((e) => getCircleButton(e)).toList())
        : user.userRole == "View"
            ? Container(
                margin: EdgeInsets.only(top: 20),
                child: Wrap(
                    alignment: WrapAlignment.spaceBetween,
                    spacing: 80.0,
                    children:
                        listItems3.map((e) => getCircleButton(e)).toList()),
              )
            : Wrap(
                alignment: WrapAlignment.spaceBetween,
                spacing: 5.0,
                children: listItems2.map((e) => getCircleButton(e)).toList());

    /*
    ListView.separated(
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return getCircleButton(listItems[index]);
        },
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            width: 8,
          );
        },
        itemCount: listItems.length);
        */
    return listview;
  }

  sendCommandDirectlyToDevice2(command) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse("https://mdash.net/api/v2/devices/" +
        widget.cellDevice.macId +
        "/rpc/setContrl?access_token=JKrRbHr8MnhdUUso91Zmm91A");
    // setState(() {
    //   sentApi = path;
    // });
    var encodedCommand = jsonEncode(command);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    //print(response);
    var convertedData = json.decode(response.body);
    // setState(() {
    //   gotResponse = convertedData;
    // });
    return response;
  }

  sendStopMotor() {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var config = {
      'frametype': "setContrl",
    };
    var configCmd = {
      'version': version,
      'deviceId': widget.cellDevice.macId,
      'RudderMotorSpeed': 0,
      'DriveMotorSpeed': 0,
      'RudderMotorDirection': 1
    };
    //config['commands'] = [configCmd];
    var finalObj = {'frametype': "setContrl", 'commands': configCmd};

    try {
      var responseData = sendCommandDirectlyToDevice2(configCmd);
      // showOverlay(finalObj);
      // //print(responseData.body);
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.green;
      //   toastName = "Success";
      // });
    } catch (error) {
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.red;
      //   toastName = "Failed";
      // });
      //print(error);
    }
  }

  Widget getCircleButton(Map titleMap) {
    Widget wid = titleMap.isNotEmpty
        ? Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[350]),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: GestureDetector(
                onTap: () {
                  titleMap['index'] == "pause" || titleMap['index'] == "end"
                      ? showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return titleMap['title'];
                          })
                      : showModalBottomSheet(
                          // isDismissible: titleMap['index'] == "drive" ? false : true,
                          enableDrag: true,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20),
                            ),
                          ),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          backgroundColor: titleMap['index'] == "tripinfo" ||
                                  titleMap['index'] == "tune"
                              ? Colors.transparent
                              : Color(0xFFE8EAF6),
                          isScrollControlled: true,
                          context: context,
                          builder: (context) => titleMap['index'] ==
                                      "tripinfo" ||
                                  titleMap['index'] == "tune"
                              ? titleMap[
                                  'title'] /*makeDismissible(
                                  child: DraggableScrollableSheet(
                                      initialChildSize: 0.5,
                                      minChildSize: 0.3,
                                      maxChildSize: 0.9,
                                      builder: (_, controller) =>
                                          /* SingleChildScrollView(
                                              scrollDirection: Axis.vertical,
                                              controller: controller,
                                              child:*/
                                          Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(20),
                                                  topRight: Radius.circular(20),
                                                ),
                                                color: Color(0xFFE8EAF6),
                                              ),
                                              child: titleMap['title'])))*/
                              : Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.5,
                                  child: Center(child: titleMap['title']),
                                )).whenComplete(() {
                          if (titleMap['index'] == "drive") {
                            sendStopMotor();
                          }
                        });
                },
                /*  showBarModalBottomSheet(
              barrierColor: Color.fromRGBO(0, 0, 0, 0.3),
              context: context,
              builder: (context) => Container(
                height: 400,
                child: Center(child: Text(titleMap['title'])),
              ),


            );
          },*/
                child: Image.asset(titleMap['asset'],
                    height: 40, fit: BoxFit.fill)),
          )
        : SizedBox();
    return wid;
  }

  add_epoch_time(String macId) {
    var time;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (macId == element.deviceId) {
        time = element.epochTime;
      }
    });

    return time == null || time == ""
        ? Text("")
        : Text(
            "${DateFormat('dd/MM-HH:mm').format(DateTime.fromMillisecondsSinceEpoch(time * 1000))}",
            style: TextStyle(
                fontSize: 8,
                // fontWeight: FontWeight.bold,
                fontFamily: 'BrandonReg'),
          );
  }

  add_version(String macId) {
    var version;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (macId == element.deviceId) {
        var data = element.version;
        var list = data.split("_");
        version = list.removeLast();
      }
    });

    return version == null || version == ""
        ? Text("")
        : Text(
            "($version)",
            style: TextStyle(
                // fontWeight: FontWeight.bold,
                fontSize: 10,
                fontFamily: 'BrandonReg',
                color: Colors.black),
          );
  }

  add_battery_status(String macId) {
    var percentage;
    var i;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (macId == element.deviceId) {
        var data = element.voltage;
        var a = data - 11.5;
        var b = 2;
        var total = ((a / b) * 100);
        if (total > 100) {
          percentage = "100";
        } else if (total < 0) {
          percentage = "0";
        } else {
          percentage = (total).toStringAsFixed(0);
        }
        var value = double.parse(percentage);
        i = value / 100;
      }
    });

    return percentage == null || percentage == ""
        ? Text("")
        : CircularPercentIndicator(
            radius: 35.0,
            lineWidth: 5.0,
            percent: i,
            center: Text(
              "$percentage%",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 10,
                  fontFamily: 'BrandonReg',
                  color: Colors.black),
            ),
            backgroundColor: Colors.grey,
            progressColor: i < 0.15
                ? Colors.red
                : 0.30 < i
                    ? Colors.green
                    : Colors.orange);

    /*Text(
            "$percentage%",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 10,
                fontFamily: 'BrandonReg',
                color: Colors.black),
          );*/
  }

  add_weight(String macId) {
    String current_weight;
    String actual_weight;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (macId == element.deviceId) {
        String a = (element.current_Weight).toStringAsFixed(1);
        String b = (element.weight_to_disburse).toStringAsFixed(1);

        current_weight = a;
        actual_weight = b;
      }
    });

    return current_weight == null || current_weight == ""
        ? Text("")
        : Text(
            "$current_weight/$actual_weight kg",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 10,
                fontFamily: 'BrandonReg',
                color: Colors.black),
          );
  }

  add_last_time(String macId) {
    var tripId;

    user.selGateway.liveDeviceMap.values.toList().forEach((element) {
      if (macId == element.deviceId) {
        var data = element.tripId;
        tripId = data;
      }
    });

    return tripId == null || tripId == ""
        ? Text("")
        : Text(
            "${DateFormat('dd/MM-HH:mm').format(DateTime.fromMillisecondsSinceEpoch(tripId * 1000))}",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 8,
                fontFamily: 'BrandonReg',
                color: Colors.black),
          );
  }

  Widget makeDismissible({Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(onTap: () {}, child: child),
      );

  /* Future pickDateTime(BuildContext context) async {
    final date = await pickDate(context);
    if (date == null) return;
    setState(() {
      dateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
        context: context,
        initialDate: dateTime ?? initialDate,
        firstDate: DateTime(DateTime.now().year - 5),
        lastDate: DateTime.now());
    if (newDate == null) {
      return initialDate;
    } else {
      return newDate;
    }
  }*/
}
