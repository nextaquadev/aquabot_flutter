import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:flutter/material.dart';

class CellDeviceOnView extends StatefulWidget {
  final BotModel selDevice;
  CellDeviceOnView({this.selDevice});

  @override
  State<CellDeviceOnView> createState() => _CellDeviceOnViewState();
}

class _CellDeviceOnViewState extends State<CellDeviceOnView> {
  var totaldistance;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.55,
      padding: EdgeInsets.only(left: 4, right: 4),
      child: getDeviceOnView(context),
    );
  }

  Widget getDeviceOnView(BuildContext context) {
    var spincounter;
    var etest;
    var feederstatus;
    LiveBotModel lModel = widget.selDevice.liveModel;
    if (lModel != null) {
      spincounter = lModel.spinCounter;
      etest = lModel.etestState;
      feederstatus = lModel.feederStatus;
    }
    var wid = Column(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          getPostValue(),
          Row(
            children: [
              getLeftPost(),
              SizedBox(
                height: 20,
                width: 140,
                child: getSlider(context),
              ),
              getRightPost()
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                feederstatus == 1
                    ? "Feed"
                    : etest != 0
                        ? "Etest"
                        : "",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.green,
                  fontFamily: 'BrandonReg',
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                feederstatus == 0 && etest == 0 ? "Empty" : "",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.green,
                  fontFamily: 'BrandonReg',
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                spincounter != -1 ? "Spinning" : "",
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xFFFFC107),
                  fontFamily: 'BrandonReg',
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "${totaldistance.toStringAsFixed(2)}m",
                style: TextStyle(
                  fontSize: 10,
                  color: Color(0xFFFFC107),
                  fontFamily: 'BrandonReg',
                ),
              )
            ],
          )
        ]);
    /* mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          //  alignment: Alignment.center,
          children: [
            Positioned(top: 1, left: 30, right: 10, child: getPostValue()),
            Positioned(child: getLeftPost()),
            Positioned(child: getSlider(context)),
            Positioned(child: getRightPost()),
          ],
        ),
        /*  getPostValue(),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [getLeftPost(), getSlider(context), getRightPost()]),
        Text("Feed",
            style: TextStyle(
              color: Colors.green,
              fontFamily: 'BrandonReg',
            ))*/*/

    return wid;
  }

  Widget getPostValue() {
    double distanceFrmPost = 0;
    var waitTimecounter;
    var deviceType;

    LiveBotModel lModel = widget.selDevice.liveModel;
    if (lModel != null) {
      distanceFrmPost = double.parse(lModel.distanceFrmPost);
      waitTimecounter = lModel.waitTimeCounter;
      deviceType = lModel.deviceType;
    }
    var wid = Align(
      child: Text(
        deviceType == 'Line' && waitTimecounter > 0
            ? " Waiting"
            : distanceFrmPost.toStringAsFixed(2) + "m",
        style: TextStyle(
          color: Color(0xFFFFC107),
          fontFamily: 'BrandonReg',
        ),
      ),
      alignment: Alignment.center,
    );
    return wid;
  }

  Widget getLeftPost() {
    String leftPostName = '0';

    LiveBotModel lModel = widget.selDevice.liveModel;
    if (lModel != null) {
      leftPostName = (lModel.currentPost + 1).toString();
    }
    var wid = Container(
        height: 30,
        width: 25,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xFFFFC107)),
          shape: BoxShape.circle,

          /*  borderRadius: BorderRadius.all(
              Radius.circular(20) //                 <--- border radius here
              ),*/
        ),
        child: Text(
          leftPostName,
          style: TextStyle(
            fontFamily: 'BrandonReg',
            color: Color(0xFFFFC107),
          ),
        ));
    return wid;
  }

  Widget getRightPost() {
    String rightPostName = '1';

    LiveBotModel lModel = widget.selDevice.liveModel;
    if (lModel != null) {
      rightPostName = (lModel.currentPost + 2).toString();
    }
    var wid = Container(
        padding: EdgeInsets.all(3),
        height: 30,
        // width: MediaQuery.of(context).size.width * 0.1,

        width: 25,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Color(0xFFFFC107)),
          /*  borderRadius: BorderRadius.all(
              Radius.circular(8) //                 <--- border radius here
              ),*/
        ),
        child: Text(
          rightPostName,
          style: TextStyle(
            fontFamily: 'BrandonReg',
            color: Color(0xFFFFC107),
          ),
        ));
    return wid;
  }

  Widget getSlider(BuildContext context) {
    double distanceBetweenPosts = 0;
    double distanceFrmPost = 0;

    LiveBotModel lModel = widget.selDevice.liveModel;
    if (lModel != null) {
      distanceBetweenPosts = double.parse(lModel.distanceBetweenPosts);
      distanceFrmPost = double.parse(lModel.distanceFrmPost);
      setState(() {
        totaldistance = distanceBetweenPosts;
      });
    }
    var wid = SliderTheme(
      data: SliderTheme.of(context).copyWith(
          overlayShape: SliderComponentShape.noOverlay,
          activeTrackColor: Colors.yellow[700],
          inactiveTrackColor: Colors.grey,
          trackShape: RectangularSliderTrackShape(),
          trackHeight: 4.0,
          thumbColor: Colors.yellow[700],
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 3.0),
          showValueIndicator: ShowValueIndicator.always,
          valueIndicatorShape: PaddleSliderValueIndicatorShape(),
          valueIndicatorColor: Colors.yellow[700],
          valueIndicatorTextStyle: TextStyle(
            color: Colors.white,
          )),
      child: Slider(
        min: 0,
        label: distanceFrmPost.toString(),
        max: distanceBetweenPosts,
        value: distanceFrmPost,
        onChanged: (value) {},
      ),
    );
    return wid;
  }
}
