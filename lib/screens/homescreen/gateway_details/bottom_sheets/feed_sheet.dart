import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Feed extends StatefulWidget {
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Feed({this.isSelected, this.cellDevice, this.index});
  // const Feed({ Key? key }) : super(key: key);

  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  AquaUser user = AquaUser.getInstance();
  var deviceId = "";
  var data;
  var selectedValue;
  var enteredFeedRate = null;
  var enteredSpinDuration = null;
  var feeds = [];
  var selectedFeedType = "Avanthi-CP";
  var enteredValues;
  var enteredApi;
  var enteredFeedType;
  var displayToast = false;
  var toastColor = Colors.green;
  var toastName = "";
  bool recievedData = false;
  bool isOffline = false;
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");

  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    // getFeedRateDetails(deviceId);
    getFeedConfig();
    super.initState();
  }

  getFeedRateDetails(deviceId) async {
    DocumentReference firebaseData = await FirebaseFirestore.instance
        .collection("deviceFeedRates")
        .doc("${deviceId}");

    firebaseData.get().then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        ////print(documentSnapshot.data());
        var data1 = documentSnapshot.data();
        ////print(data1['feedRate']);
        setState(() {
          // data = {
          //   'selectedValue': data1['feedType'],
          //   'enteredFeedRate':data1['feedRate'],
          //   'enteredSpinDuration': data1['spinDuration'],
          // };
          data = data1;
        });

        setState(() {
          selectedFeedType = data['feedType'];
        });

        setState(() {
          enteredFeedRate = data['feedRate'];
          enteredSpinDuration = data['spinDuration'];
        });
      } else {
        if (enteredFeedRate == null && enteredSpinDuration == null) {
          setState(() {
            enteredSpinDuration = 0;
            enteredFeedRate = 0;
          });
        }
      }
    });
  }

  // saveFeedRateDetails(deviceId) async {
  //   var data = {
  //     'feedRate':
  //         enteredFeedRate != null ? double.parse("$enteredFeedRate") : 0,
  //     'feedType': enteredFeedType != null ? enteredFeedType : "",
  //     'spinDuration':
  //         enteredSpinDuration != null ? int.parse("$enteredSpinDuration") : 0
  //   };
  //   DocumentReference firebaseData = await FirebaseFirestore.instance
  //       .collection("deviceFeedRates")
  //       .doc("${deviceId}")
  //       .set(data)
  //       .then((value) => null);

  //   // firebaseData.get().then((DocumentSnapshot documentSnapshot) {
  //   //   if(documentSnapshot.exists){
  //   //     //print(documentSnapshot.data());
  //   //   }
  //   // });
  // }

  sendDataToDevice() {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    // if(selectedValue == "Feeds Not available" || selectedValue == "" ){
    //     return;
    // }
    var obj = {
      'version': version,
      'deviceId': deviceId,
      'feedType': enteredFeedType != null ? enteredFeedType : "",
      'feedRate':
          enteredFeedRate != null ? double.parse("$enteredFeedRate") : 0,
      'spinDuration':
          enteredSpinDuration != null ? int.parse("$enteredSpinDuration") : 0
    };
    setState(() {
      enteredValues = obj;
      recievedData = false;
    });
    //print(obj);
    var finalObj = {'frametype': "setFeedConfig", 'commands': obj};
    // showOverlay(finalObj);
    sendCommandDirectlyToDevice(obj, "setFeedConfig");
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();
    setState(() {
      recievedData = true;
    });
    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  sendCommandDirectlyToDevice(command, methodName) async {
    try {
      //print(methodName);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${deviceId}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      setState(() {
        enteredApi = path;
      });
      var encodedCommand = jsonEncode(command);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        //print(convertedData);
        // saveFeedRateDetails(deviceId);
        // setState(() {
        //     displayToast = true;
        //     toastColor = Colors.green;
        //     toastName = "Success";
        //   });
        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return Alert(
        //           "Command sent successfully", Colors.green, "Feed Settings");
        //     });
        getResponseBasedOnDevice("", 200);
        // Fluttertoast.showToast(msg: "Success", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.greenAccent, textColor: Colors.white, fontSize: 16.0);
      } else {
        // print(response['error']['message']);
        // saveFeedRateDetails(deviceId);
        getResponseBasedOnDevice("", 500);
      }
    } catch (error) {
      //print(error);
      getResponseBasedOnDevice("", 500);
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert("Sending Command failed", Colors.red, "Feed Settings");
      //     });
      // setState(() {
      //       displayToast = true;
      //       toastColor = Colors.red;
      //       toastName = "Failed";
      // });
      // Fluttertoast.showToast(msg: "Failed", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.redAccent, textColor: Colors.white, fontSize: 16.0);
      // saveFeedRateDetails(deviceId);
    }
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  getFeedConfig() async {
    setState(() {
      recievedData = false;
    });
    //2021-12-16 09:16:45.238553
    var dateT = DateTime.now();
    //print(dateT);
    //print(dateT.hour);
    try {
      var url = Uri.parse(
          "https://mdash.net/api/v2/devices/$deviceId/rpc/getFeedConfig?access_token=5yvbTOSq90iYY992mIg3wOhA");
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var response = await http.post(url);
      //print(response.body);
      if (response.statusCode == 500) {
        var body = jsonDecode(response.body) as Map;
        //print(body['error']);
        setState(() {
          isOffline = true;
          recievedData = true;
          // showError = body['error']['message'];
        });
        // showDialog(
        //   context: context,
        //   builder: (BuildContext context) {
        //     return CommandAlert("${body['error']['message']}", Colors.green,
        //         "Apfc Config");
        //   });
      } else {
        if (response.statusCode == 200) {
          var decodedData = jsonDecode(response.body);
          //print(response.body);
          setState(() {
            enteredSpinDuration = decodedData['spinDuration'] != null
                ? decodedData['spinDuration']
                : 10;
            enteredFeedRate = decodedData['feedRate'] != null
                ? decodedData['feedRate']
                : 0.25;
            selectedFeedType =
                decodedData['feedType'] != null ? decodedData['feedType'] : "";
            enteredFeedType =
                decodedData['feedType'] != null ? decodedData['feedType'] : "";
            recievedData = true;
            isOffline = false;
          });
        } else {
          setState(() {
            isOffline = true;
            recievedData = true;
            // showError = body['error']['message'];
          });
        }
      }
    } catch (error) {
      //print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    var feedsList = user.selGateway.feeds;
    List feeds = [];
    feedsList.map((ele) {
      if (ele['keyCode'] != enteredFeedType) {
        feeds.add(enteredFeedType);
      }
      feeds.add(ele['keyCode']);
    }).toList();
    if (feedsList.length == 0) {
      feeds.add(enteredFeedType);
    }
    feeds = feeds.toSet().toList();
    //print(feeds);
    // if (enteredFeedType == null) {
    //   setState(() {
    //     enteredFeedType = feeds[0];
    //   });
    // }
    //print(selectedFeedType);
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    var emptyFeedType = ["Feeds Not available"];
    //print(data);
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: recievedData == false
          ? SpinKitThreeBounce(
              size: 30.0,
              color: Color(0xFF0170d0),
            )
          : isOffline == false
              ? Container(
                  alignment: Alignment.topCenter,

                  margin: EdgeInsets.only(left: 20, right: 20),
                  //   height: 400,
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("${deviceName}-(${deviceId})",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      fontFamily: 'BrandonReg')),
                            ],
                          ),
                          Divider(
                            thickness: 1,
                            color: Colors.black,
                          ),
                          Text("Feed Type",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  fontFamily: 'BrandonReg',
                                  color: Color(0xFF05375a))),
                          SizedBox(
                            height: 5,
                          ),
                          feeds.length != 0
                              ? DropdownButton(
                                  underline: Container(
                                    color: Colors.redAccent,
                                  ),
                                  value: enteredFeedType,
                                  onChanged: (newValue) {
                                    setState(() {
                                      enteredFeedType = newValue;
                                    });
                                  },
                                  items: feeds.map((ele) {
                                    return DropdownMenuItem(
                                      child: new Text(ele),
                                      value: ele,
                                    );
                                  }).toList(),
                                )
                              : DropdownButton(
                                  underline: Container(
                                    color: Colors.redAccent,
                                  ),
                                  value: enteredFeedType,
                                  onChanged: (newValue) {
                                    setState(() {
                                      enteredFeedType = newValue;
                                    });
                                  },
                                  items: emptyFeedType.map((ele) {
                                    return DropdownMenuItem(
                                      child: new Text(ele),
                                      value: ele,
                                    );
                                  }).toList(),
                                ),
                          Text("Feed Rate in (Kg)",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  fontFamily: 'BrandonReg',
                                  color: Color(0xFF05375a))),
                          enteredFeedRate != null
                              ? TextFormField(
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFFf2f2f2))),
                                    hintText: "Feed Rate",
                                    //labelText: "$data['waitTime']",
                                  ),
                                  keyboardType: TextInputType.number,
                                  initialValue: "$enteredFeedRate",
                                  inputFormatters: [
                                    CustomRangeTextInputFormatter(1.5),
                                  ],
                                  onChanged: (text) {
                                    var itm = enteredFeedRate;
                                    // if(int.parse(text) <= 2){
                                    // setState(() {
                                    enteredFeedRate = text;
                                    // });
                                    // }
                                  },
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Text("Spin Duration in (Sec)",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  fontFamily: 'BrandonReg',
                                  color: Color(0xFF05375a))),
                          enteredFeedRate != null
                              ? TextFormField(
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFFf2f2f2))),

                                    //labelText: "$data['waitTime']",
                                  ),
                                  inputFormatters: [
                                    CustomRangeTextInputFormatter(15),
                                  ],
                                  initialValue: "$enteredSpinDuration",
                                  onChanged: (text) {
                                    var itm = enteredFeedRate;
                                    // if(int.parse(text) <= 30){
                                    //   setState(() {
                                    enteredSpinDuration = text;
                                    //   });
                                    // }
                                  },
                                  keyboardType: TextInputType.number,
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                // displayToast == true ? Container(
                                //   height: 30,
                                //   width: 220,
                                //   child: RenderToast(toastName, toastColor, (){ setState(() {
                                //     displayToast = false;
                                //   }); }),
                                // ) : SizedBox(),
                                CommandButton("Send", Colors.blueAccent, () {
                                  sendDataToDevice();
                                }),
                              ],
                            ),
                          )
                        ]),
                  ),
                )
              : Center(
                  child: Text(
                  "Device is Offline",
                  style: TextStyle(fontFamily: 'Brandon', fontSize: 14),
                )),
    );
  }
}

class CustomRangeTextInputFormatter extends TextInputFormatter {
  var maxVal;
  CustomRangeTextInputFormatter(this.maxVal);
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (maxVal is int) {
      if (newValue.text == '')
        return TextEditingValue(
            text: '', selection: TextSelection.collapsed(offset: 0));
      else if (int.parse(newValue.text) < 0)
        return TextEditingValue().copyWith(text: '');

      return int.parse(newValue.text) > maxVal
          ? TextEditingValue().copyWith(text: "$maxVal")
          : newValue;
    } else {
      if (newValue.text == '')
        return TextEditingValue(
            text: '', selection: TextSelection.collapsed(offset: 0));
      else if (double.parse(newValue.text) < 0)
        return TextEditingValue().copyWith(text: '0');

      return double.parse(newValue.text) > maxVal
          ? TextEditingValue().copyWith(text: "$maxVal")
          : newValue;
    }
  }
}
