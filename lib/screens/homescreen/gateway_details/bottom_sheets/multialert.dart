import 'package:flutter/material.dart';

class MultiAlert extends StatelessWidget {
  // const Alert({ Key? key }) : super(key: key);
  final String title;
  final Color color;
  final String header;
  final Function() onPressOfOk;
  final Function() onPressOfCancel;
  MultiAlert(this.title, this.color, this.header, this.onPressOfOk,
      this.onPressOfCancel);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      child: Center(
          child: AlertDialog(
        /* title: Text("$title",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color:color,
                fontFamily: 'BrandonReg')),*/
        content: SizedBox(
          // width: 100,
          child: Container(
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$header",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      color: Colors.black,
                      fontFamily: 'BrandonReg'),
                ),
                // Text("$title",
                //     style: TextStyle(
                //         fontWeight: FontWeight.bold,
                //         fontSize: 15,
                //         color: color,
                //         fontFamily: 'BrandonReg')),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        child: Container(
                          height: 25,
                          width: 70,
                          //color: Colors.red,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            border: Border.all(color: Colors.red),
                            borderRadius: BorderRadius.all(Radius.circular(
                                    20) //                 <--- border radius here
                                ),
                          ),
                          child: Center(
                            child: Text("Cancel",
                                style: TextStyle(
                                    fontFamily: 'BrandonReg',
                                    color: Colors.white)),
                          ),
                        ),
                        onTap: onPressOfCancel),
                    GestureDetector(
                        child: Container(
                          height: 25,
                          width: 70,
                          //color: Colors.blueAccent,
                          decoration: BoxDecoration(
                            color: Colors.blueAccent,
                            border: Border.all(color: Colors.blueAccent),
                            borderRadius: BorderRadius.all(Radius.circular(
                                    15) //                 <--- border radius here
                                ),
                          ),
                          child: Center(
                            child: Text("Ok",
                                style: TextStyle(
                                    fontFamily: 'BrandonReg',
                                    color: Colors.white)),
                          ),
                        ),
                        onTap: onPressOfOk),
                  ],
                )
              ],
            ),
          ),
        ),
      )),
    );
  }
}
