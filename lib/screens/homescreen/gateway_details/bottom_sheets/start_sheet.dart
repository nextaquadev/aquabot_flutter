import 'dart:convert';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
//import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/send_command.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'command_button.dart';
import 'package:http/http.dart' as http;

class Start extends StatefulWidget {
  final int index;
  Start(this.index);
  // const Start({ Key? key }) : super(key: key);

  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  bool feedType = false;
  var feedqty = 25.0;
  var dateInTimeFormat = 0;
  var deviceId;
  AquaUser user = AquaUser.getInstance();
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    super.initState();
  }

  renderText(name, fontSize, fontWeight) {
    var text = Text(name,
        style: TextStyle(
            fontWeight: fontWeight,
            fontSize: fontSize,
            fontFamily: 'BrandonReg',
            color: Colors.black));
    return text;
  }

  renderTextField(_placeHolder, _labelText, color, onChanged) {
    var textField = TextFormField(
      decoration: InputDecoration(
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: color)),
        hintText: _placeHolder,
        //labelText: _labelText,
      ),
      initialValue: _labelText,
      onChanged: onChanged,
      keyboardType: TextInputType.number,
    );
    return textField;
  }

  onSwitchChanged() {
    if (feedType == true) {
      setState(() {
        feedType = false;
      });
    } else {
      setState(() {
        feedType = true;
      });
    }
  }

  onChangeOfFeedWght(text) {
    setState(() {
      feedqty = double.parse(text);
    });
    //print(feedqty);
  }

  onChangeOfTripTime(text) {
    var value = int.parse("$text");
    setState(() {
      dateInTimeFormat = value;
    });
  }

  sendCommandDirectlyToDevice(command, methodName, deviceId) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/$deviceId/rpc/$methodName?access_token=LRoyC90hJjxuoklY7rPAH5Q');
    // setState(() {
    //   enteredApi = path;
    // });
    ////print(enteredApi);
    //print(command);
    var encodedCommand = jsonEncode(command);
    //print(encodedCommand);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    var convertedData = jsonDecode(response.body);
    //print(convertedData);
    //print(response.statusCode);
    return response;
  }

  Widget RenderConfigStatusBS(newobj, List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var obj = newobj;
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200 &&
                                    (obj != null && obj['ReasonForFail'] == 1)
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
          obj != null
              ? Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(obj['ReasonForFail'] == -1
                      ? "Weight is less than or equal to zero"
                      : obj['ReasonForFail'] == -2
                          ? "Route Not Found"
                          : obj['ReasonForFail'] == -3
                              ? "Battery Low"
                              : obj['ReasonForFail'] == 1
                                  ? "DistanceToTravel : ${obj['fDistanceToTravel']}\nWeightToDisburse : ${obj['fWeightToDisburse']}"
                                  : ""),
                )
              : SizedBox(),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var newobj = scheduleObj;
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();

    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(newobj, successListaf));
  }

  homeTrip(device) async {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;

    var config = {
      'frametype': "homeTrip",
    };
    //prepare control command data object
    var configCmd = {
      "version": version,
      "deviceId": deviceId,
    };
    try {
      var responseData =
          await sendCommandDirectlyToDevice(configCmd, 'homeTrip', deviceId);
      if (responseData.statueCode == 200) {
        // //print(responseData.body);
        //print("Home trip");
      }
      if (responseData.statusCode == 200) {
        getResponseBasedOnDevice("", 200);
      } else {
        getResponseBasedOnDevice("", 500);
      }
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert(
      //           "Command sent successfully", Colors.green, "Start Settings");
      //     });
    } catch (error) {
      //print(error);
      getResponseBasedOnDevice("", 500);
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert(
      //           "Sending commend failed", Colors.red, "Start Settings");
      //     });
    }
    // config['commands'] = [configCmd]
  }

  startTrip() async {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    Map config = {
      'frametype': "startTrip",
    };
    //print(deviceId);
    var tripid = DateFormat("HHmmssddMM").format(DateTime.now());
    var configCmd = {
      "version": version,
      "type": feedType == true ? 1 : 0,
      "deviceId": deviceId,
      "tripID": int.parse("$tripid"),
      "weighToDisburset": feedqty,
      "nextTripTime": dateInTimeFormat
    };
    //print("hello");
    //print(configCmd);
    config['commnds'] = [configCmd];
    try {
      var responseData =
          await sendCommandDirectlyToDevice(configCmd, 'startTrip', deviceId);
      // if(responseData == 200){
      //   //print(responseData.body);
      // }
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert(
      //           "Command sent successfully", Colors.green, "Start Settings");
      //     });
      if (responseData.statusCode == 200) {
        getResponseBasedOnDevice(jsonDecode(responseData.body), 200);
      } else {
        getResponseBasedOnDevice(null, 500);
      }
      //print(responseData.body);
      //print("Failed");
      //print("startTrip");
    } catch (error) {
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert(
      //           "Sending commend failed", Colors.red, "Start Settings");
      //     });
      getResponseBasedOnDevice("", 500);
      //print(error);
    }
  }

  startBtnHandler() {
    startTrip();
  }

  onTapOfSend() {
    if (feedqty > 0) {
      startBtnHandler();
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        margin: EdgeInsets.only(top: 30, right: 25, left: 25),
        height: 400,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              renderText("Feed Disburse", 16.0, FontWeight.bold),
              SizedBox(
                height: 7,
              ),
              renderText(
                  "Please enter no of KGs of feed to disburse and pick time",
                  15.0,
                  FontWeight.normal),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  renderText("Feed", 12.0, FontWeight.normal),
                  Switch(
                    value: feedType,
                    activeColor: Color(0xFF67D550),
                    inactiveTrackColor: Color(0xFFEB5F2B),
                    thumbColor: MaterialStateProperty.all(Colors.white),
                    onChanged: (value) {
                      onSwitchChanged();
                    },
                  ),
                  renderText("Mineral", 12.0, FontWeight.normal)
                ],
              ),
              SizedBox(
                height: 7,
              ),
              renderText("Feed Weight", 15.0, FontWeight.normal),
              renderTextField(
                  "Enter feed Weight", feedqty.toString(), Colors.black87,
                  (text) {
                onChangeOfFeedWght(text.toString());
              }),
              SizedBox(
                height: 10,
              ),
              renderText("Next Trip Time", 15.0, FontWeight.normal),
              renderTextField("Enter feed Weight", dateInTimeFormat.toString(),
                  Colors.black87, (text) {
                onChangeOfTripTime(
                    text != "" ? text.toString() : dateInTimeFormat.toString());
              }),
              SizedBox(
                height: 7,
              ),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: Align(
                    alignment: Alignment.centerRight,
                    child: CommandButton("Send", Colors.blueAccent, () {
                      onTapOfSend();
                    })),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
