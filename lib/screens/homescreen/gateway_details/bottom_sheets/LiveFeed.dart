import 'dart:convert';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

class LiveFeed extends StatefulWidget {
  // const LiveFeed({ Key? key }) : super(key: key);

  @override
  State<LiveFeed> createState() => _LiveFeedState();
}

class _LiveFeedState extends State<LiveFeed> {
  AquaUser user = AquaUser.getInstance();
  var totalTripFeed = 0;
  var tripInfoData = [];
  var tripsList = [];
  // var totalFeed;
  // var deviceId = "";
  var showStartDate = false;
  var tomorrow = false;
  var feedInfoLoading = false;
  var startDate = DateTime.now();
  var lodingSlots = false;
  var slotsArr = [];
  var dateTime = DateTime.now();
  var epochtime;
  // var tripInfoData;
  List feedData = [];
  var todayTotalFeed = 0.0;
  var totalFeed = 0.0;
  @override
  void initState() {
    getTripData();
    // fetchTripInfoData();
    super.initState();
  }

  fetchTripInfoData() async {
    setState(() {
      todayTotalFeed = 0.0;
      totalFeed = 0.0;
    });
    var gateway = user.selGateway.gateWayMacId;
    //  //print(gateway);
    DocumentReference trip_info_data = await FirebaseFirestore.instance
        .collection("wifi_af_ver03_feeder_data")
        .doc("$gateway")
        .collection("info")
        .doc('livedata');
    trip_info_data.snapshots().listen((documentSnapshot) {
      if (documentSnapshot.exists) {
        ////print(documentSnapshot.data());
        setState(() {
          tripInfoData = documentSnapshot.data();
        });
        parseResponseData(documentSnapshot.data(), 'live');
      }
    });
  }

  getTripinfoHistory(start, end) async {
    setState(() {
      todayTotalFeed = 0.0;
      totalFeed = 0.0;
      feedData = [];
    });
    try {
      /*   //print("HAi....");
      //print(start);
      //print("end");
      //print(end);*/
      var gateway = await user.selGateway.gateWayMacId;
      var trip_info_history_data = await FirebaseFirestore.instance
          .collection("wifi_af_ver03_feeder_data")
          .doc("${gateway}")
          .collection("history")
          .where('lut', isGreaterThanOrEqualTo: start)
          .where('lut', isLessThanOrEqualTo: end)
          .get()
          .then((documentSnapshot) {
        var docsLength = documentSnapshot.docs;
        var docsData = documentSnapshot.docs[0];
        //print(documentSnapshot.docs[0].data());
        // setState(() {
        //   tripInfoData = docsData.data();
        // });
        if (docsLength.length > 0) {
          this.parseResponseData(docsData.data(), 'hist');
        } else {
          setState(() {
            tripInfoData = [];
          });
        }
      });
    } catch (error) {
      if (this.mounted) {
        setState(() {
          //tripInfoData = [];
          if (tripInfoData != null) {
            // setState(() {
            feedData = [];
            // });
            // fetchTripInfoData();
          }
        });
      }
    }
  }

  convertTripWiseMapToList(data) {
    var list = [];
    if (data != null) {
      data.forEach((ele, val) => list.add(val));
    }
    return list;
  }

  parseResponseData(data, type) {
    if (data['af_calibration_data'] != null) {
      List localFeedData = [];
      var listData = convertTripWiseMapToList(data['af_calibration_data']);
      for (var i = 0; i < listData.length; i++) {
        user.selGateway.botDevices.map((e) {
          if (e.macId == listData[i]['macID']) {
            var validateData =
                DateTime.fromMillisecondsSinceEpoch(listData[i]['lut'] * 1000);
            if (validateData.day == dateTime.day &&
                validateData.month == dateTime.month &&
                validateData.year == dateTime.year) {
              localFeedData.add(listData[i]);
            }
          }
        }).toList();
      }

      Map afData = data['af_calibration_data'];

      setState(() {
        feedData = localFeedData;
      });
      feedData.forEach((e) {
        user.selGateway.botDevices.forEach((i) {
          if (i.macId == e['macID']) {
            e['deviceName'] = i.deviceName;
          }
        });
      });
      for (var i = 0; i < feedData.length; i++) {
        setState(() {
          if (feedData[i]['todayFeed'] != null) {
            todayTotalFeed += double.parse(
                "${(feedData[i]['todayFeed']).toStringAsFixed(2)}");
          }
          if (feedData[i]['totalFeed'] != null) {
            totalFeed += double.parse(
                "${(feedData[i]['totalFeed']).toStringAsFixed(2)}");
          }
        });
      }
    }
  }

  prepareElasticRequestJsonForIndTrip(tripId, epochTime, deviceId) {
    // var deviceId = deviceId;
    var start;
    if (dateTime != null) {
      start = dateTime;
    } else {
      start = DateTime.now();
    }
    start = new DateTime(start.year, start.month, start.day, 0, 0, 0, 0);

    var startTime = DateFormat.Hms().format(start);

    var min = (start.millisecondsSinceEpoch ~/ 1000).round();
    var max = min + 86400;
    var endTime = DateTime.fromMillisecondsSinceEpoch(max * 1000);
    
    var query = {
		"size":10,
			"query": {
				"bool": {
				
					"filter": [ {
						"match_phrase": {
							"deviceId": "$deviceId"
						}
						
					}, 
					{
						"match_phrase": {
							"tripID": "$tripId"
						}
						
					},
					{
						"match_phrase": {
							"epochTime": "$epochTime"
						}
						
					},
					{
						"range": {
							"epochTime": {
                  "format": "epoch_millis",
                  "gte": 1648665000,
                  "lte": 1648751399
                }
						}
					}]
				}
			}
		};
    return query;
  }


  prepareElasticRequestJsonForL1(order, deviceId) {
    // var deviceId = deviceId;
    var start;
    if (dateTime != null) {
      start = dateTime;
    } else {
      start = DateTime.now();
    }
    start = new DateTime(start.year, start.month, start.day, 0, 0, 0, 0);

    var startTime = DateFormat.Hms().format(start);

    var min = (start.millisecondsSinceEpoch ~/ 1000).round();
    var max = min + 86400;
    var endTime = DateTime.fromMillisecondsSinceEpoch(max * 1000);
    // var query = {
		// 	"aggs": {
		// 		"tripID": {
		// 			"terms": {
		// 				"field": "tripID",
		// 				"order": {
		// 					"_key": "desc"
		// 				},
		// 				"size": 20
		// 			},
		// 			"aggs": {
		// 				"currentWeight": {
		// 					"max": {
		// 						"field": "currentWeight"
								
								
		// 					}
		// 				},
		// 				"weightToDisburse": {
		// 					"max": {
		// 						"field": "weightToDisburse"
		// 					}
		// 				}
		// 			}
		// 		}
		// 	},
		// 	"size":400,
		// 	"query": {
		// 		"bool": {
				
		// 			"filter": [ {
		// 				"match_phrase": {
		// 					"deviceId": "$deviceId"
		// 				}
		// 			}, {
		// 				"range": {
		// 					"epochTime": {
    //               "format": "epoch_millis",
    //               "gte": (start.millisecondsSinceEpoch ~/ 1000).round(),
    //               "lte": (endTime.millisecondsSinceEpoch ~/ 1000).round()
    //             }
		// 				}
		// 			}]
		// 		}
		// 	}
		// };
    var query = {
			"aggs": {
				"tripID": {
					"terms": {
						"field": "tripID",
						"order": {
							"_key": "desc"
						},
						"size": 20
					},
					"aggs": {
						"currentWeight": {
							"max": {
								"field": "currentWeight"
								
								
							}
						},
						"epochTime": {
							"max": {
								"field": "epochTime"
								
								
							}
						},
						"weightToDisburse": {
							"max": {
								"field": "weightToDisburse"
							}
						}
					}
				}
			},
			"size":400,
			"query": {
				"bool": {
				
					"filter": [ {
						"match_phrase": {
							"deviceId": "$deviceId"
						}
						
					}, 
					{
						"match_phrase": {
							"tripStatus": "3"
						}
						
					},
					{
						"range": {
							"epochTime": {
                  "format": "epoch_millis",
                  "gte": 1648665000,
                  "lte": 1648751399
                }
						}
					}]
				}
			}
		};
    return query;
  }



  parseTripData(response) async {
    var hitsJson = response['aggregations'];
    List dataArray = hitsJson['tripID']['buckets'];
    var data = [];
    for (var i = 0; i < dataArray.length; i++) {
      var dataItem = dataArray[i];
      var gateWayDataUpdatePkt = dataItem;

      data.add(gateWayDataUpdatePkt);
    }
    data.sort((a, b) => a["key"].compareTo(b["key"]));
    // print('Low to hight in price: $data');
    return data;
  }

  fetchIndividualTripData(tripId, epochTime, deviceId) async {
    var url;
    // 'http://34.69.73.157:9242/nextaquarpc_linemon/_search'

    url = Uri.parse("http://34.69.73.157:9242/aquabot/_search");

    Map<String, String> header = {'Content-Type': 'application/json'};
    var bodyJson = jsonEncode(prepareElasticRequestJsonForIndTrip(tripId, epochTime, deviceId));
    // print(bodyJson);
    var response = await http.post(url,
        body: bodyJson, headers: header, encoding: Encoding.getByName("utf-8"));
    // print(response.body);
    var convertedData = json.decode(response.body);
    return convertedData;
  }

  getTripData() async {
    var trips = [];
    setState(() {
      // tripsList = [];
      tripInfoData = [];
      // gotData = false;
    });
    var botList = user.selGateway.botDevices;
    for(var i = 0; i < botList.length; i++){
    // user.selGateway.botDevices.map((e) async {
      var todayFeed = 0.0;
      var weightToDisburse = 0.0;
    var response = await fetchTripData(botList[i].macId);
    print(response);
    var data = await parseTripData(response);
    print(data);
    setState(() {
      tripsList.addAll(data);
    });
    // if(tripsList.isNotEmpty){
      for(var index = 0; index < tripsList.length; index++){
        var indResponse = await fetchIndividualTripData(tripsList[index]['key'], int.parse("${(tripsList[index]['epochTime']['value']).toStringAsFixed(0)}"), botList[i].macId);
        var data = await parseIndTripData(indResponse);
        trips.addAll(data);
        if(botList[i].macId == trips[index]['deviceId']){
        todayFeed = todayFeed + trips[index]['currentWeight'];
        weightToDisburse = weightToDisburse+trips[index]['weightToDisburse'];
        }
        print(data);
      }
    // }
    setState(() {
          feedData.add({'todayFeed': todayFeed, 'deviceId': botList[i].macId, 'weightToDisburse': weightToDisburse});
        });
    }
    
    // setState(() {
    //   // gotData = true;
    // });
  }

  parseIndTripData(response) async {
    var hitsJson = response['hits'];
    List dataArray = hitsJson['hits'];
    var data = [];
    for (var i = 0; i < dataArray.length; i++) {
      var dataItem = dataArray[i];
      var gateWayDataUpdatePkt = dataItem['_source'];

      data.add(gateWayDataUpdatePkt);
    }
    data.sort((a, b) => a["epochTime"].compareTo(b["epochTime"]));
    print('Low to hight in price: $data');

    return data;
  }

  fetchTripData(deviceId) async {
    var url;
    // 'http://34.69.73.157:9242/nextaquarpc_linemon/_search'

    url = Uri.parse("http://34.69.73.157:9242/aquabot/_search");

    Map<String, String> header = {'Content-Type': 'application/json'};
    var bodyJson = jsonEncode(prepareElasticRequestJsonForL1("avg", deviceId));
    // print(bodyJson);
    var response = await http.post(url,
        body: bodyJson, headers: header, encoding: Encoding.getByName("utf-8"));
    // print(response.body);
    var convertedData = json.decode(response.body);
    return convertedData;
  }


  // getTripinfoHistory(start, end) async {
  //   // widget.trip_controller.clear();
  //   try {
  //     var gateway = await user.selGateway.gateWayMacId;
  //     var trip_info_history_data = await FirebaseFirestore.instance
  //         .collection("wifi_af_ver03_feeder_data")
  //         .doc("${gateway}")
  //         .collection("history")
  //         .where('lut', isGreaterThanOrEqualTo: start)
  //         .where('lut', isLessThanOrEqualTo: end)
  //         .get()
  //         .then((documentSnapshot) {
  //       var docsLength = documentSnapshot.docs;
  //       var docsData = documentSnapshot.docs[0];
  //       widget.trip_controller.addtrips(documentSnapshot.docs[0].data(),deviceId);
  //       // print("sgkgsgdsfdsgdgdfggdgdg");
  //       // print(documentSnapshot.docs[0].data());
  //       // print("sdhgkhsghdshdsjkdsgjkdgjkgdjkgdjksggdsgdfgf");
  //       // setState(() {
  //       //   tripInfoData = docsData.data();
  //       // });
  //       // setState(() {
  //       //    gotData = true;
  //       // });
  //       // if (docsLength.length > 0) {
  //       //   this.parseResponseData(docsData.data(), 'hist');
  //       // } else {
  //       //   setState(() {
  //       //     tripInfoData = [];
  //       //   });
  //       // }
  //     });
  //   } catch (error) {
  //     if (this.mounted) {
  //       // setState(() {
  //         //tripInfoData = [];
  //         if (tripInfoData != null) {
  //           fetchTripInfoData();
  //         }
  //       // });
  //     }
  //     // try{
  //     // if(tripInfoData.length == 0){
  //     //   var gateway = await user.selGateway.gateWayMacId;
  //     // print(gateway);
  //     //  DocumentReference trip_info_data = await FirebaseFirestore.instance
  //     //     .collection("wifi_af_ver03_feeder_data")
  //     //     .doc("${gateway}")
  //     //     .collection("info")
  //     //     .doc('livedata');
  //     //     trip_info_data.get().then((DocumentSnapshot documentSnapshot) {
  //     //       if(documentSnapshot.exists){
  //     //         //print(documentSnapshot.data());
  //     //         setState(() {
  //     //           tripInfoData = documentSnapshot.data();
  //     //         });
  //     //         parseResponseData(documentSnapshot.data(), 'live');
  //     //       }
  //     //     });
  //     // }
  //     // }catch(error){
  //     //   print(error);
  //     // }
  //     //  print(error);
  //   }
  // }


  checkDateTodayOrNot(date) {
    if (date != null) {
      var recievedDate = DateTime.fromMillisecondsSinceEpoch(date * 1000).day;
      var recievedMonth =
          DateTime.fromMillisecondsSinceEpoch(date * 1000).month;
      var recievedYear = DateTime.fromMillisecondsSinceEpoch(date * 1000).year;
      var currentDate = DateTime.now().day;
      var currentMonth = DateTime.now().month;
      var currentYear = DateTime.now().year;
      if ((currentDate == recievedDate) &&
          (currentMonth == recievedMonth) &&
          (currentYear == recievedYear)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  onSlotsDate(date) {
    setState(() {
      showStartDate = false;
      startDate = date;
      date = date;
      tomorrow = false;
      feedInfoLoading = true;
      lodingSlots = true;
    });
    var selectedDate = date.millisecondsSinceEpoch ~/ 1000;

    if (checkDateTodayOrNot(selectedDate) == true) {
      // let paramObj = {
      //     gateway: this.state.gateway,
      //     autofeederData: this.state.autofeederData,
      //     feederData: this.state.feederData
      // }
      fetchTripInfoData();
      // this.getSlotsFromFirebase(paramObj);
    } else {
      var endDate = date;
      var startTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
      //  //print(startTime);
      //  //print("HElll;");
      var endTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
      var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
      var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
      getTripinfoHistory(startEpoch, endEpoch);
    }

    // }
  }

  String getText() {
    if (dateTime == null) {
      return "Select DateTime";
    } else {
      // //print(dateTime.millisecondsSinceEpoch ~/ 1000);
      setState(() {
        epochtime = dateTime.millisecondsSinceEpoch ~/ 1000;
      });
      var epochDateTime = dateTime.millisecondsSinceEpoch ~/ 1000;
      /*  //print("Ep[ocvhsahhs");
      //print(epochDateTime);
      //print(dateTime);*/

      // onSlotsDate(dateTime);

      return DateFormat('dd/MM/yyyy').format(dateTime);
    }
  }

  dateLeftPressed() {
    if (dateTime != null) {
      var start = dateTime.millisecondsSinceEpoch ~/ 1000;
      // start.setHours(0, 0, 0, 0);
      var sDate = start;
      sDate = sDate - 86400;
      sDate = sDate * 1000;
      var convertedDate = DateTime.fromMillisecondsSinceEpoch(sDate);
      //dateTime = convertedDate;
      setState(() {
        dateTime = convertedDate;
      });
    }
  }

  dateRightPressed() {
    if (dateTime != null) {
      var start = dateTime.millisecondsSinceEpoch ~/ 1000;
      var currentDate = DateTime.now();
      var recievedDate = DateTime.fromMillisecondsSinceEpoch(start * 1000).day;
      var recievedMonth =
          DateTime.fromMillisecondsSinceEpoch(start * 1000).month;
      var recievedYear = DateTime.fromMillisecondsSinceEpoch(start * 1000).year;
      var currentDay = DateTime.now().day;
      var currentMonth = DateTime.now().month;
      var currentYear = DateTime.now().year;
      if ((currentDay == recievedDate) &&
          (currentMonth == recievedMonth) &&
          (currentYear == recievedYear)) {
        //  //print("Date is Today");
      } else {
        var sDate = start;
        sDate = sDate + 86400;
        sDate = sDate * 1000;
        var convertedDate = DateTime.fromMillisecondsSinceEpoch(sDate);
        setState(() {
          dateTime = convertedDate;
        });
      }
    }
  }

  slotBackwardDate() {
    dateLeftPressed();
    /*  //print("Hello");
    //print(startDate);
    //print("strafrf");*/
    var day = startDate;
    var getDate = day.millisecondsSinceEpoch;
    //  //print(getDate);
    //   //print("epich");
    var prevDay = DateTime.fromMillisecondsSinceEpoch(getDate);
    //   //print(prevDay);
    //  //print("oefsaf");
    prevDay = new DateTime(
        prevDay.year,
        prevDay.month,
        prevDay.day - 1,
        prevDay.hour,
        prevDay.minute,
        prevDay.second,
        prevDay.millisecond,
        prevDay.microsecond);
    setState(() {
      startDate = prevDay;
      tomorrow = false;
      slotsArr = [];
      lodingSlots = true;
    });
    var endDate = prevDay;
    var startTime =
        new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //  //print(startTime);
    //  //print("HElll;");
    var endTime =
        new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    getTripinfoHistory(startEpoch, endEpoch);
  }

  slotForwardDate() {
    dateRightPressed();
    var day = startDate;
    var getDate = day.millisecondsSinceEpoch;
    var nextDay = DateTime.fromMillisecondsSinceEpoch(getDate);
    //  //print("Helloghhjh");
    //  //print(nextDay);
    if ((day.day + 1) < DateTime.now().day) {
      nextDay = new DateTime(
          nextDay.year,
          nextDay.month,
          nextDay.day + 1,
          nextDay.hour,
          nextDay.minute,
          nextDay.second,
          nextDay.millisecond,
          nextDay.microsecond);
      setState(() {
        startDate = nextDay;
        tomorrow = false;
        lodingSlots = true;
        slotsArr = [];
      });

      var endDate = nextDay;
      var startTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
      //  //print(startTime);
      // //print("HElll;");
      var endTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
      var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
      var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
      getTripinfoHistory(startEpoch, endEpoch);
      //this.getSlotsFromElasticSearchDB(startTime, endTime, "slot");
    } else if ((day.day + 1) == DateTime.now().day) {
      //nextDay.setDate(day.getDate() + 1);
      // var nextDay = DateTime.fromMicrosecondsSinceEpoch(getDate);
      nextDay = new DateTime(
          nextDay.year,
          nextDay.month,
          nextDay.day + 1,
          nextDay.hour,
          nextDay.minute,
          nextDay.second,
          nextDay.millisecond,
          nextDay.microsecond);
      setState(() {
        startDate = nextDay;
        tomorrow = false;
        lodingSlots = true;
        slotsArr = [];
      });

      var endDate = nextDay;
      var startTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
      //   //print(startTime);
      //  //print("HElll1");
      var endTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
      var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
      var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
      ;
      fetchTripInfoData();
      // var paramObj = {
      //     // gateway: gateway,
      //     autofeederData: autofeederData,
      //     feederData: feederData
      // }
      //getSlotsFromFirebase(paramObj);

      //getSlotsFromElasticSearchDB(startTime, endTime, "slot")

    } else {
      if (day.month == DateTime.now().month) {
        // Alert.alert(
        //     'NextAqua',
        //     `You are selected Date is out of range`,
        // )
      } else {
        nextDay = new DateTime(
            nextDay.year,
            nextDay.month,
            nextDay.day + 1,
            nextDay.hour,
            nextDay.minute,
            nextDay.second,
            nextDay.millisecond,
            nextDay.microsecond);
        setState(() {
          startDate = nextDay;
          tomorrow = false;
          lodingSlots = true;
          slotsArr = [];
        });

        var endDate = nextDay;
        var startTime =
            new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
        //  //print(startTime);
        //  //print("HElll2");
        var endTime = new DateTime(
            endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
        var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
        var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
        getTripinfoHistory(startEpoch, endEpoch);
        //this.getSlotsFromElasticSearchDB(startTime, endTime, "slot");

        // let paramObj = {
        //     gateway: this.state.gateway,
        //     autofeederData: this.state.autofeederData,
        //     feederData: this.state.feederData
        // }
        // this.getSlotsFromFirebase(paramObj);
      }
    }
  }

  pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
        context: context,
        initialDate: dateTime ?? initialDate,
        firstDate: DateTime(DateTime.now().year - 5),
        lastDate: DateTime.now());

    if (newDate == null) {
      return initialDate;
    } else {
      return newDate;
    }
  }

  Future pickDateTime(BuildContext context) async {
    final date = await pickDate(context);
    if (date == null) return;
    setState(() {
      dateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            child: Column(children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, top: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 26.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, top: 10),
                child: GestureDetector(
                    onTap: () {},
                    child: Text(
                      "${user.selGateway.farmName}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontFamily: 'BrandonReg',
                          fontWeight: FontWeight.bold),
                    )),
              ),
            ],
          ),
          Align(
            alignment: Alignment.topRight,
            child: Container(
                margin: EdgeInsets.only(right: 20, top: 20),
                alignment: Alignment.center,
                color: Colors.white,
                height: 30,
                width: 150,
                child: Material(
                  elevation: 2,
                  child: Container(
                    child: Row(
                      //  mainAxisAlignment: MainAxisAlignment.center,
                      //  crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 20,
                          width: 20,
                          child: GestureDetector(
                            onTap: () {
                              slotBackwardDate();
                              // setState(() {
                              //   isMap = false;
                              // });
                            },
                            child: Image.asset('images/arrowLeftGry.png'),
                          ),
                        ),
                        SizedBox(width: 20),
                        Container(
                          margin: EdgeInsets.only(top: 7),
                          child: SizedBox(
                            height: 25,
                            width: 80,
                            child: GestureDetector(
                              onTap: () {
                                pickDateTime(context);
                                // setState(() {
                                //   isMap = false;
                                // });
                              },
                              child: Text(
                                getText(),
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          height: 20,
                          width: 20,
                          child: GestureDetector(
                            onTap: () {
                              slotForwardDate();
                              // setState(() {
                              //   isMap = false;
                              // });
                            },
                            child: Image.asset('images/arrowRightGrey.png'),
                          ),
                        ),
                      ],
                    ),
                  ),
                )

                /*ElevatedButton(
                                    style: ElevatedButton.styleFrom(primary: Colors.white),
                                    onPressed: () {
                                      pickDateTime(context);
                                    },
                                    child: Text(
                                      getText(),
                                      style: TextStyle(color: Colors.black),
                                    )),*/
                ),
          ),
          feedData.length == 0
              ? SizedBox()
              : Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.shade200)),
                  height: 35,
                  margin: EdgeInsets.only(left: 10, right: 10, top: 30),
                  child: Material(
                    elevation: 1,
                    child: Container(
                      // color: Colors.red,

                      child: Row(
                        //  mainAxisAlignment: MainAxisAlignment.space,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width * 0.25,
                              child: Text(
                                "Device Name",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.001,
                            color: Colors.grey.shade400,
                          ),
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width * 0.33,
                              child: Text("Today Feed",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold))),
                          Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width * 0.001,
                            color: Colors.grey.shade400,
                          ),
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width * 0.35,
                              child: Text("Total Feed",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)))
                        ],
                      ),
                    ),
                  ),
                ),
          feedData.length == 0
              ? Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: Center(
                    child: Text("Feed Data is not available"),
                  ))
              : ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: 350, minHeight: 100),
                  // height: 350,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: feedData.length,
                    itemBuilder: (BuildContext context, int index) {
                      // if(feedData[index]['todayFeed'] != null){
                      // todayTotalFeed += double.parse("${(feedData[index]['todayFeed']).toStringAsFixed(2)}");
                      // }

                      return Column(
                        children: [
                          feedData[index]['deviceId'] != null
                              ? Container(
                                  margin: EdgeInsets.only(left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey.shade200)),
                                  child: Material(
                                    child: Container(
                                      height: 35,
                                      child: Row(
                                        children: [
                                          feedData[index]['deviceId'] != null
                                              ? Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.25,
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    feedData[index][
                                                                'deviceName'] !=
                                                            null
                                                        ? feedData[index]
                                                            ['deviceName']
                                                        : feedData[index]
                                                            ['deviceId'],
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            'BrandonReg',
                                                        fontSize: 14,
                                                        color: Colors.black),
                                                  ))
                                              : Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.25,
                                                ),
                                          Container(
                                            alignment: Alignment.center,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.001,
                                            color: Colors.grey.shade400,
                                          ),
                                          feedData[index]['todayFeed'] != null
                                              ? Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.33,
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "${feedData[index]['todayFeed'].toStringAsFixed(2)} kg",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            'BrandonReg',
                                                        fontSize: 14,
                                                        color: Colors.black),
                                                  ))
                                              : Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.33,
                                                ),
                                          Container(
                                            alignment: Alignment.center,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.001,
                                            color: Colors.grey.shade400,
                                          ),
                                          feedData[index]['weightToDisburse'] != null
                                              ? Container(
                                                  alignment: Alignment.center,
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.35,
                                                  child: Text(
                                                    "${feedData[index]['weightToDisburse'].toStringAsFixed(2)} kg",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            'BrandonReg',
                                                        fontSize: 14,
                                                        color: Colors.black),
                                                  ))
                                              : Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.35,
                                                )
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : SizedBox(),
                        ],
                      );
                    },
                  ),
                ),
          feedData.length == 0
              ? SizedBox()
              : Container(
                  height: 40,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.shade200)),
                  // margin: EdgeInsets.only(),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.35,
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.28,
                          child: Text(
                              "Total: ${todayTotalFeed.toStringAsFixed(2)} kg",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'BrandonReg',
                                  fontSize: 14,
                                  color: Colors.black))),
                      Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width * 0.30,
                          child: Text(
                              "Total: ${totalFeed.toStringAsFixed(2)} kg",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'BrandonReg',
                                  fontSize: 14,
                                  color: Colors.black))),
                    ],
                  ),
                )
        ])),
      ),
    );
  }
}
