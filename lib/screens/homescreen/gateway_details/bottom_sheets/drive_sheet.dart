import 'dart:convert';
import 'dart:math';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_xlider/flutter_xlider.dart';

class Drive extends StatefulWidget {
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Drive({this.isSelected, this.cellDevice, this.index});
//  const Drive({ Key? key }) : super(key: key);

  @override
  _DriveState createState() => _DriveState();
}

class _DriveState extends State<Drive> {
  AquaUser user = AquaUser.getInstance();
  final _globalKey = GlobalKey<ScaffoldMessengerState>();
  var deviceId = "";
  var val = 0.0;
  var driveForwardMax;
  var driveBackwardMax;
  var rudderSpeedMax;
  var epoch;
  var rudderDoubleM = 16000.0;
  var rudderDoubleMax = 8000.0;
  var doubleDriveMotorMaxSpeed = 0.0;
  var sentApi;
  var gotResponse;
  var displayToast = false;
  var toastColor = Colors.green;
  var toastName = "";
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  var data = {
    'deviceId': '',
    'driveForwardMax': 0,
    'driveBackwardMax': 0,
    'isForwar': true,
    'driveMotorMaxSpeed': 0.0,
    'driveMotorSpeed': 0,
    'rudderMotorSpeed': 0,
    'requestData': '',
    'rudderSpeedMax': 0,
    'epoch': 0,
    'driveMSpeed': 0.0,
    'rudderMSpeed': 0,
    'rudderDummeySpeed': 0,
    'doubleSpeed': 0.0
  };
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
      user.selGateway.liveDeviceMap.values.toList().forEach((element) {
        if (deviceId == element.deviceId) {
          driveForwardMax =
              element.driveForwardMax != 0 ? element.driveForwardMax : 8100;
          driveBackwardMax = element.driveBackwardMax;
          rudderSpeedMax = element.rudderSpeedMax;
          epoch = element.epochTime;
          rudderDoubleM = element.rudderSpeedMax != 0.0
              ? element.rudderSpeedMax.toDouble()
              : 16000.0;
          rudderDoubleMax = element.driveForwardMax != 0.0
              ? element.rudderSpeedMax.toDouble() / 2
              : 8000.0;
          doubleDriveMotorMaxSpeed = element.driveForwardMax != 0.0
              ? element.driveForwardMax.toDouble()
              : 8100.0;
        }
      });
      data = {
        ...data,
        'deviceId': deviceId,
        'driveBackwardMax': 8100,
        'driveForwardMax': 8100,
        'driveMotorMaxSpeed': 8100.0,
        'rudderSpeedMax': 16000,
        'driveMSpeed': 0.0,
        'rudderMSpeed': 0,
        'epoch': epoch
      };
    });
    super.initState();
  }

  // @override
  // void dispose(){
  //   super.dispose();
  //   // if(mounted){
  //     sendStopMotor();
  //   // }
  // }
  sendCommandDirectlyToDevice2(command) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse("https://mdash.net/api/v2/devices/" +
        data['deviceId'] +
        "/rpc/setContrl?access_token=JKrRbHr8MnhdUUso91Zmm91A");
    // setState(() {
    //   sentApi = path;
    // });
    var encodedCommand = jsonEncode(command);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    //print(response);
    var convertedData = json.decode(response.body);
    // setState(() {
    //   gotResponse = convertedData;
    // });
    return response;
  }

  forwardReverseHandler() {
    setState(() {
      data = {
        ...data,
        'isForwar': !data['isForwar'],
        'driveMotorMaxSpeed': !data['isForwar']
            ? data['driveForwardMax']
            : data['driveBackwardMax'],
        'driveMSpeed': 0.0
      };
    });
    //print(data['isForwar']);
  }

  sendStopMotor() async {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var config = {
      'frametype': "setContrl",
    };
    var configCmd = {
      'version': version,
      'deviceId': data['deviceId'],
      'RudderMotorSpeed': 0,
      'DriveMotorSpeed': 0,
      'RudderMotorDirection': 1
    };
    //config['commands'] = [configCmd];
    var finalObj = {'frametype': "setContrl", 'commands': configCmd};

    try {
      var responseData = await sendCommandDirectlyToDevice2(configCmd);
      // showOverlay(finalObj);
      //print(responseData.body);
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.green;
      //   toastName = "Success";
      // });
    } catch (error) {
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.red;
      //   toastName = "Failed";
      // });
      //print(error);
    }
  }

  updateMotorSpeed(speed, motorType) {
    //print(speed);
    if (motorType == 'driveMotor') {
      setState(() {
        data = {...data, 'driveMotorSpeed': speed};
      });
    } else {
      setState(() {
        data = {...data, 'rudderMotorSpeed': speed};
      });
    }
    sendMotorSpeedToserver(deviceId);
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  sendMotorSpeedToserver(deviceID) {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var config = {
      "frametype": "setContrl",
    };
    var rudderMspped;
    var rudderMotDirection = 2;
    //print(data['rudderSpeedMax']);
    int divRudderSpeedMax = data['rudderSpeedMax'] == 0.0
        ? 0
        : int.parse("${data['rudderSpeedMax']}");
    var reudderSpeedMax = divRudderSpeedMax ~/ 2;
    //print("rudderMotorSpeed");
    //print(data['rudderMSpeed']);
    if (data['rudderMSpeed'] != 0) {
      double rudderMotorDir = data['rudderMotorSpeed'];
      int rudderMotorDirecSpeed = rudderMotorDir.toInt();
      if (rudderMotorDirecSpeed > reudderSpeedMax) {
        rudderMspped = data['rudderDummeySpeed'];
        rudderMotDirection = 1;
      }
      // else if (data['rudderMotorSpeed'] == reudderSpeedMax) {
      //     rudderMspped = data['rudderDummeySpeed'];
      //     rudderMotDirection = 1;
      // }
      else if (data['rudderMotorSpeed'] == reudderSpeedMax) {
        rudderMspped = 0;
        rudderMotDirection = 1;
      } else {
        rudderMspped = data['rudderDummeySpeed'];
        rudderMotDirection = 2;
      }
    } else {
      //print(data['rudderDummeySpeed']);
      rudderMspped = data['rudderDummeySpeed'];
      rudderMotDirection = 1;
    }

    var configCmd = {
      'version': version,
      'deviceId': deviceID,
      'RudderMotorSpeed': rudderMspped,
      'RudderMotorDirection': rudderMotDirection,
      'DriveMotorSpeed': data['driveMotorSpeed'],
      'DriveMotorDirection': data['isForwar'] == true ? 1 : 2,
    };
    var finalObj = {'frametype': "setContrl", 'commands': configCmd};
    //  showOverlay(finalObj);
    //config['commands'] = [configCmd]
    sendCommandDirectlyToDevice(configCmd, deviceID);
  }

  sendCommandDirectlyToDevice(command, deviceID) async {
    try {
      //print(deviceID);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/$deviceID/rpc/setContrl?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      var encodedCommand = jsonEncode(command);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        //print(convertedData);
        setState(() {
          gotResponse = convertedData;
          sentApi = path;
        });
        //print("Success");
        setState(() {
          displayToast = true;
          toastColor = Colors.green;
          toastName = "Success";
        });
        //  showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return Alert(
        //           "Command sent successfully", Colors.green, "Drive Settings");
        //     });

        // Fluttertoast.showToast(msg: "Success", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
        // var snackBar = SnackBar(content: Text("Success"));
        // _globalKey.currentState.showSnackBar(SnackBar(content: Text("Success")));
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Success")));
      }
    } catch (error) {
      //print(error);
      setState(() {
        displayToast = true;
        toastColor = Colors.red;
        toastName = "Failed";
      });
      // showDialog(
      //   context: context,
      //   builder: (BuildContext context) {
      //     return Alert(
      //         "Sending Command failed", Colors.red, "Drive Settings");
      //   });
      // Fluttertoast.showToast(msg: "Failed", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.redAccent, textColor: Colors.white, fontSize: 16.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    // //print(data['driveMSpeed']);
    // //print(doubleDriveMotorMaxSpeed);
    // // //print('vjhvj');
    // //print(rudderDoubleMax);
    var displayRuddderSpeed = data['doubleSpeed'];
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    //int rudderMSpeed = data['rudderMSpeed'];
    //assert(rudderMSpeed is double);
    ////print(rudderMSpeed.runtimeType);
    //var rudderMSpeed = double.parse(speedM);
    //assert(rudderMSpeed is double);
    // int rudderSpeedMax = int.parse(data['rudderSpeedMax']);
    // var speedMax = data['rudderSpeedMax'];
    ////print(rudderSpeedMax.runtimeType);
    // assert(speedMax is double);
    // //print(speedMax.runtimeType);
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      alignment: Alignment.topCenter,

      // height: 400,
      child: SingleChildScrollView(
        child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("${deviceName}-(${deviceId})",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          fontFamily: 'BrandonReg')),
                ],
              ),
              Divider(
                thickness: 1,
                color: Colors.black,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CommandButton("Stop", Colors.blueAccent, () {
                    sendStopMotor();
                  }),
                  //ElevatedButton(onPressed: () {}, child: Text("Forward")),
                  CommandButton(
                      data['isForwar'] == true ? "Forward" : "Reverse",
                      data['isForwar'] == true
                          ? Colors.blueAccent
                          : Colors.redAccent, () {
                    forwardReverseHandler();
                  })
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  rudderDoubleMax >
                          (rudderDoubleM != null ? rudderDoubleM : 16000.0) / 2
                      ? Text("${data['doubleSpeed']}R")
                      : rudderDoubleMax ==
                              (rudderDoubleM != null
                                      ? rudderDoubleM
                                      : 16000.0) /
                                  2
                          ? Text("0")
                          : Text("${data['doubleSpeed']}L"),
                  Text("${data['driveMSpeed']}")
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 65),
                      width: 200,
                      child: FlutterSlider(
                        values: [rudderDoubleMax],
                        max: rudderDoubleM != null ? rudderDoubleM : 16000.0,
                        min: 0,
                        onDragCompleted:
                            (handlerIndex, lowerValue, upperValue) {
                          updateMotorSpeed(lowerValue, 'rudderMotor');
                        },
                        handler: FlutterSliderHandler(
                          decoration: BoxDecoration(),
                          child: Material(
                            //type: MaterialType.canvas,
                            //elevation: 3,
                            child: Container(
                                height: 200,
                                padding: EdgeInsets.only(bottom: 4),
                                child: Image.asset('images/right-arrow.png')),
                            color: Color(0xFFE8EAF6),
                          ),
                        ),
                        onDragging: (handlerIndex, lowerValue, upperValue) {
                          var value = lowerValue - 8000;
                          value = value.abs();
                          var rudderMSpeed = data['rudderMSpeed'];
                          var rudderSpeedMax = data['rudderSpeedMax'];
                          setState(() {
                            rudderDoubleMax = lowerValue;
                          });
                          if (rudderDoubleMax > rudderDoubleM / 2) {
                            setState(() {
                              rudderMSpeed = value;
                              data = {
                                ...data,
                                'doubleSpeed': value,
                                'rudderDummeySpeed': value,
                                'rudderMSpeed': value
                              };
                            });
                          } else {
                            setState(() {
                              rudderMSpeed = value;
                              //data['doubleSpeed'] = value;
                              data = {
                                ...data,
                                'doubleSpeed': value,
                                'rudderDummeySpeed': value,
                                'rudderMSpeed': value
                              };
                            });
                          }
                          //print("sgbgs");
                          //print(rudderMSpeed);

                          //print("right");

                          //print("left");
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 0, left: 40),
                      width: 50,
                      height: 200,
                      child: FlutterSlider(
                        axis: Axis.vertical,
                        rtl: true,
                        values: [
                          data['driveMSpeed'] != 0.0 ? data['driveMSpeed'] : 0
                        ],
                        max: 8100.0,
                        min: 0,
                        onDragCompleted:
                            (handlerIndex, lowerValue, upperValue) {
                          updateMotorSpeed(lowerValue, 'driveMotor');
                        },
                        handler: FlutterSliderHandler(
                          decoration: BoxDecoration(),
                          child: RotatedBox(
                            quarterTurns: 2,
                            child: Material(
                              //type: MaterialType.canvas,
                              //elevation: 3,
                              child: Container(
                                  height: 200,
                                  padding: EdgeInsets.only(bottom: 4),
                                  child: RotatedBox(
                                      quarterTurns: 5,
                                      child: Image.asset(
                                          'images/right-arrow.png'))),
                              color: Color(0xFFE8EAF6),
                            ),
                          ),
                        ),
                        onDragging: (handlerIndex, lowerValue, upperValue) {
                          //      data['driveMSpeed'] = lowerValue;
                          setState(() {
                            data = {...data, 'driveMSpeed': lowerValue};
                          });
                          //updateMotorSpeed(lowerValue, 'driveMotor');
                        },
                      ),
                    ),
                  ],
                ),
              )
            ]),
      ),
    );
  }
}
