import 'dart:convert';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class HomeSheet extends StatefulWidget {
  final int index;
  HomeSheet(this.index);
  @override
  _HomeSheetState createState() => _HomeSheetState();
}

class _HomeSheetState extends State<HomeSheet> {
  var deviceId;
  AquaUser user = AquaUser.getInstance();
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    super.initState();
  }

  sendCommandDirectlyToDevice(command, methodName, deviceId) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/$deviceId/rpc/$methodName?access_token=LRoyC90hJjxuoklY7rPAH5Q');
    // setState(() {
    //   enteredApi = path;
    // });
    ////print(enteredApi);
    //print(command);
    var encodedCommand = jsonEncode(command);
    //print(encodedCommand);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    var convertedData = jsonDecode(response.body);
    //print(convertedData);
    //print(response.statusCode);
    return response;
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();

    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  onTapOfOk(message, color) {
    Navigator.pop(context, false);
    var alert = showDialog(
        context: context,
        builder: (BuildContext context) {
          return Alert(message, color, "Home Settings");
        });
    return alert;
  }

  homeTrip() async {
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;

    var config = {
      'frametype': "homeTrip",
    };
    //prepare control command data object
    var configCmd = {
      "version": version,
      "deviceId": deviceId,
    };
    try {
      var responseData =
          await sendCommandDirectlyToDevice(configCmd, 'homeTrip', deviceId);
      // if(responseData.statueCode == 200){
      //   //print(responseData.body);
      //   //print("Home trip");
      // }
      // onTapOfOk("Command sent successfully", Colors.green);
      if (responseData.statusCode == 200) {
        getResponseBasedOnDevice("", 200);
      } else {
        getResponseBasedOnDevice("", 500);
      }
    } catch (error) {
      // onTapOfOk("Sending command failed", Colors.red);
      getResponseBasedOnDevice("", 500);
      //print(error);
    }
    // config['commands'] = [configCmd]
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: AlertDialog(
        title: Text("Are you sure, you want to start Home Trip? ",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                fontFamily: 'BrandonReg')),
        content: SizedBox(
          width: 100,
          child: Container(
            height: 35,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        child: Text("Cancel"),
                        onTap: () => Navigator.pop(context, false)),
                    GestureDetector(
                      child: Text("Ok"),
                      onTap: () {
                        homeTrip();
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                // displayToast == true ? Container(
                //   height: 30,
                //   child: RenderToast(toastName, toastColor, (){ setState(() {
                //     displayToast = false;
                //   }); }),
                // ) : SizedBox()
              ],
            ),
          ),
        ),
      )),
    );
  }
}
