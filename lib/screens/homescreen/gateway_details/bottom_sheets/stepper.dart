import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Steper extends StatefulWidget {
  // const Steper({ Key? key }) : super(key: key);
  final text;
  final width;
  Function() onincrement;
  Function() ondecrement;
  Function(String) onChanged;
  final contWidth;
  final marginLeft;
  final maxVal;
  TextEditingController controller;
  Steper(this.text, this.width, this.onincrement, this.ondecrement,
      this.onChanged, this.contWidth, this.marginLeft, this.maxVal, this.controller);

  @override
  State<Steper> createState() => _SteperState();
}

class _SteperState extends State<Steper> {
  static final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: 30,
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
          border: Border.all(color: Colors.black38)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
              onTap: widget.ondecrement,
              child: Icon(
                Icons.remove,
                color: Colors.black,
                size: 22,
              )),
          Container(
            //height: 1,
            width: MediaQuery.of(context).size.width * 0.002,
            color: Colors.black12,
            height: MediaQuery.of(context).size.height * 0.10,
          ),
          //SizedBox(width: 2,),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              //margin: EdgeInsets.only(left: 15),
              //margin: EdgeInsets.only(bottom: 5),
              child: Align(
                alignment: Alignment.topCenter,
                child: SizedBox(
                    height: 20,
                    width: widget.contWidth,
                    child: TextFormField(
                      // scrollPadding: EdgeInsets.only(bottom: 40),
                      //textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 17.0),
                      textAlignVertical: TextAlignVertical.center,
                      controller: widget.controller,
                      key: Key(widget.text.toString()),
                      //initialValue: widget.text.toString(),
                      // validator: (value){
                      //   if(num.parse(value) > widget.maxVal){
                      //     return widget.maxVal;
                      //   }
                      //   return null;
                      // },
                      inputFormatters: <TextInputFormatter>[
                        //FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(3),
                        //  CustomRangeTextInputFormatter(widget.maxVal),
                      ],
                      //onChanged: widget.onChanged,
                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                      maxLength: widget.maxVal,
                      decoration: InputDecoration(
                        counterText: "",
                        counterStyle: TextStyle(fontSize: 0),
                          labelText: widget.text.toString(),
                          labelStyle: TextStyle(color: widget.controller.text == "" ? Colors.black : widget.controller.text == null ? Colors.white : Colors.white),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              bottom: widget.controller.text == "" ? 9 : 9, left: widget.marginLeft)),
                    )),
              ),
            ),
          ),
          Container(
            //height: 1,
            width: MediaQuery.of(context).size.width * 0.002,
            color: Colors.black12,
            height: MediaQuery.of(context).size.height * 0.10,
          ),
          InkWell(
              onTap: widget.onincrement,
              child: Icon(
                Icons.add,
                color: Colors.black,
                size: 22,
              )),
        ],
      ),
    );
  }
}

class CustomRangeTextInputFormatter extends TextInputFormatter {
  var maxVal;
  CustomRangeTextInputFormatter(this.maxVal);
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue,TextEditingValue newValue,) { 
    if(newValue.text == '')
      return TextEditingValue().copyWith(text: '0');
    else if(int.parse(newValue.text) < 0)
      return TextEditingValue().copyWith(text: '0');

    return int.parse(newValue.text) > maxVal ? TextEditingValue().copyWith(text: "$maxVal") : newValue;
  }
}