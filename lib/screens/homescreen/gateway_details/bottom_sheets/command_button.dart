import 'package:flutter/material.dart';

class CommandButton extends StatelessWidget {
  //const ({ Key? key }) : super(key: key);
  final String name;
  final Color color;
  final GestureTapCallback onPressed;
  CommandButton(this.name, this.color, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(margin: EdgeInsets.only(left: 20), decoration: BoxDecoration(borderRadius: BorderRadius.circular(12)),child: ElevatedButton(onPressed: onPressed, child: Text(name), style: ElevatedButton.styleFrom(primary: color, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),),);
  }
}