import 'dart:convert';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;

import 'caommand_alert.dart';

class Schedule extends StatefulWidget {
  //const Schedule({ Key? key }) : super(key: key);
  final int index;
  Schedule(this.index);

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {
  var deviceId = "";
  var deviceName = "";
  AquaUser user = AquaUser.getInstance();
  var gapBTController = TextEditingController();
  var kgsPTController = TextEditingController();
  var timeIBTController = TextEditingController();
  var tripWtController = TextEditingController();
  var schEnable = false;
  var schInterval = 0;
  var schWt = 0;
  var schGap = 0;
  var schKgs = 0;
  var recievedData = false;
  var showError = "";
  var isOffline = false;

  @override
  void initState(){
    super.initState();
    deviceId = user.selGateway.botDevices[widget.index].macId;
    deviceName = user.selGateway.botDevices[widget.index].deviceName;
    getSchedule();
  }

  getSchedule() async {
    // setState(() {
    //   recievedData = false;
    // });
    //2021-12-16 09:16:45.238553
    var dateT = DateTime.now();
    print(dateT);
    print(dateT.hour);
    try {
      var url = Uri.parse(
          "https://mdash.net/api/v2/devices/$deviceId/rpc/getSchedule?access_token=JKrRbHr8MnhdUUso91Zmm91A");
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var response = await http.post(url);
      print(response.body);
      if (response.statusCode == 200) {
      var decodedData = jsonDecode(response.body);
      setState(() {
        schEnable = decodedData['schEnable'] != null
            ? decodedData['schEnable'] == 1
                ? true
                : false
            : false;
        schInterval = decodedData['schInterval'] != null
            ? decodedData['schInterval']
            : 0;
        schWt = decodedData['schWt'] != null ? decodedData['schWt'] : 0;
        schGap = decodedData['gapsBtwtrips'] != null ? decodedData['gapsBtwtrips'] : 0;
        schKgs = decodedData['kgsPerTrip'] != null ? decodedData['kgsPerTrip'] : 0;
        recievedData = true;
        isOffline = false;
        timeIBTController.text = "$schInterval";
        tripWtController.text = "$schWt";
        gapBTController.text = "$schGap";
        kgsPTController.text = "$schKgs";
      });
      }else{
        var body = jsonDecode(response.body) as Map;
        print(body['error']);
        setState(() {
          isOffline = true;
          recievedData = true;
          showError = body['error']['message'];
          schEnable = false;
        schInterval = 0;
        schWt = 0;
        schGap = 0;
        schKgs = 0;
          timeIBTController.text = "$schInterval";
        tripWtController.text = "$schWt";
        gapBTController.text = "$schGap";
        kgsPTController.text = "$schKgs";
        });
      }
    } catch (error) {
      print(error);
    }
  }


  Widget getText(key, value) {
    var text = Container(
        width: 145.0,
        child: RichText(
            text: TextSpan(
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'BrandonReg',
              fontSize: 14.0,
              color: Colors.black),
          text: "$key: ",
          children: [
            TextSpan(
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  fontFamily: 'BrandonReg',
                  color: Colors.black),
              text: "$value",
            ),
          ],
        )));
    return text;
  }

  onValueChange(text, type){
    if(type == "interval"){
      if(int.parse("$text") < 301){
      var val = int.parse(text);
        setState(() {
          schInterval = val;
          timeIBTController.text = text;
        });
      }else{
        setState(() {
          schInterval = 300;
          timeIBTController.text = "300";
        });
      }
        // if (schInterval > 120) {
        //   showDialog(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return CommandAlert(
        //             "C1 must be less than 100", Colors.red, "Apfc Config");
        //       });
        // }
    }else if(type == "wt"){
      if(int.parse("$text") < 201){
      var val = int.parse(text);
        setState(() {
          schWt = val;
          tripWtController.text = text;
        });
      }else{
        setState(() {
          schWt = 200;
          tripWtController.text = "200";
        });
      }
        // if (schWt > 200) {
        //   showDialog(
        //       context: context,
        //       builder: (BuildContext context) {
        //         return CommandAlert(
        //             "C2 must be less than 100", Colors.red, "Apfc Config");
        //       });
        // }
    }
  }

  onIncrement(type){
    if(type == "interval"){
       if(int.parse("${timeIBTController.text}") < 300){
        var val = schInterval + 1;
      setState(() {
        schInterval = timeIBTController.text == ""
            ? schInterval + 1
            : int.parse("${timeIBTController.text}") + 1;
        timeIBTController.text = "$schInterval";
      });
       }
    }else if(type == "kgs"){
       // if(int.parse("${timeIBTController.text}") < 100){
        var val = schKgs + 1;
      setState(() {
        schKgs = kgsPTController.text == ""
            ? schKgs + 1
            : int.parse("${kgsPTController.text}") + 1;
        kgsPTController.text = "$schKgs";
      });
      // }
    }else if(type == "gap"){
       // if(int.parse("${timeIBTController.text}") < 100){
        var val = schGap + 1;
      setState(() {
        schGap = gapBTController.text == ""
            ? schGap + 1
            : int.parse("${gapBTController.text}") + 1;
        gapBTController.text = "$schGap";
      });
      // }
    }else{
       if(int.parse("${tripWtController.text}") < 200){
        var val = schWt + 1;
      setState(() {
        schWt = tripWtController.text == ""
            ? schWt + 1
            : int.parse("${tripWtController.text}") + 1;
        tripWtController.text = "$schWt";
      });
       }
    }
  }

  onDecrement(type){
    if(type == "interval"){
      if (int.parse("${timeIBTController.text}") > 0) {
        var val = schInterval - 1;
        setState(() {
          // C1 = val;
          schInterval = timeIBTController.text == ""
              ? schInterval - 1
              : int.parse("${timeIBTController.text}") - 1;
          timeIBTController.text = "$schInterval";
        });
       }
    }else if(type == "kgs"){
      // if (int.parse("${timeIBTController.text}") > 0) {
        var val = schKgs - 1;
        setState(() {
          // C1 = val;
          schKgs = kgsPTController.text == ""
              ? schKgs - 1
              : int.parse("${kgsPTController.text}") - 1;
          kgsPTController.text = "$schKgs";
        });
      // }
    }else if(type == "gap"){
      // if (int.parse("${timeIBTController.text}") > 0) {
        var val = schGap - 1;
        setState(() {
          // C1 = val;
          schGap = gapBTController.text == ""
              ? schGap - 1
              : int.parse("${gapBTController.text}") - 1;
          gapBTController.text = "$schGap";
        });
      // }
    }else{
       if (int.parse("${tripWtController.text}") > 0) {
        var val = schWt - 1;
        setState(() {
          // C1 = val;
          schWt = tripWtController.text == ""
              ? schWt - 1
              : int.parse("${tripWtController.text}") - 1;
          tripWtController.text = "$schWt";
        });
       }
    }
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    user.selGateway.botDevices.map((ele) {
      if(ele.macId == deviceId){
      successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();
    setState(() {
      recievedData = true;
    });
    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
    
  }

  sendCommandDirectlyToDevice(command, methodName) async {
    try {
      print(methodName);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/$deviceId/rpc/$methodName?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      var encodedCommand = jsonEncode(command);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        
        
        getResponseBasedOnDevice("" , 200);
        
      } else {
        
        getResponseBasedOnDevice("", 500);
      }
    } catch (error) {
      print(error);
      getResponseBasedOnDevice("", 500);
    }
  }

  sendScheduleCommand() async {
    setState(() {
      schInterval = int.parse("${timeIBTController.text}");
      schWt = int.parse("${tripWtController.text}");
    });
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var cmdarr = [];
    var obj = {
        "version": version,
        "schEnable": schEnable == true ? 1 : 0,
        "schInterval": schInterval,
        "schWt": schWt,
      };
      cmdarr.add(obj);
      var finalObj = {'frametype': "setSchedule", 'commands': obj};
      if(schWt > 200){
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return CommandAlert(
                "Trip Weight must be less than or equal to 200", Colors.red, "Schedule");
          });
      }else if(schInterval > 300){
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return CommandAlert(
                "Time interval must be less than or equal to 300", Colors.red, "Schedule");
          });
      }else{
        // showDialog(
        //   context: context,
        //   builder: (BuildContext context) {
        //     return CommandAlert(
        //         "Time interval must be less than 120", Colors.red, "Schedule");
        //   });
         sendCommandDirectlyToDevice(obj, "setSchedule");
      }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        alignment: Alignment.topCenter,

        margin: EdgeInsets.only(left: 20, right: 20),
        //   height: 400,
        child: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("${deviceName}-(${deviceId})",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            fontFamily: 'BrandonReg')),
                  ],
                ),
                Divider(
                  thickness: 1,
                  color: Colors.black,
                ),
                recievedData == false ? Container(
                  height: 300,
                  child: Center(
                    child: SpinKitThreeBounce(
            size: 30.0,
            color: Color(0xFF0170d0),
          ),
                  ),
                ) : isOffline == true ? Container(height: 300, child: Center(child: Text("$showError"))) : Column(
                  children: [
                //     Row(
                //       mainAxisAlignment: MainAxisAlignment.spaceAround,
                //       children: [
                //         Text("Gap between Trips",
                //         style: TextStyle(
                //             fontWeight: FontWeight.bold,
                //             fontSize: 15,
                //             fontFamily: 'BrandonReg',
                //             color: Color(0xFF05375a))),
                //             Steper("$schGap", 110.0, (){ onIncrement("gap"); }, (){onDecrement("gap");}, (text){}, 55.0, 20.0, 3, gapBTController)
                //       ],
                //     ),
                //     SizedBox(height: 20,),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   children: [
                //     Text("Kgs per Trip",
                //     style: TextStyle(
                //         fontWeight: FontWeight.bold,
                //         fontSize: 15,
                //         fontFamily: 'BrandonReg',
                //         color: Color(0xFF05375a))),
                //     Steper("$schKgs", 110.0, (){ onIncrement("kgs"); }, (){onDecrement("kgs");}, (text){}, 55.0, 20.0, 3, kgsPTController)
                //   ],
                // ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Time interval between Trips(mins)",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg',
                        color: Color(0xFF05375a))),
                        Steper("$schInterval", 110.0, (){onIncrement("interval");}, (){onDecrement("interval");}, (text){ onValueChange(text, "interval"); }, 55.0, 20.0, 3, timeIBTController)
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Trip weight (kg)",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg',
                        color: Color(0xFF05375a))),
                        Steper("$schWt", 110.0, (){ onIncrement("tripWt"); }, (){onDecrement("tripWt");}, (text){ onValueChange(text, "wt"); }, 55.0, 20.0, 3, tripWtController)
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Text("Schedule",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg',
                        color: Color(0xFF05375a))),
                  Row(
                    children: [
                      Text("Off",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg',
                        color: Color(0xFF05375a))),
                      Switch(value: schEnable, onChanged: (val){
                        setState(() {
                          schEnable = !schEnable;
                        });
                      }),
                      Text("On",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg',
                        color: Color(0xFF05375a))),
                    ],
                  )
                ],),
                
                SizedBox(
                  height: 20,
                ),

                Align(
                                alignment: Alignment.topRight,
                                child: SizedBox(
                                  height: 30,
                  width: 100,
                                  child: CommandButton("Send", Colors.blueAccent, () {
                    sendScheduleCommand();
                  }),
                                ),)
                  ],
                ),
              ]),
        ),
      ) 
    );
  }
}
