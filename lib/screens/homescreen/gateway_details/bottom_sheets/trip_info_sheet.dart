import 'dart:async';
import 'dart:convert';

import 'dart:math';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/trip_info_getx.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/trip_map_screen.dart';
import 'package:flutter/material.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:date_time_picker/date_time_picker.dart';


class Trip_info extends StatefulWidget {
  //const Trip_info({ Key? key }) : super(key: key);
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  var trip_controller=Get.put(Trip_Controller());
  Trip_info({this.isSelected, this.cellDevice, this.index});
  @override
  _Trip_infoState createState() => _Trip_infoState();
}

class _Trip_infoState extends State<Trip_info> {
  AquaUser user = AquaUser.getInstance();
  var textFontSize = 16.0;
  var totalTripFeed = 0;
  var tripInfoData = [];
  var tripsList = [];
  var totalFeed;
  var deviceId = "";
  bool isMap = false;
  var tripid;
  var gotData = false;
  var showLoadingProgress = true;
  var showStartDate = false;
  var tomorrow = false;
  var feedInfoLoading = false;
  var lodingSlots = false;
  var showMonth = false;
  var date = DateTime.now();
  var mode = "date";
  var startDate = DateTime.now();
  var showMap = false;
  var loading = false;
  var regions = {};
  var deviceRoute = [];
  var pondPositions = [];
  var paths = [];
  var initialRegions = {};
  var slotsArr = [];
  var dateTime = DateTime.now();

  var epochtime;
  bool isloading = true;

  @override
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    getTripData();
    // var selectedDate = dateTime.millisecondsSinceEpoch ~/ 1000;

    // if (checkDateTodayOrNot(selectedDate) == true) {
    //   // let paramObj = {
    //   //     gateway: this.state.gateway,
    //   //     autofeederData: this.state.autofeederData,
    //   //     feederData: this.state.feederData
    //   // }
    //   fetchTripInfoData();
    //   // this.getSlotsFromFirebase(paramObj);
    // } else {
    //   var endDate = date;
    //   var startTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //   //  print(startTime);
    //   //  print("HElll;");
    //   var endTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    //   var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    //   var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    //   getTripinfoHistory(startEpoch, endEpoch);
    // }
    // var selectedDate = dateTime.millisecondsSinceEpoch ~/ 1000;

    // if (checkDateTodayOrNot(selectedDate) == true) {
    //   // let paramObj = {
    //   //     gateway: this.state.gateway,
    //   //     autofeederData: this.state.autofeederData,
    //   //     feederData: this.state.feederData
    //   // }
    //   fetchTripInfoData();
    //   // this.getSlotsFromFirebase(paramObj);
    // } else {
    //   var endDate = date;
    //   var startTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //   //  print(startTime);
    //   //  print("HElll;");
    //   var endTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    //   var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    //   var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    //   getTripinfoHistory(startEpoch, endEpoch);
    // }

    // fetchTripInfoData();
    super.initState();
  }

  preparePondsPositionCords(points) {
    if (points != null) {
      var cordArr = [];
      var keys = points.keys;

      for (var i = 0; i < keys.length; i++) {
        var indx = keys[i];
        cordArr.add(points[indx]);
      }
      return cordArr;
    } else {
      return [];
    }
  }

  removeZeroRegions(region) {
    var regions = region;
    var keys = region.keys;
    if (keys.length > 0) {
      for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (regions[key]['latitude'] <= 0 && regions[key]['longitude'] <= 0) {
          regions.remove(key);
        }
      }
    }
    return regions;
  }

  preparePondsPositionCordsofDevice(points) {
    if (points != null) {
      var cordArr = [];
      var temp = {};
      var keys = points.keys;
      // let keys1 = Object.keys(points);
      // let keys=keys1.filter(el=>el!='label');
      for (var i = 0; i < keys.length; i++) {
        var indx = keys[i];
        if (i == 0) {
          temp = points[i];
        }

        cordArr.add(points[indx]);
      }
      cordArr.add(temp);

      return cordArr;
    } else {
      return [];
    }
  }

  getRegionForCoordinates(points) {
    if (points != null) {
      var cordArr = [];
      var keys = points.keys;
      for (var i = 0; i < keys.length; i++) {
        var indx = keys[i];
        cordArr.add(points[indx]);
      }
      var minX, maxX, minY, maxY;

      // init first point
      ((point) => {
            minX = point['latitude'],
            maxX = point['latitude'],
            minY = point['longitude'],
            maxY = point['longitude'],
          })(cordArr[0]);

      // calculate rect
      cordArr.map((point) => {
            // minX = Math.min(minX, point['latitude']),
            // maxX = Math.max(maxX, point['latitude']),
            // minY = Math.min(minY, point['longitude']),
            // maxY = Math.max(maxY, point['longitude'])
          });

      var midX = (minX + maxX) / 2;
      var midY = (minY + maxY) / 2;
      var deltaX = (maxX - minX);
      var deltaY = (maxY - minY);

      return {
        // latitude: midX,
        // longitude: midY,
        // latitudeDelta: 0.0001,//deltaX
        // longitudeDelta: 0.0020//deltaY
      };
    } else {
      return {
        // latitude: 17.4027,
        // longitude: 78.4525,
        // latitudeDelta: 0.0922,//deltaX
        // longitudeDelta: 0.0421//deltaY
      };
    }
  }

  hideSlotDateTimePicker() {
    setState(() {
      showStartDate = false;
      tomorrow = false;
      feedInfoLoading = false;
      lodingSlots = false;
      showMonth = false;
    });
  }

  getShortHandDate(date) {
    return date.day + "/" + (date.month() + 1) + "/" + date.year();
  }

  query(trip) {
    return {
      'sort': [
        {
          'epochTime': {'order': 'desc'}
        }
      ],
      'size': 2000,
      'query': {
        'bool': {
          'must': [
            {
              'match_phrase': {'tripID': trip['tripID']}
            },
            {
              'match_phrase': {'deviceId': deviceId}
            },
            {
              'match_phrase': {
                'siteId': user.selGateway.botDevices[widget.index].macId
              }
            },
            {
              'match_phrase': {'tripStatus': "3"}
            }
          ]
        }
      }
    };
  }

  checkDateTodayOrNot(date) {
    if (date != null) {
      var recievedDate = DateTime.fromMillisecondsSinceEpoch(date * 1000).day;
      var recievedMonth =
          DateTime.fromMillisecondsSinceEpoch(date * 1000).month;
      var recievedYear = DateTime.fromMillisecondsSinceEpoch(date * 1000).year;
      var currentDate = DateTime.now().day;
      var currentMonth = DateTime.now().month;
      var currentYear = DateTime.now().year;
      if ((currentDate == recievedDate) &&
          (currentMonth == recievedMonth) &&
          (currentYear == recievedYear)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  preparePath(data) {
    if (data['hits'].length > 0) {
      var pathsArr = [];
      for (var i = 0; i < data['hits'].length - 1; i++) {
        var val = data.hits[i]['._source'];

        var path = {
          'latitude': val['gpsLat1'],
          'longitude': val['gpsLong1'],
        };
        pathsArr.add(path);
      }
      setState(() {
        paths = pathsArr;
      });
    }
  }

  parseResponseData(data, type) {
    if (data['af_calibration_data'] != null) {
      var resData = data['af_calibration_data'];
      //print(data['af_calibration_data']);

      if (type == 'live') {
        List tripData = [];
        var lut = resData[deviceId];
        //var lutData = lut['lut'];

        //print(lutData);
        //var timeFromLut = checkDateTodayOrNot(lut['lut']);

        if (resData[deviceId] != null) {
          //  if(checkDateTodayOrNot(lut['lut']) == true){
//                         if (resData[deviceId] != null) {
//                           var tripWiseFeedObjs = lut['tripWiseFeedObjs'];
//                             setState(() {
//                 totalFeed = lut['totalFeed'];
//               });
//               List keys = tripWiseFeedObjs.keys.toList();
//               print(keys);
//               var tripWiseFeedObj = lut['tripWiseFeedObjs'];

//               keys.map((ele){
//                 var obj = tripWiseFeedObj[ele];
//                 obj['triptime'] = ele;
//                 tripData.add(obj);
//               }).toList();
//               print(tripData);
//               print("trpData:${tripData}");
// ;              setState(() {
//                 tripInfoData = tripData;
//               });
//                         } else {
//                            setState(() {
//                              tripInfoData = [];
//                            });
//                         }
          //  }else {
          if (resData[deviceId] != null) {
            //      print("Eopiohhasghdgsgdsgdhsghadgsfahj");
            //     print(dateTime);
            var lutToDate =
                DateTime.fromMillisecondsSinceEpoch(lut['lut'] * 1000);
            if (dateTime.day == lutToDate.day &&
                dateTime.month == lutToDate.month &&
                dateTime.year == lutToDate.year) {
              var tripWiseFeedObjs = lut['tripWiseFeedObjs'];
              setState(() {
                totalFeed = lut['totalFeed'];
              });
              List keys = tripWiseFeedObjs.keys.toList();
              //     print(keys);
              var tripWiseFeedObj = lut['tripWiseFeedObjs'];

              keys.map((ele) {
                var obj = tripWiseFeedObj[ele];
                obj['triptime'] = ele;
                tripData.add(obj);
              }).toList();
              //    print(tripData);
              //    print("trpData:${tripData}");
              setState(() {
                tripData.sort((productA, productB) =>
                    int.parse("${productA['tripID']}")
                        .compareTo(int.parse("${productB['tripID']}")));

                tripInfoData = tripData;
               gotData = true;
              });
            } else {
              setState(() {
                tripInfoData = [];
                gotData = true;
              });
            }
            //   print(tripInfoData);
          } else {
            setState(() {
              tripInfoData = [];
              gotData = true;
            });
          }
          //    setState(() {
          //      tripInfoData = [];
          //    });
          // }
        } else {
          setState(() {
            tripInfoData = [];
            gotData = true;
          });
        }
      } else {
        List tripData = [];
        var lut = resData[deviceId];
        var lutData = lut['lut'];
        var tripWiseFeedObjs = lut['tripWiseFeedObjs'];
        //  print(lutData);
        var timeFromLut = checkDateTodayOrNot(lutData);
        if (resData[deviceId] != null) {
          //    print(lut);
          setState(() {
            totalFeed = lut['totalFeed'];
          });
          List keys = tripWiseFeedObjs.keys.toList();
          var tripWiseFeedObj = lut['tripWiseFeedObjs'];
          keys.forEach((ele) {
            var obj = tripWiseFeedObj[ele];
            obj['triptime'] = ele;
            tripData.add(obj);
          });
          // print("This is trip$tripData");
          setState(() {
            tripData.sort((productA, productB) =>
                int.parse("${productA['tripID']}")
                    .compareTo(int.parse("${productB['tripID']}")));

            tripInfoData = tripData;
            gotData = true;
            //  print("This is $tripInfoData");
          });
        } else {
          setState(() {
            tripInfoData = [];
            gotData = true;
          });
        }
      }
    } else {
      setState(() {
        tripInfoData = [];
        gotData = true;
      });
    }
  }

  getTripRoutIn(tripinfo) async {
    setState(() {
      loading = true;
    });
    var postionCords = this.preparePondsPositionCords(initialRegions);
    setState(() {
      pondPositions = postionCords;
    });
    //this.setState({pondPositions:postionCords})
    var query = this.query(tripinfo);
    try {
      //   print(deviceId);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/$deviceId/rpc/setContrl?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      var encodedCommand = jsonEncode(query);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        //  print(convertedData);
        preparePath(convertedData['hits']);
        setState(() {
          loading = false;
        });
        //   print("Success");
      }
    } catch (error) {
      //  print(error);
      setState(() {
        loading = false;
      });
    }
  }

  fetchTripInfoData() async {
    widget.trip_controller.clear();
    try{
    var gateway = await user.selGateway.gateWayMacId;
    //  print(gateway);
    var trip_info_data = await FirebaseFirestore.instance.collection("wifi_af_ver03_feeder_data").doc("${gateway}").collection("info").doc('livedata');
    trip_info_data.get().then((documentSnapshot) async {
      if (documentSnapshot.exists) {
        widget.trip_controller.addtrips(documentSnapshot.data(),deviceId);
        //print(documentSnapshot.data());
        // if(this.mounted){
        // setState(() {
        //   // tripInfoData = documentSnapshot.data();
        //   gotData = true;
        // });
        // }
        // parseResponseData(documentSnapshot.data(), 'live');
      }
    });
    }catch(error){
      print(error);
    }
  }

  slotForwardDate() {
    dateRightPressed();
    var day = startDate;
    var getDate = day.millisecondsSinceEpoch;
    var nextDay = DateTime.fromMillisecondsSinceEpoch(getDate);
    //  print("Helloghhjh");
    //  print(nextDay);
    // if ((day.day + 1) < DateTime.now().day) {
      nextDay = new DateTime(
          nextDay.year,
          nextDay.month,
          nextDay.day + 1,
          nextDay.hour,
          nextDay.minute,
          nextDay.second,
          nextDay.millisecond,
          nextDay.microsecond);
      setState(() {
        startDate = nextDay;
        tomorrow = false;
        lodingSlots = true;
        slotsArr = [];
      });

      var endDate = nextDay;
      var startTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
      //  print(startTime);
      // print("HElll;");
      var endTime =
          new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
      var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
      var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
      getTripData();
      // getTripinfoHistory(startEpoch, endEpoch);
    //   //this.getSlotsFromElasticSearchDB(startTime, endTime, "slot");
    // } else if ((day.day + 1) == DateTime.now().day) {
    //   //nextDay.setDate(day.getDate() + 1);
    //   // var nextDay = DateTime.fromMicrosecondsSinceEpoch(getDate);
    //   nextDay = new DateTime(
    //       nextDay.year,
    //       nextDay.month,
    //       nextDay.day + 1,
    //       nextDay.hour,
    //       nextDay.minute,
    //       nextDay.second,
    //       nextDay.millisecond,
    //       nextDay.microsecond);
    //   setState(() {
    //     startDate = nextDay;
    //     tomorrow = false;
    //     lodingSlots = true;
    //     slotsArr = [];
    //   });

    //   var endDate = nextDay;
    //   var startTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //   //   print(startTime);
    //   //  print("HElll1");
    //   var endTime =
    //       new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    //   var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    //   var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    //   ;
    //   fetchTripInfoData();
    //   // var paramObj = {
    //   //     // gateway: gateway,
    //   //     autofeederData: autofeederData,
    //   //     feederData: feederData
    //   // }
    //   //getSlotsFromFirebase(paramObj);

    //   //getSlotsFromElasticSearchDB(startTime, endTime, "slot")

    // } else {
    //   if (day.month == DateTime.now().month) {
    //     // Alert.alert(
    //     //     'NextAqua',
    //     //     `You are selected Date is out of range`,
    //     // )
    //   } else {
    //     nextDay = new DateTime(
    //         nextDay.year,
    //         nextDay.month,
    //         nextDay.day + 1,
    //         nextDay.hour,
    //         nextDay.minute,
    //         nextDay.second,
    //         nextDay.millisecond,
    //         nextDay.microsecond);
    //     setState(() {
    //       startDate = nextDay;
    //       tomorrow = false;
    //       lodingSlots = true;
    //       slotsArr = [];
    //     });

    //     var endDate = nextDay;
    //     var startTime =
    //         new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //     //  print(startTime);
    //     //  print("HElll2");
    //     var endTime = new DateTime(
    //         endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    //     var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    //     var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    //     getTripinfoHistory(startEpoch, endEpoch);
    //     //this.getSlotsFromElasticSearchDB(startTime, endTime, "slot");

    //     // let paramObj = {
    //     //     gateway: this.state.gateway,
    //     //     autofeederData: this.state.autofeederData,
    //     //     feederData: this.state.feederData
    //     // }
    //     // this.getSlotsFromFirebase(paramObj);
    //   }
    // }
  }

  prepareElasticRequestJsonForIndTrip(tripId, epochTime) {
    // var deviceId = deviceId;
    var start;
    if (dateTime != null) {
      start = dateTime;
    } else {
      start = DateTime.now();
    }
    start = new DateTime(start.year, start.month, start.day, 0, 0, 0, 0);

    var startTime = DateFormat.Hms().format(start);

    var min = (start.millisecondsSinceEpoch ~/ 1000).round();
    var max = min + 86400;
    var endTime = DateTime.fromMillisecondsSinceEpoch(max * 1000);
    
    var query = {
		"size":10,
			"query": {
				"bool": {
				
					"filter": [ {
						"match_phrase": {
							"deviceId": "$deviceId"
						}
						
					}, 
					{
						"match_phrase": {
							"tripID": "$tripId"
						}
						
					},
					{
						"match_phrase": {
							"epochTime": "$epochTime"
						}
						
					},
					{
						"range": {
							"epochTime": {
                  "format": "epoch_millis",
                  "gte": (start.millisecondsSinceEpoch ~/ 1000).round(),
                  "lte": (endTime.millisecondsSinceEpoch ~/ 1000).round()
                }
						}
					}]
				}
			}
		};
    return query;
  }


  prepareElasticRequestJsonForL1(order) {
    // var deviceId = deviceId;
    var v = DateTime.now();
    print(v.timeZoneName.toString());
    var start;
    if (dateTime != null) {
      start = dateTime;
    } else {
      start = DateTime.now();
    }
    start = new DateTime(start.year, start.month, start.day, 0, 0, 0, 0);

    var startTime = DateFormat.Hms().format(start);

    var min = (start.millisecondsSinceEpoch ~/ 1000).round();
    var max = min + 86400;
    var endTime = DateTime.fromMillisecondsSinceEpoch(max * 1000);
    // var query = {
		// 	"aggs": {
		// 		"tripID": {
		// 			"terms": {
		// 				"field": "tripID",
		// 				"order": {
		// 					"_key": "desc"
		// 				},
		// 				"size": 20
		// 			},
		// 			"aggs": {
		// 				"currentWeight": {
		// 					"max": {
		// 						"field": "currentWeight"
								
								
		// 					}
		// 				},
		// 				"weightToDisburse": {
		// 					"max": {
		// 						"field": "weightToDisburse"
		// 					}
		// 				}
		// 			}
		// 		}
		// 	},
		// 	"size":400,
		// 	"query": {
		// 		"bool": {
				
		// 			"filter": [ {
		// 				"match_phrase": {
		// 					"deviceId": "$deviceId"
		// 				}
		// 			}, {
		// 				"range": {
		// 					"epochTime": {
    //               "format": "epoch_millis",
    //               "gte": (start.millisecondsSinceEpoch ~/ 1000).round(),
    //               "lte": (endTime.millisecondsSinceEpoch ~/ 1000).round()
    //             }
		// 				}
		// 			}]
		// 		}
		// 	}
		// };
    var query = {
			"aggs": {
				"tripID": {
					"terms": {
						"field": "tripID",
						"order": {
							"_key": "desc"
						},
						"size": 20
					},
					"aggs": {
						"currentWeight": {
							"max": {
								"field": "currentWeight"
								
								
							}
						},
						"epochTime": {
							"max": {
								"field": "epochTime"
								
								
							}
						},
						"weightToDisburse": {
							"max": {
								"field": "weightToDisburse"
							}
						}
					}
				}
			},
			"size":40,
			"query": {
				"bool": {
				
					"filter": [ {
						"match_phrase": {
							"deviceId": "$deviceId"
						}
						
					}, 
					{
						"match_phrase": {
							"tripStatus": "3"
						}
						
					},
					{
						"range": {
							"epochTime": {
                  "format": "epoch_millis",
                  "gte": (start.millisecondsSinceEpoch ~/ 1000).round(),
                  "lte": (endTime.millisecondsSinceEpoch ~/ 1000).round()
                }
						}
					}]
				}
			}
		};
    return query;
  }



  parseTripData(response) async {
    var hitsJson = response['aggregations'];
    List dataArray = hitsJson['tripID']['buckets'];
    var data = [];
    for (var i = 0; i < dataArray.length; i++) {
      var dataItem = dataArray[i];
      var gateWayDataUpdatePkt = dataItem;

      data.add(gateWayDataUpdatePkt);
    }
    // data.sort((a, b) => a["key"].compareTo(b["key"]));
    // print('Low to hight in price: $data');
    return data;
  }

  fetchIndividualTripData(tripId, epochTime) async {
    var url;
    // 'http://34.69.73.157:9242/nextaquarpc_linemon/_search'

    url = Uri.parse("http://34.69.73.157:9242/aquabot/_search");

    Map<String, String> header = {'Content-Type': 'application/json'};
    var bodyJson = jsonEncode(prepareElasticRequestJsonForIndTrip(tripId, epochTime));
    // print(bodyJson);
    var response = await http.post(url,
        body: bodyJson, headers: header, encoding: Encoding.getByName("utf-8"));
    // print(response.body);
    var convertedData = json.decode(response.body);
    return convertedData;
  }

  getTripData() async {
    setState(() {
      tripsList = [];
      tripInfoData = [];
      gotData = false;
    });
    var response = await fetchTripData();
    print(response);
    var data = await parseTripData(response);
    print(data);
    setState(() {
      tripsList = data;
    });
    if(tripsList.isNotEmpty){
      for(var index = 0; index < tripsList.length; index++){
        var indResponse = await fetchIndividualTripData(tripsList[index]['key'], int.parse("${(tripsList[index]['epochTime']['value']).toStringAsFixed(0)}"));
        var data = await parseIndTripData(indResponse);
        setState(() {
          tripInfoData.addAll(data);
          if(tripInfoData.length >= 2){
          tripInfoData.sort((a, b) => a["tripID"].compareTo(b["tripID"]));
          }
        });
        print(data);
      }
    }
    setState(() {
      gotData = true;
    });
  }

  parseIndTripData(response) async {
    var hitsJson = response['hits'];
    List dataArray = hitsJson['hits'];
    var data = [];
    for (var i = 0; i < dataArray.length; i++) {
      var dataItem = dataArray[i];
      var gateWayDataUpdatePkt = dataItem['_source'];

      data.add(gateWayDataUpdatePkt);
    }
    data.sort((a, b) => a["epochTime"].compareTo(b["epochTime"]));
    print('Low to hight in price: $data');

    return data;
  }

  fetchTripData() async {
    var url;
    // 'http://34.69.73.157:9242/nextaquarpc_linemon/_search'

    url = Uri.parse("http://34.69.73.157:9242/aquabot/_search");

    Map<String, String> header = {'Content-Type': 'application/json'};
    var bodyJson = jsonEncode(prepareElasticRequestJsonForL1("avg"));
    // print(bodyJson);
    var response = await http.post(url,
        body: bodyJson, headers: header, encoding: Encoding.getByName("utf-8"));
    // print(response.body);
    var convertedData = json.decode(response.body);
    return convertedData;
  }


  getTripinfoHistory(start, end) async {
    widget.trip_controller.clear();
    try {
      var gateway = await user.selGateway.gateWayMacId;
      var trip_info_history_data = await FirebaseFirestore.instance
          .collection("wifi_af_ver03_feeder_data")
          .doc("${gateway}")
          .collection("history")
          .where('lut', isGreaterThanOrEqualTo: start)
          .where('lut', isLessThanOrEqualTo: end)
          .get()
          .then((documentSnapshot) {
        var docsLength = documentSnapshot.docs;
        var docsData = documentSnapshot.docs[0];
        widget.trip_controller.addtrips(documentSnapshot.docs[0].data(),deviceId);
        // print("sgkgsgdsfdsgdgdfggdgdg");
        // print(documentSnapshot.docs[0].data());
        // print("sdhgkhsghdshdsjkdsgjkdgjkgdjkgdjksggdsgdfgf");
        // setState(() {
        //   tripInfoData = docsData.data();
        // });
        // setState(() {
        //    gotData = true;
        // });
        // if (docsLength.length > 0) {
        //   this.parseResponseData(docsData.data(), 'hist');
        // } else {
        //   setState(() {
        //     tripInfoData = [];
        //   });
        // }
      });
    } catch (error) {
      if (this.mounted) {
        // setState(() {
          //tripInfoData = [];
          if (tripInfoData != null) {
            fetchTripInfoData();
          }
        // });
      }
      // try{
      // if(tripInfoData.length == 0){
      //   var gateway = await user.selGateway.gateWayMacId;
      // print(gateway);
      //  DocumentReference trip_info_data = await FirebaseFirestore.instance
      //     .collection("wifi_af_ver03_feeder_data")
      //     .doc("${gateway}")
      //     .collection("info")
      //     .doc('livedata');
      //     trip_info_data.get().then((DocumentSnapshot documentSnapshot) {
      //       if(documentSnapshot.exists){
      //         //print(documentSnapshot.data());
      //         setState(() {
      //           tripInfoData = documentSnapshot.data();
      //         });
      //         parseResponseData(documentSnapshot.data(), 'live');
      //       }
      //     });
      // }
      // }catch(error){
      //   print(error);
      // }
      //  print(error);
    }
  }

  onSlotsDate(date) {
    setState(() {
      showStartDate = false;
      startDate = date;
      date = date;
      tomorrow = false;
      feedInfoLoading = true;
      lodingSlots = true;
    });
     
  }

  String getText() {
    if (dateTime == null) {
      return "Select DateTime";
    } else {
      // print(dateTime.millisecondsSinceEpoch ~/ 1000);
      setState(() {
        epochtime = dateTime.millisecondsSinceEpoch ~/ 1000;
      });
      var epochDateTime = dateTime.millisecondsSinceEpoch ~/ 1000;
      /*  print("Ep[ocvhsahhs");
      print(epochDateTime);
      print(dateTime);*/

      onSlotsDate(dateTime);
      var selectedDate = dateTime.millisecondsSinceEpoch ~/ 1000;

    

      return DateFormat('dd/MM/yyyy').format(dateTime);
    }
  }

  dateLeftPressed() {
    if (dateTime != null) {
      var start = dateTime.millisecondsSinceEpoch ~/ 1000;
      // start.setHours(0, 0, 0, 0);
      var sDate = start;
      sDate = sDate - 86400;
      sDate = sDate * 1000;
      var convertedDate = DateTime.fromMillisecondsSinceEpoch(sDate);
      //dateTime = convertedDate;
      setState(() {
        dateTime = convertedDate;
      });
    }
  }

  dateRightPressed() {
    if (dateTime != null) {
      var start = dateTime.millisecondsSinceEpoch ~/ 1000;
      var currentDate = DateTime.now();
      var recievedDate = DateTime.fromMillisecondsSinceEpoch(start * 1000).day;
      var recievedMonth =
          DateTime.fromMillisecondsSinceEpoch(start * 1000).month;
      var recievedYear = DateTime.fromMillisecondsSinceEpoch(start * 1000).year;
      var currentDay = DateTime.now().day;
      var currentMonth = DateTime.now().month;
      var currentYear = DateTime.now().year;
      if ((currentDay == recievedDate) &&
          (currentMonth == recievedMonth) &&
          (currentYear == recievedYear)) {
        //  print("Date is Today");
      } else {
        var sDate = start;
        sDate = sDate + 86400;
        sDate = sDate * 1000;
        var convertedDate = DateTime.fromMillisecondsSinceEpoch(sDate);
        setState(() {
          dateTime = convertedDate;
        });
      }
    }
  }

  slotBackwardDate() {
    dateLeftPressed();
    /*  print("Hello");
    print(startDate);
    print("strafrf");*/
    var day = startDate;
    var getDate = day.millisecondsSinceEpoch;
    //  print(getDate);
    //   print("epich");
    var prevDay = DateTime.fromMillisecondsSinceEpoch(getDate);
    //   print(prevDay);
    //  print("oefsaf");
    prevDay = new DateTime(
        prevDay.year,
        prevDay.month,
        prevDay.day - 1,
        prevDay.hour,
        prevDay.minute,
        prevDay.second,
        prevDay.millisecond,
        prevDay.microsecond);
    setState(() {
      startDate = prevDay;
      tomorrow = false;
      slotsArr = [];
      lodingSlots = true;
    });
    var endDate = prevDay;
    var startTime =
        new DateTime(endDate.year, endDate.month, endDate.day, 0, 0, 0, 0);
    //  print(startTime);
    //  print("HElll;");
    var endTime =
        new DateTime(endDate.year, endDate.month, endDate.day, 23, 59, 59, 0);
    var endEpoch = endTime.millisecondsSinceEpoch ~/ 1000;
    var startEpoch = startTime.millisecondsSinceEpoch ~/ 1000;
    getTripData();
    //  getTripinfoHistory(startEpoch, endEpoch);
  }

  formatAMPM(date) {
    var hours = date.hour;
    var minutes = date.minute;
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours != null ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  convertTripTime(recievedTime) {
    //var recievedDate = DateTime.fromMillisecondsSinceEpoch(recievedTime*1000).hour;
    var setTime = DateFormat('HH:mm')
        .format(DateTime.fromMillisecondsSinceEpoch(recievedTime * 1000));
    return setTime;
  }

  getTotalTripFeed(data) {
    var totalTripFeed = 0.0;
    for (var i = 0; i < data.length; i++) {
      totalTripFeed += double.parse("${data[i]['currentWeight']}");
    }
    return totalTripFeed.toStringAsFixed(2);
  }

  startLoading() {
      setState(() {
        isloading = true;
      });
      Timer(Duration(seconds: 2), () {
        setState(() {
          isloading = false;
        });
      });
      print(isloading);
    }
  @override
  Widget build(BuildContext context) {
    //var deviceId = user.selGateway.botDevices[widget.index].macId;
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    var totalfeed = totalFeed;
    var mapIndex = 0;
    int currentSortColumn = 1;
    bool isAscending = true;
    final ScrollController _controller = ScrollController();
    Widget _verticaldivider = VerticalDivider(
      color: Colors.grey[300],
      thickness: 2,
    );

    

    return Container(
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: Color(0xFFE8EAF6),
      ),
      height: isMap == true
          ? MediaQuery.of(context).size.height * 0.90
          : MediaQuery.of(context).size.height * 0.50,
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${deviceName}-(${deviceId})",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg')),
                // totalFeed != null
                //     ? Text("Total feed :- ${totalFeed.toStringAsFixed(2)} kg",
                //         style: TextStyle(
                //             fontWeight: FontWeight.bold,
                //             fontSize: 15,
                //             fontFamily: 'BrandonReg'))
                //     : Text("Total feed :- 0.00 kg",
                //         style: TextStyle(
                //             fontWeight: FontWeight.bold,
                //             fontSize: 15,
                //             fontFamily: 'BrandonReg')),
              ],
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
            Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                      alignment: Alignment.center,
                      color: Colors.white,
                      height: 30,
                      width: 150,
                      child: Row(
                        //  mainAxisAlignment: MainAxisAlignment.center,
                        //  crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 25,
                            width: 25,
                            child: GestureDetector(
                              onTap: () {
                                slotBackwardDate();
                                setState(() {
                                  isMap = false;
                                });
                              },
                              child: Image.asset('images/arrowLeftGry.png'),
                            ),
                          ),
                          SizedBox(width: 10),
                          SizedBox(
                             height: 25,
                            width: 80,
                            child: DateTimePicker(
                              // textAlign: TextAlign.start,
                              decoration: InputDecoration(border: InputBorder.none, labelText: '${dateTime.day}-${dateTime.month}-${dateTime.year}', labelStyle: TextStyle(fontSize: textFontSize, color: Colors.black)),
                              style: TextStyle(fontSize: textFontSize, color: Colors.white),
                                      // initialDate: dateTime,
                                      firstDate: DateTime(DateTime.now().year - 5),
                                      lastDate: DateTime.now(),
                                      // dateLabelText: '${dateTime.day}-${dateTime.month}-${dateTime.year}',
                                      onChanged: (val){
                                        var newDate = DateFormat("yyyy-MM-dd").parse(val);
                                        setState(() {
                                          dateTime = DateTime(
                                            newDate.year,
                                            newDate.month,
                                            newDate.day,
                                          );
                                          textFontSize = 21.0;
                                        });
                                        getTripData();
                                      },
                                    ),
                          ),
                          // Container(
                          //   margin: EdgeInsets.only(top: 7),
                          //   child: SizedBox(
                          //     height: 25,
                          //     width: 80,
                          //     child: GestureDetector(
                          //       onTap: () {
                          //         // DateTimePicker(
                          //         //   initialDate: dateTime,
                          //         //   firstDate: DateTime(DateTime.now().year - 5),
                          //         //   lastDate: DateTime.now(),
                          //         //   dateLabelText: 'Date',
                          //         //   onChanged: (val){
                          //         //     getTripData();
                          //         //   },
                          //         // );
                          //         // pickDateTime(context);
                          //         setState(() {
                          //           isMap = false;
                          //         });
                          //       },
                          //       child: Text(
                          //         getText(),
                          //         style: TextStyle(color: Colors.black),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            height: 25,
                            width: 25,
                            child: GestureDetector(
                              onTap: () {
                                slotForwardDate();
                                setState(() {
                                  isMap = false;
                                });
                              },
                              child: Image.asset('images/arrowRightGrey.png'),
                            ),
                          ),
                        ],
                      )

                      /*ElevatedButton(
                                  style: ElevatedButton.styleFrom(primary: Colors.white),
                                  onPressed: () {
                                    pickDateTime(context);
                                  },
                                  child: Text(
                                    getText(),
                                    style: TextStyle(color: Colors.black),
                                  )),*/
                      ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              gotData == false ? Container(height: MediaQuery.of(context).size.height * 0.25, child: SpinKitThreeBounce(
                            size: 30.0,
                            color: Color(0xFF0170d0),
                          ),) :

                          Container(
                            color:Colors.white,
                            child: 
                            // GetX<Trip_Controller>(builder: (snapshot){
                            //   tripInfoData=snapshot.trip_list.isNotEmpty ? snapshot.trip_list : [];
                            //   return tripInfoData != null &&
                      tripInfoData != [] &&
                      tripInfoData.length > 0
                  ? Container(
                      padding: EdgeInsets.only(left: 5, right: 5),
                      height: tripInfoData.length > 4
                          ? MediaQuery.of(context).size.height * 0.30
                          : null,
                      // width: MediaQuery.of(context).size.height * 0.30,

                      width: MediaQuery.of(context).size.width * 0.90,

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3))
                          ]),
                      //  alignment: Alignment.center,
                      //  color: Colors.white,
                      // margin: EdgeInsets.only(top: 10),*/

                      child: SingleChildScrollView(
                        controller: _controller,
                        scrollDirection: Axis.vertical,
                        child: DataTable(
                          sortColumnIndex: currentSortColumn,
                          sortAscending: isAscending,

                          //  horizontalMargin: 2,
                          dataRowColor: MaterialStateColor.resolveWith(
                              (states) => Colors.white),

                          dataRowHeight: 35,
                          //  MediaQuery.of(context).size.height * 0.10,
                          headingRowHeight: 35,
                          // MediaQuery.of(context).size.height * 0.10,
                          headingRowColor: MaterialStateColor.resolveWith(
                              (states) => Colors.white),

                          showBottomBorder: true,
                          dividerThickness: 2,
                          columnSpacing: 1,
                          columns: [
                            DataColumn(
                                label: Text("Trip no",
                                    //  textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'BrandonReg'))),
                            DataColumn(label: _verticaldivider),
                            DataColumn(
                                label: Text("Start Time",

                                    //  textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'BrandonReg')),
                                onSort: (columnIndex, ascending) {
                                  setState(() {
                                    currentSortColumn = columnIndex;
                                    isAscending = ascending;
                                    if (isAscending == true) {
                                      print(tripInfoData[0]['tripID']);
                                      print(tripInfoData[1]['tripID']);
                                      print(tripInfoData[2]['tripID']);

                                      isAscending = false;
                                      /*
                                      1635570000 1635592000 1635581000
 */
                                      // sort the product list in Ascending, order by Price
                                      tripInfoData
                                        ..sort((productA, productB) =>
                                            int.parse("${productB['tripID']}")
                                                .compareTo(int.parse(
                                                    "${productA['tripID']}")));
                                    } else {
                                      isAscending = true;
                                      tripInfoData.sort((productA, productB) =>
                                          int.parse("${productA['tripID']}")
                                              .compareTo(int.parse(
                                                  "${productB['tripID']}")));

                                      // sort the product list in Descending, order by Price
                                      /*  tripInfoData.sort((productA, productB) =>
                                          productA['tripID']
                                              .compareTo((productB['tripID'])));*/
                                    }

                                    //  tripInfoData = tripInfoData;
                                  });
                                }),
                            DataColumn(label: _verticaldivider),
                            DataColumn(
                              label: Text("Dispense/Set",
                                  //  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'BrandonReg')),
                            ),
                          ],

                          rows: tripInfoData.map<DataRow>((index) {
                            mapIndex = mapIndex + 1;
                            return DataRow(cells: <DataCell>[
                              DataCell(Container(
                                // width: 50,
                                child: GestureDetector(
                                  onTap: () {
                                    if (isMap == true) {
                                      setState(() {
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    } else {
                                      setState(() {
                                        isMap = true;
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    }
                                    /*  setState(() {
                                      isMap = !isMap;
                                      //   isMap = isMap == false ? !isMap : true;

                                      tripid = index['tripID'];
                                      //    print(tripid);
                                    });*/

                                    // tripid = index['tripID'];
                                    //    print(tripid);
                                  },
                                  child: Text("${mapIndex}",
                                      // textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'BrandonReg')),
                                ),
                              )),
                              DataCell(_verticaldivider),
                              DataCell(Container(
                                //  width: 50,
                                child: GestureDetector(
                                  onTap: () {
                                    if (isMap == true) {
                                      setState(() {
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    } else {
                                      setState(() {
                                        isMap = true;
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    }
                                    /*  setState(() {
                                      isMap = !isMap;
                                      //   isMap = isMap == false ? !isMap : true;

                                      tripid = index['tripID'];
                                      //    print(tripid);
                                    });*/
                                  },
                                  child: Text(
                                      "${convertTripTime(index['tripID'])}",
                                      //  textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: 'BrandonReg')),
                                ),
                              )),
                              DataCell(_verticaldivider),
                              DataCell(Container(
                                //  width: 50,
                                child: GestureDetector(
                                  onTap: () {
                                    if (isMap == true) {
                                      setState(() {
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    } else {
                                      setState(() {
                                        isMap = true;
                                        tripid = index['tripID'];
                                        startLoading();
                                      });
                                    }

                                    /*   setState(() {
                                      isMap = !isMap;

                                      tripid = index['tripID'];
                                      //   print(tripid);
                                    });*/
                                  },
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text:
                                              "${index['currentWeight'].toStringAsFixed(2)}/${index['weightToDisburse']} kg ",
                                          style: TextStyle(
                                              fontFamily: 'BrandonReg',
                                              fontSize: 12,
                                              color: Colors.black),
                                        ),
                                        WidgetSpan(
                                          child: SizedBox(
                                              width: 30,
                                              height: 25,
                                              child: Image.asset(
                                                'images/mapIcon.png',
                                                color: Colors.black,
                                              )),
                                        ),
                                      ],
                                    ),

                                    /*  text: TextSpan(
                                                  text: "${index['tripFeed'].toStringAsFixed(2)}/${index['weightToDisburse']} kg",
                                                    style: TextStyle(fontSize: 12),
                                                  ),*/
                                  ),
                                ),
                              )),
                            ]);
                          }).toList(),
                        ),
                      ),
                    )
                  /*Container(
                            margin: EdgeInsets.only(top: 10),
                            child: DataTable(
                                dividerThickness: 2,
                                columns: [
                                  DataColumn(label: Text("Trip no", style: TextStyle(fontSize: 11))),
                                  DataColumn(label: Text("Start Time", style: TextStyle(fontSize: 11))),
                                  DataColumn(
                                    label: Text("Dispense/Set", style: TextStyle(fontSize: 11)),
                                  ),
                                ],
                                rows: tripInfoData.map<DataRow>((index){
                                  mapIndex = mapIndex+1;
                                  return DataRow(cells: <DataCell>[
                            DataCell(GestureDetector(child:  Text("${mapIndex}", style: TextStyle(fontSize: 12)),)),
                            DataCell(GestureDetector(child: Text(
                                "${convertTripTime(index['tripID'])}", style: TextStyle(fontSize: 12)),)),
                            DataCell(GestureDetector(child:  Text("${index['tripFeed'].toStringAsFixed(2)}/${index['weightToDisburse']} kg", style: TextStyle(fontSize: 12),),)),
                                        ]);
                                        })
                                    .toList(),
                              ),
                          ) */
                  : Container(color: Color(0xFFE8EAF6), height: MediaQuery.of(context).size.height * 0.25, child: Center(child: Text("Slots are not available")))
                  // })
                  ),
              gotData == false ? SizedBox() : Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.05),
                      child: Text(
                          tripInfoData != null
                              ? "Total: ${getTotalTripFeed(tripInfoData)} kg"
                              : "Total: 0 kg",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'BrandonReg')))),
              SizedBox(height: 20),
              isMap
                  ? Container(
                      alignment: Alignment.bottomCenter,

                      // height: MediaQuery.of(context).size.height * 0.30,

                      padding: EdgeInsets.only(left: 5, right: 5),
                      color:
                          isloading == true ? Color(0xFFE8EAF6) : Colors.white,
                      //  height: 350,
                      child: 
                      // StreamBuilder(
                      //     stream: homeBloc.botDetailsRefreshStream,
                      //     builder: (context, snapshot) {
                      //       if (isloading == true) {
                      //         return CircularProgressIndicator();
                      //       }
                      //       print("${snapshot.connectionState}");
                      //       print("$tripid  ${snapshot.data}");
                      //       print(isloading);
                      //       if (snapshot.hasData) {
                      //         bool status = snapshot.data;

                      //         return getmapview();
                      //       }

                            // return getListView();
                            isloading
                                ? Container(
                                    color: Color(0xFFE8EAF6),
                                    child: CircularProgressIndicator(
                                      backgroundColor: Color(0xFFE8EAF6),
                                    ))
                                : getmapview()
                          )
                  : SizedBox(),
              /* Align(alignment: Alignment.topRight, child: Container(child: Text(tripInfoData != null ? "Total: ${getTotalTripFeed(tripInfoData)} kg" : "Total: 0 kg", style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'BrandonReg'))))*/
            ]),
          ]),
    );
  }

  Future pickDateTime(BuildContext context) async {
    final date = await pickDate(context);
    if (date == null) return;
    setState(() {
      dateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
        context: context,
        initialDate: dateTime ?? initialDate,
        firstDate: DateTime(DateTime.now().year - 5),
        lastDate: DateTime.now());

    if (newDate == null) {
      return initialDate;
    } else {
      return newDate;
    }
  }

  Widget getmapview() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Map View",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontFamily: 'BrandonReg',
                  fontWeight: FontWeight.bold),
            ),
            GestureDetector(
                onTap: () {
                  setState(() {
                    isMap = !isMap;
                  });
                },
                child: Icon(Icons.clear))
          ],
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.30,

          //  height: 300,
          child: TripMapView(
            index: widget.index,
            tripid: tripid,
          ),
        )
      ],
    );
  }
}