import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/gateway.dart';
import 'package:flutter/material.dart';

class AquabotDeviceReport extends StatefulWidget {
  // const AquabotDeviceReport({ Key? key }) : super(key: key);

  @override
  State<AquabotDeviceReport> createState() => _AquabotDeviceReportState();
}

class _AquabotDeviceReportState extends State<AquabotDeviceReport> {
  AquaUser user = AquaUser.getInstance();
  List<Gateway> allGateways;
  List<Map> total_list = [];

  @override
  void initState(){
    super.initState();
    allGateways = user.gatewaysMap.entries.map((entry) => entry.value).toList();

    allGateways.map((index) {
      if(index.isWifi == true && index.aquaBotList != null && index.aquaBotList.isNotEmpty) {
        index.botDevices.map((element) {
          index.liveDeviceMap.forEach((key, value) {
            if(element.macId == value.deviceId) {
              total_list.add({
                'FarmName': index.farmName,
                'Name': "${element.deviceName} (${value.deviceId.replaceAll(RegExp(r'[^0-9]'), '')})",
                'gatewayid': index.gateWayMacId,
              });
            }
          });
        }).toList();
      }
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      
    );
  }
}