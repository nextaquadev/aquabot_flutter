import 'package:flutter/material.dart';

class CommandAlert extends StatelessWidget {
  // const Alert({ Key? key }) : super(key: key);
  final String title;
  final Color color;
  final String header;
  CommandAlert(this.title, this.color, this.header);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25),
      child: Center(
          child: AlertDialog(
        /* title: Text("$title",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color:color,
                fontFamily: 'BrandonReg')),*/
        content: SizedBox(
          // width: 100,
          child: Container(
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$header",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      color: Colors.black,
                      fontFamily: 'BrandonReg'),
                ),
                Text("$title",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: color,
                        fontFamily: 'BrandonReg')),
                GestureDetector(
                    child: Container(
                      height: 25,
                          width: 70,
                          //color: Colors.blueAccent,
                          decoration: BoxDecoration(color: Colors.blueAccent, border: Border.all(color: Colors.blueAccent),
                      borderRadius: BorderRadius.all(Radius.circular(
                              15) //                 <--- border radius here
                          ),),
                      child: Center(
                        child: Text("Ok",
                            style: TextStyle(
                                fontFamily: 'BrandonReg', color: Colors.black)),
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context, false);
                    }),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
