import 'package:flutter/material.dart';

class RenderToast extends StatelessWidget {
  //const ({ Key? key }) : super(key: key);
  final String name;
  final Color color;
  final GestureTapCallback onTap;
  RenderToast(this.name, this.color, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Container(
      //  margin: EdgeInsets.only(left: 20),
      height: 30,
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              padding: EdgeInsets.only(left: 5),
              //  margin: EdgeInsets.only(left: 30),
              child: Text(name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      fontFamily: 'BrandonReg',
                      color: Colors.white))),
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: onTap,
              child: Icon(Icons.clear, color: Colors.white),
              /* Text("Cancel", style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: 'BrandonReg', color: Colors.white))),
     */
            ),
          ),
        ],
      ),
    );
  }
}
