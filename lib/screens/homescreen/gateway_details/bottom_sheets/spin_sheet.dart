import 'dart:convert';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/send_command.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class Spin extends StatefulWidget {
  // const Spin({ Key? key }) : super(key: key);
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Spin({this.isSelected, this.cellDevice, this.index});

  @override
  _SpinState createState() => _SpinState();
}

class _SpinState extends State<Spin> {
  AquaUser user = AquaUser.getInstance();
  var deviceId = "";
  var data = {'showspinnerdialog': false, 'spintime': 10};
  var displayToast = false;
  var toastColor = Colors.green;
  var toastName = "";
  var enteredValues;
  var enteredApi;
  bool recievedData = true;
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    super.initState();
  }

  sendCommandDirectlyToDevice(command, methodName, deviceId) async {
    Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
    var path = Uri.parse(
        'https://mdash.net/api/v2/devices/${deviceId}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
    setState(() {
      enteredApi = path;
    });
    var encodedCommand = jsonEncode(command);
    var response = await http.post(path,
        body: encodedCommand,
        headers: header,
        encoding: Encoding.getByName("utf-8"));
    var convertedData = json.decode(response.body);
    // setState(() {
    //     enteredValues = convertedData;
    //   });
    return response;
  }

  spinnerTestHandler() {
    setState(() {
      data = {...data, 'showspinnerdialog': true};
    });
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  handelCancelConform() {
    setState(() {
      data = {...data, 'showspinnerdialog': false};
    });
    var configCmd = {
      'secondsToRun': data['spintime'],
    };
    //print(data['spintime']);
    // Navigator.pop(context, false);
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();
    setState(() {
      recievedData = true;
    });
    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  onTapOfConfirm(message, color) {
    Navigator.pop(context, false);
    var alert = showDialog(
        context: context,
        builder: (BuildContext context) {
          return Alert(message, color, "Spin Duration Settings");
        });
    return alert;
  }

  handelSpinnerTestConform() async {
    setState(() {
      recievedData = false;
    });
    if (this.mounted) {
      setState(() {
        data = {...data, 'showspinnerdialog': false};
      });
    }
    var configCmd = {
      'secondsToRun': data['spintime'],
    };
    var finalObj = {'frametype': "spinnerTest", 'commands': configCmd};
    // showOverlay(finalObj);

    try {
      var responseData =
          await sendCommandDirectlyToDevice(configCmd, 'spinnerTest', deviceId);
      ////print(responseData.body);
      if (this.mounted) {
        setState(() {
          data = {...data, 'spintime': 10};
        });
      }
      // onTapOfConfirm("Command sent successfully", Colors.green);
      //print(responseData);
      if (responseData.statusCode == 200) {
        getResponseBasedOnDevice("", 200);
      } else {
        getResponseBasedOnDevice("", 500);
      }
    } catch (error) {
      //print(error);
      if (this.mounted) {
        setState(() {
          data = {...data, 'spintime': 10};
        });
      }
      getResponseBasedOnDevice("", 500);
      // onTapOfConfirm("Sending command failed", Colors.red);
    }
  }

  @override
  Widget build(BuildContext context) {
    //print(user.selGateway.botDevices[widget.index]);
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    var displaySpintime = data['spintime'];
    return Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        margin: EdgeInsets.only(left: 20, right: 20),
        // height: 400,
        child: ListView(children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("${deviceName}-(${deviceId})",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          fontFamily: 'BrandonReg')),
                ],
              ),
              Divider(
                thickness: 1,
                color: Colors.black,
              ),
              recievedData == false
                  ? Container(
                      height: 350,
                      child: Center(
                        child: SpinKitThreeBounce(
                          size: 30.0,
                          color: Color(0xFF0170d0),
                        ),
                      ))
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Please enter spinner time in seconds",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17,
                                      fontFamily: 'BrandonReg',
                                      color: Color(0xFF05375a))),
                              Container(
                                margin: EdgeInsets.only(top: 5),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFFf2f2f2))),
                                    //hintText:  'Wait Time',
                                  ),
                                  initialValue: "$displaySpintime",
                                  keyboardType: TextInputType.number,
                                  onChanged: (text) {
                                    setState(() {
                                      data = {'spintime': int.parse(text)};
                                    });
                                  },
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  //SizedBox(height: 10,),
                                  displayToast == true
                                      ? Container(
                                          width: 220,
                                          //height: 30,
                                          child: RenderToast(
                                              toastName, toastColor, () {
                                            setState(() {
                                              displayToast = false;
                                            });
                                          }),
                                        )
                                      : SizedBox(),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: CommandButton(
                                          "Confirm", Colors.blueAccent, () {
                                        handelSpinnerTestConform();
                                      })),
                                ],
                              ),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.end,
                              //   children: [
                              //   // ElevatedButton(onPressed: () {
                              //   //                 handelCancelConform();
                              //   //               }, child: Text("Cancel", style: TextStyle(
                              //   //                   fontWeight: FontWeight.bold,
                              //   //                   fontSize: 20,
                              //   //                   fontFamily: 'BrandonReg', color: Colors.black87)), style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Color(0xFFE8EAF6)))),
                              //   ElevatedButton(onPressed: () {
                              //                   handelSpinnerTestConform();
                              //                 }, child: Text("Confirm", style: TextStyle(
                              //                     fontSize: 17,
                              //                     fontFamily: 'BrandonReg', color: Colors.white),), style: ElevatedButton.styleFrom(primary: Colors.blueAccent, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))))
                              // ],)
                            ],
                          ),
                        )
                      ],
                    )
            ],
          ),
        ])
        // child: Center(
        // child: Text(
        //     deviceId) /*AlertDialog(
        //   title: Text("Please enter spinner time in seconds"),
        //   content: SizedBox(
        //     width: 100,
        //     child: TextFormField(),
        //   ),
        // )*/
        // ),
        );
  }
}
