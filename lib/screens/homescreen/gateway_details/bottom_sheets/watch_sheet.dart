import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/showroute_maps.dart';
import 'package:aquabot/screens/homescreen/gateway_details/detailscell/cellonview.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Watch extends StatefulWidget {
  // const Watch({ Key? key }) : super(key: key);
  final BotModel cellDevice;
  final bool isSelected;

  final int index;
  Watch({this.isSelected, this.cellDevice, this.index});

  @override
  _WatchState createState() => _WatchState();
}

class _WatchState extends State<Watch> {
  AquaUser user = AquaUser.getInstance();
  String name;
  String deviceId;
  String version;
  var epochTime;
  var voltage;
  var tripId;
  var weight_to_disburse;
  var current_Weight;
  String deviceType;
  String siteId;
  String gpsLong = '';
  String gpsLat = '';

  int tripStatus = 0;
  String distanceBetweenPosts = '0';
  String distanceFrmPost = '0';
  int currentPost = -1;
  var rudderMotorSpeed;
  var bearingBtnStations;
  var botHeading;
  var cog;
  var sog;
  var m_fCurrent;
  var accuracy;
  var tripTime;
  var distanceStep;
  var rudderDecision;
  var rudderMotorDirection;

  var isInside;
  var perpHeight;

  var driveMotorSpeed;
  var desired_sog;
  sendCommandDirectlyToDevice(device, methodName) async {
    try {
      //print(methodName);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${device}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');

      var response = await http.get(
        path,
        headers: header,
      );
      if (response.statusCode == 200) {
        //var convertedData = json.decode(response.body);
      } else {}
    } catch (error) {
      //print(error);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sendCommandDirectlyToDevice(
        user.selGateway.botDevices[widget.index].macId, "getLiveData");
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: homeBloc.botDetailsRefreshStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            bool status = snapshot.data;
            return getListView();
          }

          return getListView();
        });
  }

  Widget getListView() {
    var device_Id = user.selGateway.botDevices[widget.index].macId;

    // user.selGateway.liveDeviceMap.values.toList().forEach((element) {
    //  if (device_Id == element.deviceId) {
    desired_sog = widget.cellDevice.liveModel.desired_sog;
    driveMotorSpeed = widget.cellDevice.liveModel.driveMotorSpeed;
    rudderMotorSpeed = widget.cellDevice.liveModel.rudderMotorSpeed;
    rudderMotorDirection = widget.cellDevice.liveModel.rudderMotorDirection;
    bearingBtnStations = widget.cellDevice.liveModel.bearingBtnStations;
    botHeading = widget.cellDevice.liveModel.botHeading;
    cog = widget.cellDevice.liveModel.cog;
    accuracy = widget.cellDevice.liveModel.accuracy;
    sog = (widget.cellDevice.liveModel.sog).toStringAsFixed(1);
    rudderDecision = widget.cellDevice.liveModel.rudderDecision;
    isInside = widget.cellDevice.liveModel.isInside;
    perpHeight = widget.cellDevice.liveModel.perpHeight;
    gpsLong = widget.cellDevice.liveModel.gpsLong;
    voltage = widget.cellDevice.liveModel.voltage;
    m_fCurrent = widget.cellDevice.liveModel.m_fCurrent;
    gpsLat = widget.cellDevice.liveModel.gpsLat;
    tripId = widget.cellDevice.liveModel.tripId;
    tripTime = widget.cellDevice.liveModel.tripTime;
    distanceStep = widget.cellDevice.liveModel.distanceStep;
    current_Weight =
        (widget.cellDevice.liveModel.current_Weight).toStringAsFixed(1);
    weight_to_disburse =
        (widget.cellDevice.liveModel.weight_to_disburse).toStringAsFixed(1);

    return Container(
      alignment: Alignment.topCenter,

      margin: EdgeInsets.only(left: 20, right: 20),
      //  height: 400,
      child: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      "${user.selGateway.botDevices[widget.index].deviceName}-(${user.selGateway.botDevices[widget.index].macId})",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          fontFamily: 'BrandonReg')),
                  Container(
                    // width: 120,
                    height: 30,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(20)),
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ShowRouteMap(
                                    deviceId: device_Id,
                                    celldevice: widget.cellDevice)));
                      },
                      child: Row(
                        children: [
                          Text(
                            "Map ",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Brandon',
                            ),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Colors.white,
                          )
                        ],
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: Color(0xFF0170d0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20))),
                    ),
                  )
                ],
              ),
              Divider(
                thickness: 1,
                color: Colors.black,
              ),
              Container(
                  padding: EdgeInsets.only(right: 5),
                  //  height: 80,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(10))),

                  //  width: 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        // height: 30,
                        //  width: 50,
                        child: CellDeviceOnView(
                          selDevice: widget.cellDevice,
                        ),
                      ),

                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            "Kg",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                fontFamily: 'BrandonReg',
                                color: Colors.black),
                          ),
                          Text(
                            "$current_Weight/$weight_to_disburse",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'BrandonReg',
                            ),
                          )
                        ],
                      ),
                      // SizedBox(width: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "SOG:$sog/$desired_sog",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'BrandonReg',
                            ),
                          ),
                          Text(
                            "TT:$tripTime",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'BrandonReg',
                            ),
                          ),
                          Text(
                            "DS:$distanceStep",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'BrandonReg',
                            ),
                          )
                        ],
                      )
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Drive Motor : ",
                                    children: [
                                  driveMotorSpeed == null ||
                                          driveMotorSpeed == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$driveMotorSpeed ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Rudder Motor : ",
                                    children: [
                                  rudderMotorSpeed == null ||
                                          rudderMotorSpeed == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$rudderMotorSpeed ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Direction : ",
                                    children: [
                                  rudderMotorDirection == 1
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "right ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "left ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Bearing Btn Stns : ",
                                    children: [
                                  bearingBtnStations == null ||
                                          bearingBtnStations == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$bearingBtnStations ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Bot Heading : ",
                                    children: [
                                  botHeading == null || botHeading == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$botHeading ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "COG : ",
                                    children: [
                                  cog == null || cog == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$cog ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "hAcc : ",
                                    children: [
                                  accuracy == null || accuracy == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$accuracy ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "SOG : ",
                                    children: [
                                  sog == null || sog == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$sog ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "Ruder decision : ",
                                    children: [
                                  rudderDecision == null || rudderDecision == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$rudderDecision ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "isInside : ",
                                    children: [
                                  isInside == 1
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "true ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "false ")
                                ])),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "PerpHeight : ",
                                    children: [
                                  perpHeight == null || perpHeight == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$perpHeight ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "gpsLong : ",
                                    children: [
                                  gpsLong == null || gpsLong == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$gpsLong ")
                                ])),
                            SizedBox(
                              width: 20,
                            ),
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "TripId : ",
                                    children: [
                                  gpsLong == null || gpsLong == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$tripId ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "m_fVoltage : ",
                                    children: [
                                  voltage == null || voltage == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$voltage ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "m_fCurrent : ",
                                    children: [
                                  m_fCurrent == null || m_fCurrent == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$m_fCurrent ")
                                ])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(
                                text: TextSpan(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                    ),
                                    text: "gpsLat : ",
                                    children: [
                                  gpsLat == null || gpsLat == ''
                                      ? TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "NA ")
                                      : TextSpan(
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'BrandonReg',
                                            fontWeight: FontWeight.bold,
                                          ),
                                          text: "$gpsLat ")
                                ])),
                          ],
                        )
                      ],
                    ),
                  ),
                  /*  Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "TripId",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'BrandonReg',
                              ),
                            ),
                            tripId == null || tripId == ''
                                ? Text(
                                    "NA",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                : Text(
                                    "$tripId",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'BrandonReg',
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                          ],
                        ),
                      )*/
                ],
              )
            ]),
      ),
    );
  }
}
