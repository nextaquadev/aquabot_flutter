import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/route_create_controller.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/multialert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/mapconfig/mapconfig.dart';
import 'package:aquabot/utils/aqua_utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ShowRouteMap extends StatefulWidget {
  // const ShowRouteMap({ Key? key }) : super(key: key);
  final deviceId;
  final BotModel celldevice;
  var route_controller = Get.put(Route_Controller());

  ShowRouteMap({Key key, this.deviceId, this.celldevice}) : super(key: key);

  @override
  State<ShowRouteMap> createState() => _ShowRouteMapState();
}

class _ShowRouteMapState extends State<ShowRouteMap> {
  LatLng _initialcameraposition = LatLng(17.448227813051307, 78.36215291172266);
  GoogleMapController _controller;
  bool isLoading = true;
  bool showalert = false;
  Location _location = Location();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Set<Polyline> _polyline = {};
  Set<Polyline> _livePolyline = {};
  AquaUser user = AquaUser.getInstance();
  var devicepoints;
  @override
  void dispose() {
    try {
      _controller?.dispose();
    } catch (err) {}
    // TODO: implement dispose
    //  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  void initState() {
    widget.route_controller.clearAll();
    // TODO: implement initState
    getDevicePoints(widget.deviceId);
    _initialcameraposition = LatLng(double.parse("${widget.celldevice.lat}"),
        double.parse("${widget.celldevice.long}")); //LatLng(lat, long);
    widget.route_controller.addlable("label", {
      "m_lat": "${widget.celldevice.lat}",
      "m_lng": "${widget.celldevice.long}"
    });

    // updateLocation(widget.celldevice.lat, widget.celldevice.long);

    //  devicepoints = getDevicePoints(widget.deviceId);

    super.initState();
  }

  onpressofsend() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultiAlert("Are you sure you want to send command ?",
              Colors.black, "Are you sure you want to send command ?", () {
            sendcommandtodevice();
          }, () {
            Navigator.pop(context);
          });
        });
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
  }

  Future<List<BotMarker>> getDevicePoints(String deviceId) async {
    try {
      final urlStr =
          'https://mdash.net/api/v2/devices/$deviceId/rpc/getRoute?access_token=JKrRbHr8MnhdUUso91Zmm91A';
      var resp = await http.get(Uri.parse(urlStr));
      if (resp == null) {
        setState(() {
          isLoading = false;
          showalert = true;
        });
      } else {
        if (resp.body.length == 0) {
          setState(() {
            isLoading = false;

            showalert = true;
          });

          return null;
        }
        var data = jsonDecode(resp.body);
        print(data);
        //AlertDialog(title: Text(data.toString()));
        if (data == null) {
          setState(() {
            isLoading = false;

            showalert = true;
          });
        } else {
          if (data['m_arrLocations'] != null) {
            var markers = {};
            for (int i = 0; i < (data['m_arrLocations'] as List).length; i++) {
              Map e = (data['m_arrLocations'] as List)[i];
              widget.route_controller.addpoints("$i", e);

              // }
            }
            setState(() {
              isLoading = false;
            });

            //  updateLocation();
            //  _addPolyline();
            //   return markers;
            // (data['m_arrLocations'] as List)
            //     .map((e) => BotMarker.toMJson(e))
            //     .toList();
          } else {
            setState(() {
              isLoading = false;

              showalert = true;
            });
          }
        }
        return null;
      }
      return null;
    } catch (e) {
      setState(() {
        isLoading = false;

        showalert = true;
      });
      throw Exception(e);
    }
  }

  Future<String> setDevicePoints(String deviceId, Map devicePoints) async {
    try {
      // final deviceId = 'device421';
      final urlStr =
          'https://mdash.net/api/v2/devices/$deviceId/rpc/setRoute?access_token=JKrRbHr8MnhdUUso91Zmm91A';

      var botJson = [];
      devicePoints.forEach((key, value) {
        var lat = double.parse("${value['m_lat']}");
        var lng = double.parse("${value['m_lng']}");
        if (lat != 0.0 && lng != 0.0) {
          botJson.add({
            "m_lat": "${lat.toStringAsFixed(6)}",
            "m_lng": "${lng.toStringAsFixed(6)}",
          });
        } else {
          botJson.add({"m_lat": "0", "m_lng": "0"});
        }
      });
      print(botJson);

      var botMap = {"m_arrLocations": botJson};

      var client = http.Client();

      Map<String, String> headers = {
        'Content-Type': 'application/json; charset=UTF-8',
      };
      var response = await client.post(Uri.parse(urlStr),
          body: jsonEncode(botMap), headers: headers);

      if (response != null) {
        var data = jsonDecode(response.body);
        if (data["statusCode"] == 200) {
          saveDataToFirebase(deviceId, devicePoints);

          return data.toString();
        } else {
          Navigator.pop(context);

          showDialog(
              context: context,
              builder: (BuildContext context) {
                return Alert(
                    "Sending command failed", Colors.redAccent, "Aquabot");
              });
        }
      }
      return null;
    } catch (e) {
      Navigator.pop(context);

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Alert("Sending command failed", Colors.redAccent, "Aquabot");
          });
      throw Exception(e);
    }
  }

  savelabelToFirebase(deviceId, labeldata) async {
    var siteId = user.selGateway.gateWayMacId;

    var aquabotlist = user.selGateway.aquaBotList;
    var newdata = labeldata['label'];
    var lat = double.parse("${newdata['m_lat']}");
    var lng = double.parse("${newdata['m_lng']}");
    aquabotlist.forEach((key, value) {
      if (key == deviceId) {
        value['lat'] = double.parse(lat.toStringAsFixed(6));
        value['long'] = double.parse(lng.toStringAsFixed(6));
      }
    });

    var data = {"aquaBotList": aquabotlist};
    if (aquabotlist != null) {
      try {
        DocumentReference dr = await FirebaseFirestore.instance
            .collection("gateways")
            .doc(siteId)
            .collection("info")
            .doc('profiledata')
            .update(data)
            .then((value) {});
      } catch (e) {
        widget.route_controller.updatealertfalse();

        Navigator.pop(context);

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Alert(
                  "Sending command failed", Colors.redAccent, "Aquabot");
            });
      }
    }
  }

  saveDataToFirebase(deviceId, devicePoints) {
    var siteId = user.selGateway.gateWayMacId;
    var botJson = {};
    devicePoints.forEach((key, value) {
      var lat = double.parse("${value['m_lat']}");
      var lng = double.parse("${value['m_lng']}");
      if (lat != 0.0 && lng != 0.0) {
        botJson["$key"] = {
          "latitude": double.parse(lat.toStringAsFixed(6)),
          "longitude": double.parse(lng.toStringAsFixed(6))
        };
      } else {}
    });

    if (botJson != null) {
      var dr = FirebaseFirestore.instance
          .collection("gateways")
          .doc(siteId)
          .collection("info")
          .doc('profiledata');

      dr.update({'aquaBotList.$deviceId.regions.$deviceId': botJson}).then(
          (value) {
        widget.route_controller.updatealertfalse();

        Navigator.pop(context);

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Alert("SuccessFully Updated", Colors.green, "Aquabot");
            });
      }).catchError((error) {
        widget.route_controller.updatealertfalse();

        print("Failed to update user: $error");
        Navigator.pop(context);

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return Alert(
                  "Sending command failed", Colors.redAccent, "Aquabot");
            });
      });
    }
  }

  sendcommandtodevice() async {
    Navigator.pop(context);
    loader();
    try {
      savelabelToFirebase(
          widget.celldevice.macId, widget.route_controller.labelpoint);
      setDevicePoints(
          widget.celldevice.macId, widget.route_controller.allpoints);
    } catch (e) {
      widget.route_controller.updatealertfalse();
      Navigator.pop(context);

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Alert("Sending command failed", Colors.redAccent, "Aquabot");
          });
    }
  }

  loader() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
            margin: EdgeInsets.only(left: 25, right: 25),
            height: 100,
            child: Center(
                child: AlertDialog(
              content: Container(
                height: 100,
                child: SpinKitRing(
                  size: 30.0,
                  color: Color(0xFF0170d0),
                ),
              ),
            )),
          );
        });
  }

  _addPolyline(Map allpoints) {
    int i = 0;
    List<LatLng> points = [];
    int k = 0;

    allpoints != null
        ? allpoints.forEach((key, value) {
            var lat = double.parse(value['m_lat']);
            var lng = double.parse(value['m_lng']);
            if (lat != 0.0 && lng != 0.0) {
              points.add(LatLng(lat, lng));
              _addNameMarkers(LatLng(lat, lng), key);
            }
          })
        : "";

    var polyline_id = PolylineId(AquaUtils().getRandomString(5));

    final polyline = Polyline(
      polylineId: PolylineId(AquaUtils().getRandomString(5)),
      color: Colors.white,
      points: points,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
      width: 3,
      patterns: [PatternItem.dot, PatternItem.gap(5)],
    );
    i = i + 1;
    widget.route_controller.addpolyline(polyline);
  }

  Future _addNameMarkers(latlang, i) async {
    final MarkerId markerId = MarkerId("$i");
    //  botmarker); //MarkerId(AquaUtils().getRandomString(5));
    LatLng ll = latlang;
    final Uint8List markerIcon =
        await AquaUtils().getBytesFromAsset('images/yellow_home.png', 45);

    BitmapDescriptor bitmapDescriptor = i == "0"
        ? BitmapDescriptor.fromBytes(markerIcon)
        : await AquaUtils()
            .getClusterMarker("$i", Color(0xFFFFC107), 20, Color(0xFFFFC107));
    Marker marker = Marker(
      markerId: markerId,
      // draggable: true,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      // infoWindow: InfoWindow(
      //   title: botmarker.name,
      //   snippet: ' ',
      // ),
      // icon: bitmapDescriptor,
      icon: bitmapDescriptor,
    );
    /* onDragEnd: (LatLng ll) {
          this.markerSelected(ll, markerId);
        },
        onTap: () {
          this.markerTapped(botmarker);
        });*/
    //  markers[markerId] = marker;
    widget.route_controller.addmarkers(markerId, marker);

    /*  if (markers.length == (postCount + botCount)) {
      // setState(() {});
      homeBloc.refreshMap(true);
    }*/
  }

  Future _addLabelMarkers(point) async {
    MarkerId markerId = MarkerId("label");
    // LatLng ll = botmarker.latlang;
    var newpoint = point['label'];
    var lat = double.parse(newpoint['m_lat']);
    var lng = double.parse(newpoint['m_lng']);
    LatLng ll = LatLng(lat, lng);

    BitmapDescriptor bitmapDescriptor = await AquaUtils().getClusterMarkerName(
      widget.celldevice.deviceName,
      Colors.white,
      40,
    );
    Marker marker = Marker(
      markerId: markerId,
      draggable: false,
      position:
          ll, //With this parameter you automatically obtain latitude and longitude
      infoWindow: InfoWindow(
        title: "Label here",
        //  snippet: 'This ssss good',
      ),
      icon: bitmapDescriptor, /* BitmapDescriptor.fromBytes(markerIcon),*/
    );
    widget.route_controller.addmarkers(markerId, marker);
  }

  Future<BitmapDescriptor> getClusterMarker(
    String title,
    Color textColor,
    double radius,
  ) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    // final Paint paint = Paint()..color = clusterColor;
    final TextPainter textPainter = TextPainter(
        textDirection: TextDirection.ltr, textAlign: TextAlign.center);
    // final double radius = 60;
    // canvas.drawCircle(
    //   Offset(radius, radius),
    //   radius,
    //   paint,
    // );

    textPainter.text = TextSpan(
      text: title,
      style: TextStyle(
        fontSize: title.length == 1 ? radius : radius - 5,
        fontWeight: FontWeight.bold,
        color: textColor,
      ),
    );
    textPainter.layout();
    textPainter.paint(
      canvas,
      Offset(
        0,
        0,
      ),
    );
    final image = await pictureRecorder.endRecording().toImage(
          radius.toInt(),
          radius.toInt(),
        );
    final data = await image.toByteData(format: ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  Widget getMapView(snapshot) {
    _addPolyline(snapshot.allpoints.value);
    _addLabelMarkers(snapshot.labelpoint.value);

    //updateLocation(snapshot.allpoints.value);

    //  addMarkers();

    Widget mapWid = GoogleMap(
      //  onCameraMove: _updatecamera,
      gestureRecognizers: Set()
        ..add(Factory<OneSequenceGestureRecognizer>(
            () => new EagerGestureRecognizer()))
        ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
        ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
        ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
        ..add(Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer())),
      initialCameraPosition:
          CameraPosition(target: _initialcameraposition, zoom: 19),
      mapType: MapType.satellite,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      zoomGesturesEnabled: true,
      zoomControlsEnabled: false,
      myLocationButtonEnabled: false,
      mapToolbarEnabled: false,
      markers: Set<Marker>.of(snapshot.markers.values),
      polylines: Set<Polyline>.of(snapshot.polylines.value),
      onLongPress: (latlang) {
        _addMarkerLongPressed(latlang);

        showBarModalBottomSheet(
          barrierColor: Color.fromRGBO(0, 0, 0, 0.3),
          context: context,
          builder: (context) => Container(
            color: Color(0xFFE8EAF6),
            height: MediaQuery.of(context).size.height * 0.5,
            child: MapConfig(
              selDevice: widget.celldevice,
              selLatLong: latlang,
              allpoints: snapshot.allpoints,
            ),
          ),
        ).then((value) {
          widget.route_controller.removemarkers('touched');
        });
      },
    );

    return mapWid;
  }

  shownewalert(cont) {
    return showDialog(
        context: cont,
        builder: (cont) {
          return AlertDialog(
            title: Text("Warning"),
            content: Text(
                "You have some changes Please send command otherwise you will loose your changes ? "),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                      child: Container(
                        height: 25,
                        width: 70,
                        //color: Colors.red,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          border: Border.all(color: Colors.red),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  20) //                 <--- border radius here
                              ),
                        ),
                        child: Center(
                          child: Text("Cancel",
                              style: TextStyle(
                                  fontFamily: 'BrandonReg',
                                  color: Colors.white)),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      }),
                  GestureDetector(
                      child: Container(
                        height: 25,
                        width: 70,
                        //color: Colors.blueAccent,
                        decoration: BoxDecoration(
                          color: Colors.blueAccent,
                          border: Border.all(color: Colors.blueAccent),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  15) //                 <--- border radius here
                              ),
                        ),
                        child: Center(
                          child: Text("Ok",
                              style: TextStyle(
                                  fontFamily: 'BrandonReg',
                                  color: Colors.white)),
                        ),
                      ),
                      onTap: () {
                        widget.route_controller.updatealertfalse();

                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      }),
                ],
              )
            ],
          );
        });
  }

  _addMarkerLongPressed(LatLng latlang) {
    double lat = latlang.latitude;
    double long = latlang.longitude;

    MarkerId markerId = MarkerId("touched");
    //print("This is marker:$markerId     $latlang");

    Marker marker = Marker(
      // This marker id can be anything that uniquely identifies each marker.
      markerId: markerId,
      draggable: false,
      visible: true,

      position: latlang,
      infoWindow: InfoWindow(
        title: "Touched",
        //  snippet: '5 Star Rating',
      ),
      icon: BitmapDescriptor.defaultMarker,
    );
    widget.route_controller.addmarkers(markerId, marker);
  }

  @override
  Widget build(BuildContext context) {
    //   getDevicePoints(widget.deviceId);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 30,
            backgroundColor: Colors.white,
            leading: GestureDetector(
              onTap: () {
                bool alert = widget.route_controller.sent;

                alert == true
                    ? shownewalert(context)
                    : Navigator.of(context).pop();

                //  Navigator.of(context).pop();
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
            ),
            title: GestureDetector(
              onTap: () {
                bool alert = widget.route_controller.sent;

                alert == true
                    ? shownewalert(context)
                    : Navigator.of(context).pop();

                // Navigator.of(context).pop();
              },
              child: Container(
                alignment: Alignment.centerLeft,
                height: 25,
                width: MediaQuery.of(context).size.width * 0.60,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Text(
                    "${user.selGateway.farmName}",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontFamily: 'Brandon'),
                  ),
                ),
              ),
            ),
            actions: [
              Container(
                // width: 120,
                height: 30,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(20)),
                child: ElevatedButton(
                  onPressed: () {
                    onpressofsend();
                  },
                  child: Row(
                    children: [
                      Text(
                        "Send",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Brandon',
                        ),
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Color(0xFF0170d0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
          body: SafeArea(
              child: isLoading == true
                  ? SpinKitThreeBounce(
                      size: 30.0,
                      color: Color(0xFF0170d0),
                    )
                  : showalert == true
                      ? Center(child: Text("Device Offline/No Data Found"))
                      : Container(
                          color: Colors.white,
                          child: GetX<Route_Controller>(builder: (snapshot) {
                            return getMapView(snapshot);
                          })))),
    );
  }
}
