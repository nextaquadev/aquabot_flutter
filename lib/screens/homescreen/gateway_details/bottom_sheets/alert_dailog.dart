import 'package:flutter/material.dart';

class AlertDail extends StatelessWidget {
  // const Alert({ Key? key }) : super(key: key);
  final String title;
  final Color color;
  final String header;
  final Function() onPressOfOk; 
  AlertDail(this.title, this.color, this.header, this.onPressOfOk);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 25),
      child: Center(
          child: AlertDialog(
        /* title: Text("$title",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color:color,
                fontFamily: 'BrandonReg')),*/
        content: SizedBox(
          // width: 100,
          child: Container(
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "$header",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      color: Colors.black,
                      fontFamily: 'BrandonReg'),
                ),
                Text("$title",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        color: color,
                        fontFamily: 'BrandonReg')),
                GestureDetector(
                    child: Text("Ok",
                        style: TextStyle(
                            fontFamily: 'BrandonReg', color: Colors.black)),
                    onTap: onPressOfOk),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
