import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/command_button.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Tune extends StatefulWidget {
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Tune({this.isSelected, this.cellDevice, this.index});
  //const Tune({ Key? key }) : super(key: key);

  @override
  _TuneState createState() => _TuneState();
}

class _TuneState extends State<Tune> {
  AquaUser user = AquaUser.getInstance();
  bool sendingCommand = false;
  var deviceId = "";
  var enteredApi;
  var enteredData;
  var displayToast = false;
  var toastColor = Colors.green;
  var toastName = "";
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  var data = {
    'deviceId': null,
    'deviceType': null,
    'tuneMaxRudderDutyCycle': 0,
    'showMsz': false,
    'tuneMinRudderDutyCycle': 0,
    'showMszMinRudderDutyCycle': false,
    'tunePerpHeightLimit': 0,
    'showMsztunePerpHeightLimit': false,
    'tunePerpHeightGain': 0,
    'showMsztunePerpHeightGain': false,
    'tuneBearingGain': 0,
    'showMsztuneBearingGain': false,
    'tuneFwdAngleLimit': 0,
    'showMsztuneFwdAngleLimit': false,
    'tuneAutoModeSpeed': 0,
    'showMsztuneAutoModeSpeed': false,
    'isLoading': true,
    'respMsz': '',
    'showSnackbar': false,
    'mszColor': 'red',
    'tuneBotHeadinCorrection': 0,
    'tuneTurnDistaceLimit': 0,
    'tuneFeedStep': 0,
    'shouldSpin': 0,
    'spinnerMotorDutyCycle': 0,
    'handBrake': 0,
    'dataSendLoopWD': 0,
    'k_debug': 0,
    'hasADCI2C': 0,
    'PerpHtKp': 0,
    'PerpHtKd': 0,
    'PerpHtKi': 0,
    'AngleKp': 0,
    'logLavel': 0,
    'AngleKd': 0,
    'AngleKi': 0,
    'tuneDriveGain': 0,
    'tuneNearPostLimit': 0,
    'MaxTripTime': 0
  };
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    getConfigurationDataFromDevice();
    super.initState();
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  getConfigurationDataFromDevice() async {
    var deviceid = deviceId;
    setState(() {
      data = {
        ...data,
        'deviceId': deviceid,
        'isLoading': true,
      };
      sendingCommand = true;
    });
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var cmdObj = {'version': version, 'deviceId': deviceid};
    try {
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${deviceid}/rpc/getTuneValues?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var encodedcmdObj = jsonEncode(cmdObj);
      var getData = await http.post(path,
          body: encodedcmdObj,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (getData.statusCode == 200) {
        var responseData = json.decode(getData.body);
        //print(responseData);
        var isdat = {'isLoading': false, 'deviceType': "Route"};

        setState(() {
          data = {
            ...data,
            'tuneNearPostLimit': responseData.keys.contains('tuneNearPostLimit')
                ? responseData['tuneNearPostLimit']
                : "0",
            'tuneDriveGain': responseData.keys.contains('tuneDriveGain')
                ? responseData['tuneDriveGain']
                : "0",
            'tuneBotHeadinCorrection':
                responseData.keys.contains('tuneBotHeadinCorrection')
                    ? responseData['tuneBotHeadinCorrection']
                    : "0",
            'tuneTurnDistaceLimit':
                responseData.keys.contains('tuneTurnDistaceLimit')
                    ? responseData['tuneTurnDistaceLimit']
                    : "0",
            'tunePerpHeightLimit':
                responseData.keys.contains('TunePerpHeightLimit')
                    ? responseData['TunePerpHeightLimit']
                    : "0",
            'tunePerpHeightGain':
                responseData.keys.contains('tunePerpHeightGain')
                    ? responseData['tunePerpHeightGain']
                    : "0",
            'tuneBearingGain': responseData.keys.contains('tuneBearingGain')
                ? responseData['tuneBearingGain']
                : "0",
            'tuneFwdAngleLimit': responseData.keys.contains('TuneFwdAngleLimit')
                ? responseData['TuneFwdAngleLimit']
                : "0",
            'tuneMaxRudderDutyCycle':
                responseData.keys.contains('MaxRudderDutyCycle')
                    ? responseData['MaxRudderDutyCycle']
                    : "0",
            'tuneMinRudderDutyCycle':
                responseData.keys.contains('MinRudderDutyCycle')
                    ? responseData['MinRudderDutyCycle']
                    : "0",
            'tuneAutoModeSpeed': responseData.keys.contains('tuneAutoModeSpeed')
                ? responseData['tuneAutoModeSpeed']
                : "0",
            'tuneFeedStep': responseData.keys.contains('tuneFeedStep')
                ? responseData['tuneFeedStep']
                : "0",
            'shouldSpin': responseData.keys.contains('shouldSpin')
                ? responseData['shouldSpin']
                : "0",
            'spinnerMotorDutyCycle':
                responseData.keys.contains('spinnerMotorDutyCycle')
                    ? responseData['spinnerMotorDutyCycle']
                    : "0",
            'handBrake':
                responseData.keys.contains('handBrake') == true ? 1 : 0,
            'dataSendLoopWD': responseData.keys.contains('dataSendLoopWD')
                ? responseData['dataSendLoopWD']
                : "0",
            'logLavel': responseData.keys.contains('logLavel')
                ? responseData['logLavel']
                : "0",
            'k_debug': responseData.keys.contains('k_debug')
                ? responseData['k_debug']
                : "0",
            'hasADCI2C': responseData.keys.contains('hasADCI2C')
                ? responseData['hasADCI2C']
                : "0",
            'PerpHtKp': responseData.keys.contains('PerpHtKp')
                ? responseData['PerpHtKp']
                : "--",
            'PerpHtKd': responseData.keys.contains('PerpHtKd')
                ? responseData['PerpHtKd']
                : "0",
            'PerpHtKi': responseData.keys.contains('PerpHtKi')
                ? responseData['PerpHtKi']
                : "0",
            'AngleKp': responseData.keys.contains('AngleKp')
                ? responseData['AngleKp']
                : "0",
            'AngleKd': responseData.keys.contains('AngleKd')
                ? responseData['AngleKd']
                : "0",
            'AngleKi': responseData.keys.contains('AngleKi')
                ? responseData['AngleKi']
                : "0",
            'MaxTripTime': responseData.keys.contains('MaxTripTime')
                ? responseData['MaxTripTime']
                : "0",
            'deviceType': responseData.keys.contains('deviceType')
                ? responseData['deviceType']
                : "Line",
            'deviceId': responseData.keys.contains('deviceId')
                ? responseData['deviceId']
                : data['deviceId'],
            'isLoading': false,
          };
          sendingCommand = false;
        });
        ////print(data);
      } else {
        setState(() {
          data = {
            ...data,
            'showSnackbar': true,
            'isLoading': false,
          };
          sendingCommand = false;
        });
      }
    } catch (error) {
      setState(() {
        data = {
          ...data,
          'showSnackbar': true,
          'isLoading': false,
        };
        sendingCommand = false;
      });
      //print(error);
    }
  }

  tuneMaxRudderDutyCycleInputChange(val) {
    setState(() {
      data = {
        ...data,
        'tuneMaxRudderDutyCycle': val,
      };
    });
  }

  onDismissSnackBar(val) {
    setState(() {
      data = {...data, 'showSnackbar': false};
    });
  }

  tuneMinRudderDutyCycleInputChange(val) {
    setState(() {
      data = {...data, 'tuneMinRudderDutyCycle': val};
    });
  }

  tunePerpHeightLimitInputChange(val) {
    setState(() {
      data = {...data, 'tunePerpHeightLimit': val};
    });
  }

  tunePerpHeightGainInputChange(val) {
    setState(() {
      data = {'tunePerpHeightGain': val};
    });
  }

  tuneBearingGainInputChange(val) {
    setState(() {
      data = {...data, 'tuneBearingGain': val};
    });
  }

  tuneFwdAngleLimitInputChange(val) {
    setState(() {
      data = {...data, 'tuneFwdAngleLimit': val};
    });
  }

  tuneAutoModeSpeedInputChange(val) {
    setState(() {
      data = {...data, 'tuneAutoModeSpeed': val};
    });
  }

  sendTuneDetalisCommand(methodName) {
    setState(() {
      data = {...data, 'isLoading': true};
      sendingCommand = true;
    });
    var deviceid = deviceId;
    //get current time in seconds
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var configCmd;
    if (methodName == 'setTuneValues') {
      configCmd = {
        'version': version,
        'deviceId': deviceid,
        'tuneDriveGain': data['tuneDriveGain'] != null
            ? int.parse("${data['tuneDriveGain']}")
            : "--",
        'tuneNearPostLimit': data['tuneNearPostLimit'] != null
            ? int.parse("${data['tuneNearPostLimit']}")
            : "--",
        'tuneBotHeadinCorrection': data['tuneBotHeadinCorrection'] != null
            ? int.parse("${data['tuneBotHeadinCorrection']}")
            : "--",
        'tuneTurnDistaceLimit': data['tuneTurnDistaceLimit'] != null
            ? int.parse("${data['tuneTurnDistaceLimit']}")
            : "--",
        "TunePerpHeightLimit": data['tuneTurnDistaceLimit'] != null
            ? double.parse("${data['tunePerpHeightLimit']}")
            : "--",
        'tunePerpHeightGain': data['tuneTurnDistaceLimit'] != null
            ? int.parse("${data['tunePerpHeightGain']}")
            : "--",
        'tuneBearingGain': data['tuneTurnDistaceLimit'] != null
            ? int.parse("${data['tuneBearingGain']}")
            : "--",
        'TuneFwdAngleLimit': data['tuneFwdAngleLimit'] != null
            ? int.parse("${data['tuneFwdAngleLimit']}")
            : "--",
        'MaxRudderDutyCycle': data['tuneMaxRudderDutyCycle'] != null
            ? int.parse("${data['tuneMaxRudderDutyCycle']}")
            : "--",
        'MinRudderDutyCycle': data['tuneMinRudderDutyCycle'] != null
            ? int.parse("${data['tuneMinRudderDutyCycle']}")
            : "--",
        'tuneAutoModeSpeed': data['tuneAutoModeSpeed'] != null
            ? int.parse("${data['tuneAutoModeSpeed']}")
            : "--",
        'tuneFeedStep': data['tuneFeedStep'] != null
            ? int.parse("${data['tuneFeedStep']}")
            : "--",
        'shouldSpin': data['shouldSpin'] != null
            ? int.parse("${data['shouldSpin']}")
            : "--",
        'spinnerMotorDutyCycle': data['spinnerMotorDutyCycle'] != null
            ? int.parse("${data['spinnerMotorDutyCycle']}")
            : "--",
        'dataSendLoopWD': data['dataSendLoopWD'] != null
            ? int.parse("${data['dataSendLoopWD']}")
            : "--",
        'k_debug':
            data['k_debug'] != null ? int.parse("${data['k_debug']}") : "--",
        'hasADCI2C': data['hasADCI2C'] != null
            ? int.parse("${data['hasADCI2C']}")
            : "--",
        'logLavel':
            data['logLavel'] != null ? int.parse("${data['logLavel']}") : "--",
        'PerpHtKp':
            data['PerpHtKp'] != null ? int.parse("${data['PerpHtKp']}") : "--",
        'PerpHtKd':
            data['PerpHtKd'] != null ? int.parse("${data['PerpHtKd']}") : "--",
        'PerpHtKi':
            data['PerpHtKi'] != null ? int.parse("${data['PerpHtKi']}") : "--",
        'AngleKp':
            data['AngleKp'] != null ? int.parse("${data['AngleKp']}") : "--",
        'AngleKd':
            data['AngleKd'] != null ? int.parse("${data['AngleKd']}") : "--",
        'AngleKi':
            data['AngleKi'] != null ? int.parse("${data['AngleKi']}") : "--",
        'MaxTripTime': data['MaxTripTime'] != null
            ? int.parse("${data['MaxTripTime']}")
            : "--"
      };
      if (data['tuneDriveGain'] == "--") {
        configCmd.remove('tuneDriveGain');
      }
      if (data['tuneNearPostLimit'] == "--") {
        configCmd.remove('tuneNearPostLimit');
      }

      if (data['tuneBotHeadinCorrection'] == "--") {
        configCmd.remove('tuneBotHeadinCorrection');
      }
      if (data['tuneTurnDistaceLimit'] == "--") {
        configCmd.remove('tuneTurnDistaceLimit');
      }
      if (data['tunePerpHeightLimit'] == "--") {
        configCmd.remove('TunePerpHeightLimit');
      }
      if (data['tunePerpHeightGain'] == "--") {
        configCmd.remove('tunePerpHeightGain');
      }
      if (data['tuneBearingGain'] == "--") {
        configCmd.remove('tuneBearingGain');
      }
      if (data['tuneFwdAngleLimit'] == "--") {
        configCmd.remove('TuneFwdAngleLimit');
      }
      if (data['tuneMaxRudderDutyCycle'] == "--") {
        configCmd.remove('MaxRudderDutyCycle');
      }
      if (data['tuneMinRudderDutyCycle'] == "--") {
        configCmd.remove('MinRudderDutyCycle');
      }
      if (data['tuneAutoModeSpeed'] == "--") {
        configCmd.remove('tuneAutoModeSpeed');
      }
      if (data['tuneFeedStep'] == "--") {
        configCmd.remove('tuneFeedStep');
      }
      if (data['shouldSpin'] == "--") {
        configCmd.remove('shouldSpin');
      }
      if (data['spinnerMotorDutyCycle'] == "--") {
        configCmd.remove('spinnerMotorDutyCycle');
      }
      // if (data['handBrake'] == "--") {
      //   configCmd.remove('handBrake');
      // }
      if (data['dataSendLoopWD'] == "--") {
        configCmd.remove('dataSendLoopWD');
      }
      if (data['k_debug'] == "--") {
        configCmd.remove('k_debug');
      }
      if (data['hasADCI2C'] == "--") {
        configCmd.remove('hasADCI2C');
      }
      if (data['PerpHtKp'] == "--") {
        configCmd.remove('PerpHtKp');
      }
      if (data['PerpHtKd'] == "--") {
        configCmd.remove('PerpHtKd');
      }
      if (data['PerpHtKi'] == "--") {
        configCmd.remove('PerpHtKi');
      }
      if (data['AngleKp'] == "--") {
        configCmd.remove('AngleKp');
      }
      if (data['AngleKd'] == "--") {
        configCmd.remove('AngleKd');
      }
      if (data['logLavel'] == "--") {
        configCmd.remove('logLavel');
      }
      if (data['AngleKi'] == "--") {
        configCmd.remove('AngleKi');
      }
    } else if (methodName == 'tuneMinRudderDutyCycle') {
      var configCmd = {
        "tune": data['tuneMinRudderDutyCycle'],
      };
    } else if (methodName == 'tunePerpHeightLimit') {
      var configCmd = {
        "tune": data['tunePerpHeightLimit'],
      };
    } else if (methodName == 'tunePerpHeightGain') {
      var configCmd = {
        "tune": data['tunePerpHeightGain'],
      };
    } else if (methodName == 'tuneBearingGain') {
      var configCmd = {
        "tune": data['tuneBearingGain'],
      };
    } else if (methodName == 'tuneFwdAngleLimit') {
      var configCmd = {
        "tune": data['tuneFwdAngleLimit'],
      };
    } else {
      var configCmd = {
        "tune": data['tuneAutoModeSpeed'],
      };
    }
    var finalObj = {'frametype': "$methodName", 'commands': configCmd};
    // showOverlay(finalObj);
    sendCommandDirectlyToDevice(configCmd, methodName);
  }

  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();
    setState(() {
      sendingCommand = false;
    });
    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  sendCommandDirectlyToDevice(command, methodName) async {
    try {
      //print(methodName);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${data['deviceId']}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      setState(() {
        enteredApi = path;
      });
      var encodedCommand = jsonEncode(command);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        //print(convertedData);
        setState(() {
          enteredData = convertedData;
          sendingCommand = true;
        });
        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return Alert(
        //           "Command sent successfully", Colors.green, "Tune Settings");
        //     });
        getResponseBasedOnDevice("", 200);
        /* setState(() {
          displayToast = true;
          toastColor = Colors.green;
          toastName = "Success";
        });*/
        // Fluttertoast.showToast(msg: "Success", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
        if (methodName == 'tuneMaxRudderDutyCycle') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'respMsz': 'Success',
              'mszColor': 'green'
            };
          });
        } else if (methodName == 'tuneMinRudderDutyCycle') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMszMinRudderDutyCycle': true,
            };
          });
        } else if (methodName == 'tunePerpHeightLimit') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMsztunePerpHeightLimit': true,
            };
          });
        } else if (methodName == 'tunePerpHeightGain') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMsztunePerpHeightGain': true,
            };
          });
        } else if (methodName == 'tuneBearingGain') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMsztuneBearingGain': true,
            };
          });
        } else if (methodName == 'tuneFwdAngleLimit') {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMsztuneFwdAngleLimit': true,
            };
          });
        } else {
          setState(() {
            data = {
              ...data,
              'isLoading': false,
              'showMsztuneAutoModeSpeed': true,
            };
          });
        }
      }
    } catch (error) {
      //print(error);
      /*  setState(() {
        displayToast = true;
        toastColor = Colors.red;
        toastName = "Failed";
      });*/
      // showDialog(
      //     context: context,
      //     builder: (BuildContext context) {
      //       return Alert("Sending command failed", Colors.red, "Tune Settings");
      //     });
      setState(() {
        sendingCommand = true;
      });
      getResponseBasedOnDevice("", 500);
      // Fluttertoast.showToast(msg: "Failed", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 4, backgroundColor: Colors.redAccent, textColor: Colors.white, fontSize: 16.0);
      if (methodName == 'tuneMaxRudderDutyCycle') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'respMsz': 'Failed',
            'showMsz': true,
            'mszColor': 'red'
          };
        });
      } else if (methodName == 'tuneMinRudderDutyCycle') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMszMinRudderDutyCycle': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      } else if (methodName == 'tunePerpHeightLimit') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMsztunePerpHeightLimit': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      } else if (methodName == 'tunePerpHeightGain') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMsztunePerpHeightGain': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      } else if (methodName == 'tuneBearingGain') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMsztuneBearingGain': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      } else if (methodName == 'tuneFwdAngleLimit') {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMsztuneFwdAngleLimit': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      } else {
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'showMsztuneAutoModeSpeed': true,
            'respMsz': 'Failed',
            'mszColor': 'red'
          };
        });
      }
    }
  }

  onChangePerpHtKp(text) {
    setState(() {
      data = {...data, 'PerpHtKp': text};
    });
  }

  renderText(name) {
    var text = Text(name,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            fontFamily: 'BrandonReg',
            color: Color(0xFF05375a)));
    return text;
  }

  renderTextField(_placeHolder, _labelText) {
    var textField = TextFormField(
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFf2f2f2))),
        hintText: _placeHolder,
        //labelText: _labelText,
      ),
      initialValue: _labelText,
    );
    return textField;
  }

  @override
  Widget build(BuildContext context) {
    ////print(data);
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    var tfValue = data['tuneMaxRudderDutyCycle'];
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        // margin: EdgeInsets.only(left: 20, right: 20),
        alignment: Alignment.topCenter,

        height: data['deviceType'] != null &&
                data['deviceType'] != "Line" &&
                data['isLoading'] == false
            ? MediaQuery.of(context).size.height * 0.90
            : MediaQuery.of(context).size.height * 0.50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: Color(0xFFE8EAF6),
        ),

        padding: EdgeInsets.only(left: 20, right: 20),
        child: ListView(children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("${deviceName}-(${deviceId})",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            fontFamily: 'BrandonReg')),
                  ],
                ),
                Divider(
                  thickness: 1,
                  color: Colors.black,
                ),
                sendingCommand == true
                    ? Container(
                        height: 350,
                        child: Center(
                          child: SpinKitThreeBounce(
                            size: 30.0,
                            color: Color(0xFF0170d0),
                          ),
                        ))
                    : Column(
                        children: [
                          data['deviceType'] != null &&
                                  data['deviceType'] != "Line" &&
                                  data['isLoading'] == false
                              ? Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      renderText("Turn Max Rudder DutyCycle"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),

                                          //labelText: "$data['waitTime']",
                                        ),
                                        initialValue: "$tfValue",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          tuneMaxRudderDutyCycleInputChange(
                                              text);
                                        },
                                      ),
                                      renderText("Turn Min Rudder DutyCycle"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tuneMinRudderDutyCycle']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          tuneMinRudderDutyCycleInputChange(
                                              text);
                                        },
                                      ),
                                      renderText("Perp Height Limit"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tunePerpHeightLimit']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          tunePerpHeightLimitInputChange(text);
                                        },
                                      ),
                                      renderText("Fwd Angle Limit"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tuneFwdAngleLimit']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          tuneFwdAngleLimitInputChange(text);
                                        },
                                      ),
                                      renderText("Auto Mode Speed"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tuneAutoModeSpeed']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          tuneAutoModeSpeedInputChange(text);
                                        },
                                      ),
                                      renderText("PerpHtKp"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue: "${data['PerpHtKp']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          onChangePerpHtKp(text);
                                        },
                                      ),
                                      renderText("AngleKp"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue: "${data['AngleKp']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          setState(() {
                                            data = {...data, 'AngleKp': text};
                                          });
                                        },
                                      ),
                                      renderText("Tune Drive Gain"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tuneDriveGain']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          setState(() {
                                            data = {
                                              ...data,
                                              'tuneDriveGain': text
                                            };
                                          });
                                        },
                                      ),
                                      renderText("TuneNearPostLimit"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue:
                                            "${data['tuneNearPostLimit']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          setState(() {
                                            data = {
                                              ...data,
                                              'tuneNearPostLimit': text
                                            };
                                          });
                                        },
                                      ),
                                      renderText("MaxTripTime"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue: "${data['MaxTripTime']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          setState(() {
                                            data = {
                                              ...data,
                                              'MaxTripTime': text
                                            };
                                          });
                                        },
                                      ),
                                      renderText("Log Level"),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color(0xFFf2f2f2))),
                                        ),
                                        initialValue: "${data['logLavel']}",
                                        keyboardType: TextInputType.number,
                                        onChanged: (text) {
                                          setState(() {
                                            data = {...data, 'logLavel': text};
                                          });
                                        },
                                      ),
                                    ],
                                  ))
                              : SizedBox(),
                          data['deviceType'] != null &&
                                  data['deviceType'] == "Line" &&
                                  data['isLoading'] == false
                              ? Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        renderText("Auto Mode Speed"),
                                        TextFormField(
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Color(0xFFf2f2f2))),
                                          ),
                                          initialValue:
                                              "${data['tuneAutoModeSpeed']}",
                                          keyboardType: TextInputType.number,
                                          onChanged: (text) {
                                            tuneAutoModeSpeedInputChange(text);
                                          },
                                        ),
                                        renderText("Tune Drive Gain"),
                                        TextFormField(
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Color(0xFFf2f2f2))),
                                          ),
                                          initialValue:
                                              "${data['tuneDriveGain']}",
                                          keyboardType: TextInputType.number,
                                          onChanged: (text) {
                                            setState(() {
                                              data = {'tuneDriveGain': text};
                                            });
                                          },
                                        ),
                                        renderText("TuneNearPostLimit"),
                                        TextFormField(
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Color(0xFFf2f2f2))),
                                          ),
                                          initialValue:
                                              "${data['tuneNearPostLimit']}",
                                          keyboardType: TextInputType.number,
                                          onChanged: (text) {
                                            setState(() {
                                              data = {
                                                'tuneNearPostLimit': text
                                              };
                                            });
                                          },
                                        ),
                                        renderText("MaxTripTime"),
                                        TextFormField(
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Color(0xFFf2f2f2))),
                                          ),
                                          initialValue:
                                              "${data['MaxTripTime']}",
                                          keyboardType: TextInputType.number,
                                          onChanged: (text) {
                                            setState(() {
                                              data = {'MaxTripTime': text};
                                            });
                                          },
                                        ),
                                      ]))
                              : SizedBox(),
                          data['deviceType'] == null &&
                                  data['isLoading'] == false
                              ? Center(
                                  child: Text(
                                      "Device is offline failed to fetch data"),
                                )
                              : SizedBox(),
                          data['deviceType'] != null &&
                                  data['isLoading'] == false
                              ? Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      //SizedBox(height: 10,),
                                      displayToast == true
                                          ? Container(
                                              width: 240,
                                              //height: 30,
                                              child: RenderToast(
                                                  toastName, toastColor, () {
                                                setState(() {
                                                  displayToast = false;
                                                });
                                              }),
                                            )
                                          : SizedBox(),
                                      Align(
                                          alignment: Alignment.centerRight,
                                          child: CommandButton(
                                              "Send", Colors.blueAccent, () {
                                            sendTuneDetalisCommand(
                                                'setTuneValues');
                                          })),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                        ],
                      ),
              ]),
        ]),
      ),
    );
  }
}
