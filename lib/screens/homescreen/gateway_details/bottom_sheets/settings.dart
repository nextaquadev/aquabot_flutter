import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Settings extends StatefulWidget {
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Settings({this.isSelected, this.cellDevice, this.index});
  // const Settings({ Key? key }) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  AquaUser user = AquaUser.getInstance();
  var deviceId;
  var isSwitched = false;
  var isLoading;
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  var data = {
    'deviceId': null,
    'deviceType': null,
    'version': 0,
    'waitTime': 0,
    'showMsz': false,
    'feedDirection': 1,
    'showSnackbar': false,
    'isLoading': false,
  };
  var enteredApi;
  var enteredData;
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    //fetchTripInfoData();
    //sendCommandDirectlyToDevice();
    //sendTuneDetalisCommand();
    getConfigurationDataFromDevice();
    super.initState();
  }

  getConfigurationDataFromDevice() async {
    var deviceid = deviceId;
    setState(() {
      data = {...data, 'deviceId': deviceid, 'isLoading': true};
    });
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var cmdObj = {'version': version, 'deviceId': deviceid};
    try {
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${deviceId}/rpc/getConfiguration?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      setState(() {
        enteredApi = path;
      });
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var encodedcmdObj = jsonEncode(cmdObj);
      var getData = await http.post(path,
          body: encodedcmdObj,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (getData.statusCode == 200) {
        var responseData = json.decode(getData.body);
        //print(responseData);

        setState(() {
          data = {
            ...data,
            "waitTime": responseData.keys.contains('waitTime')
                ? responseData['waitTime']
                : "--",
            "feedDirection": responseData.keys.contains('feedDirection')
                ? responseData.data.feedDirection
                : "--",
            "deviceType": responseData.keys.contains('deviceType')
                ? responseData['deviceType']
                : "Line",
            "deviceId": responseData.keys.contains('deviceId')
                ? responseData['deviceId']
                : data['deviceId'],
            "isLoading": false,
          };
        });
        //print(data);
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 4,
            backgroundColor: Colors.greenAccent,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        setState(() {
          data = {
            ...data,
            'showSnackbar': true,
            'isLoading': false,
          };
        });
      }
    } catch (error) {
      setState(() {
        data = {
          ...data,
          'showSnackbar': true,
          'isLoading': false,
        };
      });
      //print(error);
      Fluttertoast.showToast(
          msg: "Failed",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 4,
          backgroundColor: Colors.redAccent,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  setWaitTime(val) {
    setState(() {
      data = {...data, 'waitTime': val};
    });
  }

  setFeedDirection(val) {
    setState(() {
      data = {...data, 'feedDirection': val};
    });
  }

  toggelSwitch() {
    if (data['feedDirection'] == 1) {
      setFeedDirection(2);
    } else {
      setFeedDirection(1);
    }
  }

  sendTuneDetalisCommand(methodName) {
    setState(() {
      data = {...data, 'isLoading': true};
    });
    var deviceid = deviceId;
    //get current time in seconds
    var currentTime = DateTime.now();
    var version = currentTime.millisecondsSinceEpoch ~/ 1000;
    var configCmd;
    if (methodName == 'setConfiguration') {
      configCmd = {
        "version": version,
        "deviceId": deviceid,
        "deviceType": "Line",
        "waitTime": data['waitTime'] != null ? data['waitTime'] : "--",
        "feedDirection":
            data['feedDirection'] != null ? data['feedDirection'] : "--",
      };
    }

    setState(() {
      enteredData = configCmd;
    });
    var finalObj = {'frametype': "$methodName", 'commands': configCmd};
    // showOverlay(finalObj);
    sendCommandDirectlyToDevice(configCmd, methodName);
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  sendCommandDirectlyToDevice(command, methodName) async {
    try {
      //print(methodName);
      Map<String, String> header = {
        'Content-Type': 'application/json; charset=UTF-8',
        'Charset': 'utf-8',
      };
      var path = Uri.parse(
          'https://mdash.net/api/v2/devices/${data['deviceId']}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
      var encodedCommand = jsonEncode(command);
      var response = await http.post(path,
          body: encodedCommand,
          headers: header,
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 200) {
        var convertedData = json.decode(response.body);
        //print(convertedData);
        setState(() {
          data = {
            ...data,
            'isLoading': false,
            'respMsz': 'Success',
            'mszColor': 'green'
          };
        });
      }
    } catch (error) {
      //print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    var deviceName = user.selGateway.botDevices[widget.index].deviceName;
    var loading = data['isLoading'];
    var tfValue;
    if (data['isLoading'] == false) {
      tfValue = data['waitTime'];
    }
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(left: 20, right: 20),
      height: 400,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${deviceName}-(${deviceId})",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                        fontFamily: 'BrandonReg')),
              ],
            ),
            Divider(
              thickness: 1,
              color: Colors.black,
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5),
                  child: data['isLoading'] == false
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Wait Time",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: 'BrandonReg',
                                    color: Color(0xFF05375a))),
                            TextFormField(
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xFFf2f2f2))),
                                hintText: 'Wait Time',
                                //labelText: "$data['waitTime']",
                              ),
                              initialValue: "$tfValue",
                              keyboardType: TextInputType.number,
                              onChanged: (text) {
                                setWaitTime(text);
                              },
                            )
                          ],
                        )
                      : SizedBox(),
                ),
                data['isLoading'] == false
                    ? Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Feed Direction",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: 'BrandonReg',
                                    color: Color(0xFF05375a))),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          width: 1, color: Color(0xFFf2f2f2)))),
                              child: Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("One Way"),
                                  Switch(
                                    value: isSwitched,
                                    onChanged: (value) {
                                      setState(() {
                                        isSwitched = value;
                                      });
                                      toggelSwitch();
                                    },
                                    activeTrackColor: Colors.greenAccent,
                                    activeColor: Colors.greenAccent,
                                  ),
                                  Text("Two Way")
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    : SizedBox(),
                data['deviceType'] != null && data['isLoading'] == false
                    ? Container(
                        child: ElevatedButton(
                            onPressed: () {
                              sendTuneDetalisCommand('setConfiguration');
                            },
                            child: Text("Send"),
                            style: ElevatedButton.styleFrom(
                                primary: Colors.blueAccent,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)))),
                      )
                    : SizedBox()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
