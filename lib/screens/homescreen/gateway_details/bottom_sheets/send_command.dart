import 'dart:convert';
import 'package:http/http.dart' as http;

sendCommandDirectlyToDevice(command, methodName, deviceId) async {
     Map<String, String> header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Charset': 'utf-8',
    };
        var path = Uri.parse('https://mdash.net/api/v2/devices/${deviceId}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
        var encodedCommand=jsonEncode(command);
        var response = await http.post(path, body: encodedCommand, headers: header,encoding: Encoding.getByName("utf-8"));
        return response;
}