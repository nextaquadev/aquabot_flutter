import 'dart:convert';

import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/send_command.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/toast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

class Pause extends StatefulWidget {
  final BotModel cellDevice;
  final bool isSelected;
  final int index;
  Pause({this.isSelected, this.cellDevice, this.index});
  // const Pause({ Key? key }) : super(key: key);

  @override
  _PauseState createState() => _PauseState();
}

class _PauseState extends State<Pause> {
  AquaUser user = AquaUser.getInstance();
  var deviceId = "";
  var data = {'isLoading': false, 'key': ""};
  var displayToast = false;
  var toastColor = Colors.green;
  var enteredData;
  var enteredApi;
  var toastName = "";
  OverlayEntry entry;
  Offset offset = Offset(20, 40);
  var pretty = JsonEncoder.withIndent("   ");
  void initState() {
    setState(() {
      deviceId = user.selGateway.botDevices[widget.index].macId;
    });
    super.initState();
  }

  // sendCommandDirectlyToDevice(command, methodName, deviceId) async {
  //    Map<String, String> header = {
  //     'Content-Type': 'application/json; charset=UTF-8',
  //     'Charset': 'utf-8',
  //   };
  //       var path = Uri.parse('https://mdash.net/api/v2/devices/${deviceId}/rpc/${methodName}?access_token=JKrRbHr8MnhdUUso91Zmm91A');
  //       setState(() {
  //         enteredApi = path;
  //       });
  //       var encodedCommand=jsonEncode(command);
  //       var response = await http.post(path, body: encodedCommand, headers: header,encoding: Encoding.getByName("utf-8"));
  //       var convertedData = jsonDecode(response.body);
  //       setState(() {
  //           enteredData = convertedData;
  //         });
  //       return response;
  //   }
  Widget RenderConfigStatusBS(List successListaf) {
    // var convertToString = user.linemon_selGateway.linemonDevices.toString();
    // var decodelayout = jsonDecode(convertToString);
    var macId;
    var _layout = user.selGateway.botDevices.map((ele) {
      macId = ele.macId;
    }).toList();
    //print(macId);
    var bottomSheet = Container(
      height: 200,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Command status")),
              Row(
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Failed"),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                      margin: EdgeInsets.only(right: 30),
                      child: Text("Success")),
                ],
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Divider(thickness: 1, color: Colors.black)),
          Wrap(
              alignment: WrapAlignment.spaceBetween,
              //spacing: 10,
              children: successListaf
                  .map((index) => Container(
                        margin: EdgeInsets.only(right: 10, top: 15),
                        width: MediaQuery.of(context).size.width * 0.13,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: index['statusCode'] == 200
                                ? Colors.green
                                : Colors.red),
                        child: Center(
                            child: Text(
                          "${index['name']}",
                          style: TextStyle(color: Colors.white),
                        )),
                      ))
                  .toList()),
        ],
      ),
    );
    return bottomSheet;
  }

  getResponseBasedOnDevice(scheduleObj, statusCode) async {
    var successListaf = [];
    // var firebaseData = await FirebaseFirestore.instance
    //     .collection('wifiDeviceRes')
    //     .doc("${scheduleObj['version']}")
    //     .get()
    //     .then((res) {
    //   if (res.exists) {
    //     var data = res.data();
    //     //print(data);
    //     //print(data['response']);
    //     var _layout = [];
    //     user.selGateway.botDevices.map((ele) {
    //       var obj = {};
    //       if (data != null) {
    //         if (data['response'][ele.macId] != null) {
    //           // obj['name'] = ele['faults'][0]['indication'];
    //           obj['id'] = ele.macId;
    //           obj['statusCode'] =
    //               data['response'][ele.macId]['statusCode'] != null
    //                   ? data['response'][ele.macId]['statusCode']
    //                   : 500;
    //           successListaf.add(obj);
    //           //print(successListaf);
    //         }
    //       }
    //     }).toList();
    //   }
    // });
    user.selGateway.botDevices.map((ele) {
      if (ele.macId == deviceId) {
        successListaf.add({'name': ele.deviceName, 'statusCode': statusCode});
      }
    }).toList();

    Navigator.pop(context);
    showModalBottomSheet(
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        backgroundColor: Color(0xFFE8EAF6),
        isScrollControlled: true,
        context: context,
        builder: (context) => RenderConfigStatusBS(successListaf));
  }

  onPressOfOk(message, color) {
    Navigator.pop(context);
    var alert = showDialog(
        context: context,
        builder: (BuildContext context) {
          return Alert(message, color, "Pause Settings");
        });
    return alert;
  }

  pauseDevice(MethodName) async {
    var command = {};
    var finalObj = {'frametype': "pauseTrip", 'commands': command};
    // showOverlay(finalObj);
    try {
      var responseData =
          await sendCommandDirectlyToDevice(command, MethodName, deviceId);

      //  //print(responseData.body);
      // Fluttertoast.showToast(msg: "Success", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 5, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.green;
      //   toastName = "Success";
      // });
      // onPressOfOk("Command sent successfully", Colors.green);
      if (responseData.statusCode == 200) {
        getResponseBasedOnDevice("", 200);
      } else {
        getResponseBasedOnDevice("", 500);
      }
      //      ScaffoldMessenger.of(context)
      // .showSnackBar(SnackBar(content: Text("Success")));
    } catch (error) {
      //print(error);
      if (this.mounted) {
        setState(() {
          data = {...data, 'isLoading': false};
        });
      }
      getResponseBasedOnDevice("", 500);
      // onPressOfOk("Sending command failed", Colors.red);
      // getResponseBasedOnDevice("" , 200);
      // Fluttertoast.showToast(msg: "Failed", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.BOTTOM, timeInSecForIosWeb: 5, backgroundColor: Colors.redAccent, textColor: Colors.white, fontSize: 16.0);
      // setState(() {
      //   displayToast = true;
      //   toastColor = Colors.red;
      //   toastName = "Failed";
      // });
      //       ScaffoldMessenger.of(context)
      // .showSnackBar(SnackBar(content: Text("Failed")));
    }
  }

  onCancel() {
    // Fluttertoast.showToast(msg: "Failed", toastLength: Toast.LENGTH_SHORT, gravity:  ToastGravity.CENTER, timeInSecForIosWeb: 3, backgroundColor: Colors.green, textColor: Colors.white, fontSize: 16.0);
    Navigator.pop(context, false);
  }

  showOverlay(data) {
    hideOverlay();
    entry = OverlayEntry(
        builder: (context) => Stack(children: <Widget>[
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  hideOverlay();
                },
                child: Container(color: Colors.transparent),
              )),
              Positioned(
                left: offset.dx,
                top: offset.dy,
                child: GestureDetector(
                  onPanUpdate: (details) {
                    offset += details.delta;
                    entry.markNeedsBuild();
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: DeviceIndicationColors.PondFillColor,
                      ),
                      height: 230,
                      width: 250,
                      child: Material(
                        color: Colors.transparent,
                        child: SingleChildScrollView(
                          child: Text(
                            "${pretty.convert(data)}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      )),
                ),
              )
            ]));

    var overlay = Overlay.of(context);
    overlay.insert(entry);
  }

  hideOverlay() {
    if (entry != null) {
      entry.remove();
      entry = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: AlertDialog(
        title: Text("Are you sure, you want to pause ?",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                fontFamily: 'BrandonReg')),
        content: SizedBox(
          width: 100,
          child: Container(
            height: displayToast == true ? 65 : 35,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        child: Text("Cancel",
                            style: TextStyle(fontFamily: 'BrandonReg')),
                        onTap: () {
                          onCancel();
                        }),
                    GestureDetector(
                      child: Text("Ok",
                          style: TextStyle(fontFamily: 'BrandonReg')),
                      onTap: () {
                        pauseDevice("pauseTrip");
                      },
                    ),
                  ],
                ),
                /*   SizedBox(
                  height: 10,
                ),
                displayToast == true
                    ? Container(
                        height: 30,
                        child: RenderToast(toastName, toastColor, () {
                          setState(() {
                            displayToast = false;
                          });
                        }),
                      )
                    : SizedBox()*/
              ],
            ),
          ),
        ),
      )),
    );
  }
}
