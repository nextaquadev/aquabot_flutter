import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/screens/homescreen/gateway_details/detailscell/gateway_details_cell.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/live_polyline.dart';
import 'package:aquabot/screens/homescreen/gateway_details/map_screen/map_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'bottom_sheets/LiveFeed.dart';

class GatewayDetails extends StatefulWidget {
  static String pageRoute = "/GatewayDetails";

  const GatewayDetails({Key key}) : super(key: key);

  @override
  _GatewayDetailsState createState() => _GatewayDetailsState();
}

class _GatewayDetailsState extends State<GatewayDetails> {
  AquaUser user = AquaUser.getInstance();
  Live_poly live_poly = Live_poly();
  BotModel selDevice = null;
  bool islivemap = true;
  LatLng initialcameraposition;
  double zoom = 19.0;
  double lat;
  bool clear = false;
  bool isExpand = false;
  double long;

  var gpsloc;
  var latitud;
  var logitud;

  // Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};

  @override
  void initState() {
    super.initState();
    gpsloc = user.selGateway.gpsLocation;
    latitud = gpsloc.split(",").first;
    logitud = gpsloc.split(",").last;
    user.selGateway.botDevices != null && user.selGateway.botDevices.length > 0
        ? user.selGateway.botDevices.forEach((index) {
            if (index.liveModel != null &&
                index.liveModel.liveLatLongs != null) {
              index.liveModel.liveLatLongs.clear();
            }
          })
        : "";

    selDevice = user.selGateway.botDevices.length > 0
        ? user.selGateway.botDevices[0]
        : null;
    lat = user.selGateway.botDevices.length > 0 &&
            user.selGateway.botDevices[0].lat != null &&
            user.selGateway.botDevices[0].lat != 0
        ? user.selGateway.botDevices[0].lat
        : double.parse("$latitud");
    // : 17.448227813051307;
    long = user.selGateway.botDevices.length > 0 &&
            user.selGateway.botDevices[0].long != null &&
            user.selGateway.botDevices[0].long != 0
        ? user.selGateway.botDevices[0].long
        : double.parse("$logitud");
    //   : 78.36215291172266;

    initialcameraposition = LatLng(lat, long);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //    appBar: getAppBar(context),
        body: SafeArea(
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Column(children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          child: MapView(
                            selDevice: selDevice,
                            initialcameraposition: initialcameraposition,
                            zoom: zoom,
                            clear: clear,
                            //polylines: polylines,
                          ),
                        ),
                        Positioned(
                          bottom: 10,
                          right: 10,
                          child: GestureDetector(
                              onTap: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          super.widget),
                                );
                              },
                              child: Icon(
                                Icons.refresh,
                                color: Colors.black,
                                size: 30,
                              )

                              /*Text(
                                "Refresh",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontFamily: 'BrandonReg',
                                    fontWeight: FontWeight.bold),
                              )*/
                              ),
                        ),
                        Positioned(
                          top: 10,
                          right: 50,
                          child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  clear = true;
                                  selDevice.liveModel != null
                                      ? selDevice.liveModel.liveLatLongs.clear()
                                      : "";
                                  /* selDevice.polylines.removeWhere((key,
                                          value) =>
                                      key.value.startsWith(RegExp('[0-9]')));*/
                                });

                                /* setState(() {
                                  selDevice.livepoints.clear();
                                });*/

                                /*  Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          super.widget),
                                );*/
                              },
                              child: Text(
                                "CLEAR",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontFamily: 'BrandonReg',
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
              //           Positioned(
              //              top: 6,
              //             right: 15,
              //             child: PopupMenuButton(
              //       child: Center(child: Icon(Icons.more_vert, color: Colors.white)),
              //       itemBuilder: (context) {
              //         return List.generate(1, (index) {
              //           return PopupMenuItem(
              //             child: Column(
              //               children: [
              //                 Container(
              //                     alignment: Alignment.center,
              //                     child: Column(children: [
              //                       GestureDetector(
              //                         onTap: (){Navigator.pushReplacement(
              // context, MaterialPageRoute(builder: (context) => LiveFeed()));},
              //                         child: Text(
              //                           "Feed Details",
              //                           style:
              //                               TextStyle(fontFamily: 'BrandonReg', fontSize: 20),
              //                         ),
              //                       ),
                                
              //                     ])),
                              
                              
              //               ],
              //             ),
              //             /* child: GestureDetector(
              //                 onTap: () {
              //                   logoutClicked();
              //                 },
              //                 child: Text('Logout')),*/
              //           );
              //         });
              //       },
              //     ),),
              //           Positioned(
              //             top: 10,
              //             left: 160,
              //             child: GestureDetector(
              //                 onTap: () {
              //                   // setState(() {
              //                   //   clear = true;
              //                   //   selDevice.liveModel != null
              //                   //       ? selDevice.liveModel.liveLatLongs.clear()
              //                   //       : "";
              //                   //   /* selDevice.polylines.removeWhere((key,
              //                   //           value) =>
              //                   //       key.value.startsWith(RegExp('[0-9]')));*/
              //                   // });
              //                 Navigator.pushReplacement(
              // context, MaterialPageRoute(builder: (context) => LiveFeed()));
              //                   /* setState(() {
              //                     selDevice.livepoints.clear();
              //                   });*/

              //                   /*  Navigator.pushReplacement(
              //                     context,
              //                     MaterialPageRoute(
              //                         builder: (BuildContext context) =>
              //                             super.widget),
              //                   );*/
              //                 },
              //                 child: Text(
              //                   "Feed",
              //                   style: TextStyle(
              //                       color: Colors.white,
              //                       fontSize: 13,
              //                       fontFamily: 'BrandonReg',
              //                       fontWeight: FontWeight.bold),
              //                 )),
              //           ),
                        Positioned(
                          top: 10,
                          left: 50,
                          child: GestureDetector(
                              onTap: () {},
                              child: Text(
                                "${user.selGateway.farmName}",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontFamily: 'BrandonReg',
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        Positioned(
                            top: 10,
                            left: 10,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white,
                                size: 26.0,
                              ),
                            ))
                      ],
                    ),
                  ),
                  StreamBuilder(
                      stream: homeBloc.botDetailsRefreshStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          bool status = snapshot.data;
                          return getListView();
                        }
                        return getListView();

                        // return getListView();
                      }),
                ]))));
  }

  Widget getAppBar(BuildContext context) {
    String farmName = user.selGateway.farmName;
    var appbar = AppBar(
      titleSpacing: 0.0,
      elevation: 0,
      leadingWidth: 50,
      leading: SizedBox(
        width: 44,
        child: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 26.0,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      title: Row(
        children: <Widget>[
          Text(
            farmName,
            style: TextStyle(
                fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
    return appbar;
  }

  deviceClicked(BotModel deviceSel) {
    setState(() {
      selDevice = deviceSel;
    });
  }

  updateLocation() {
    if (selDevice != null) {
      double lati = selDevice.lat;
      double longi = selDevice.long;
      if (lati == null || longi == null || lati == 0 || longi == 0) {
        var gpsloc = user.selGateway.gpsLocation;
        var latitud = gpsloc.split(",").first;
        var logitud = gpsloc.split(",").last;
        setState(() {
          initialcameraposition =
              LatLng(double.parse("$latitud"), double.parse("$logitud"));
          zoom = 19;
        });
      } else {
        setState(() {
          initialcameraposition = LatLng(lati, longi);
          zoom = 19;
        });
      }
    }
  }

  updateexpand(device) {
    setState(() {
      device.isExpand = selDevice == device ? !device.isExpand : false;
    });
  }

  Widget getListView() {
    AquaUser user = AquaUser.getInstance();
    // List<dynamic> allGateways = user.gatewaysMap.keys;
    user.selGateway.botDevices
      ..sort((productA, productB) =>
          productA.deviceName.compareTo(productB.deviceName));

    var listview = ListView.builder(
      // scrollDirection: Axis.horizontal,
      itemCount: user.selGateway.botDevices.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              deviceClicked(user.selGateway.botDevices[index]);
              updateLocation();
              updateexpand(user.selGateway.botDevices[index]);
            },
            child: SizedBox(
                height: selDevice == user.selGateway.botDevices[index] &&
                        user.selGateway.botDevices[index].isExpand
                    ? 180
                    : 90,
                child: Container(
                    padding: EdgeInsets.fromLTRB(8, 0, 8, 10), //(bottom: 10),
                    child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  8.0) //                 <--- border radius here
                              ),
                        ),
                        child: GatewayDetailsCell(
                          isSelected:
                              (selDevice == user.selGateway.botDevices[index]),
                          cellDevice: user.selGateway.botDevices[index],
                          index: index,
                          isExpand: user.selGateway.botDevices[index].isExpand,
                        )))));
      },
    );

    return Expanded(child: listview);
  }
}
