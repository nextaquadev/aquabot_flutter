import 'dart:async';
import 'dart:io';

import 'package:aquabot/WebServices/blocs/login_bloc.dart';
import 'package:aquabot/WebServices/http_server/firebase_utils.dart';
import 'package:aquabot/WebServices/models/phverify_model.dart';
import 'package:aquabot/screens/homescreen/aquabothome.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:timer_button/timer_button.dart';
import 'package:url_launcher/url_launcher.dart';

class NextAquaLogin extends StatefulWidget {
  static String pageRoute = "/";
  @override
  _NextAquaLoginState createState() => _NextAquaLoginState();
}

class _NextAquaLoginState extends State<NextAquaLogin> {
  var txt = TextEditingController();
  var otpTxtField = TextEditingController();
  PhVerifyModel phModel = null;
  var _appVersion = 1.4;
  var isLatestVersion = false;
  var isLoading = true;
  FirebaseAuth _auth = FirebaseAuth.instance;

  String verificationId;
  @override
  void initState() {
    super.initState();
    checkAppVersion();
  }

  checkAppVersion() async {
    var firebaseAppVersion;
    try {
      var firebaseData = await FirebaseFirestore.instance
          .collection('server')
          .doc('AquaBot')
          .get()
          .then((res) {
        //print(res.data());
        var firebaseResponse = res.data();
        firebaseAppVersion = firebaseResponse['appversion'];
        setState(() {
          isLoading = false;
        });
      });
    } catch (error) {
      //print(error);
      setState(() {
        isLoading = false;
      });
    }
    //print(firebaseAppVersion);
    if (_appVersion >= firebaseAppVersion) {
      setState(() {
        isLatestVersion = true;
      });
    } else {
      setState(() {
        isLatestVersion = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          child: isLoading == true
              ? Center(
                  child: SpinKitThreeBounce(
                    size: 30.0,
                    color: Color(0xFF0170d0),
                  ),
                )
              : isLatestVersion == true
                  ? Container(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Center(child: getLogInWidget()),
                      ),
                    )
                  : Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Deprecated Version!"),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Please update App to "),
                              InkWell(
                                child: Text("latest version",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: 'Brandon',
                                        decoration: TextDecoration.underline,
                                        color: Colors.blue)),
                                onTap: () {
                                  redirectToAppstore();
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
        ),
      ),
    );
  }

  redirectToAppstore() async {
    var url;
    if (Platform.isAndroid) {
      url =
          'https://play.google.com/store/apps/details?id=com.nextaqua.aquabot';
    } else {
      url = 'https://apps.apple.com/tc/app/aquabot-1-0/id1563036495';
    }
    if (!await launch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch';
    }
  }

  Widget getLogInWidget() {
    var loginWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('images/app_icon-trns.png', height: 100, fit: BoxFit.fill),
        SizedBox(width: 10),

        /* Text('AquaBot',
            style: TextStyle(
                color: Colors.black, fontFamily: 'BrandonReg', fontSize: 16)),*/
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          //  SizedBox(width: 10),
          Text(
            "NEXT",
            style: TextStyle(
                color: Color.fromRGBO(45, 45, 45, 1),
                fontSize: 25,
                fontFamily: 'BrandonReg',
                fontWeight: FontWeight.bold),
          ),
          Text("AQUA",
              style: TextStyle(
                  color: Color(0xFF0170d0),
                  fontSize: 25,
                  fontFamily: 'BrandonReg',
                  fontWeight: FontWeight.bold)),
        ]),
        SizedBox(
          height: 20,
        ),
        TextField(
            controller: txt,
            decoration: new InputDecoration(
              hintText: "Enter Mobile number",
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(36, 182, 174, 1)),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(36, 182, 174, 1)),
              ),
            )),
        SizedBox(
          height: 20,
        ),
        StreamBuilder(
            stream: loginBlock.phVerifyStream,
            builder: (context, AsyncSnapshot<PhVerifyModel> snapshot) {
              if (snapshot.hasData) {
                phModel = snapshot.data;

                if (phModel.authException == null) {
                  var otpField = Column(
                    children: [
                      TextField(
                          controller: otpTxtField,
                          decoration: new InputDecoration(
                            hintText: "Enter OTP",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(36, 182, 174, 1)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(36, 182, 174, 1)),
                            ),
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      getVerifyBtn('Verify OTP'),
                      phModel.authException == null
                          ? Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TimerButton(
                                    label: "Resend OTP",
                                    timeOutInSeconds: 30,
                                    onPressed: () {
                                      verifyPhoneNumber('Verify phone number');
                                      otpalert();
                                    },
                                    disabledColor: Colors.grey,
                                    color: Color(0xFF0170d0),
                                    disabledTextStyle:
                                        TextStyle(color: Colors.white),
                                    activeTextStyle:
                                        TextStyle(color: Colors.white),
                                  ))) /* GestureDetector(
                                      onTap: () {
                                        verifyPhoneNumber(
                                            'Verify phone number');
                                        otpalert();
                                      },
                                      child: Text("Resend OTP",
                                          style:
                                              TextStyle(color: Colors.blue)))))
                        */
                          : SizedBox()
                    ],
                  );
                  return otpField;
                } else {
                  message(phModel.authException.code);
                  return getVerifyBtn('Verify phone number');
                }
              } else if (snapshot.hasError) {
                return getVerifyBtn('Verify phone number');
              }
              return getVerifyBtn('Verify phone number');
              //Center(child: CircularProgressIndicator());
            }),
        StreamBuilder(
            stream: loginBlock.userCredStream,
            builder: (context, AsyncSnapshot<UserCredential> snapshot) {
              if (snapshot.hasData) {
                UserCredential authCredential = snapshot.data;
                checkUserCredentials(authCredential);
              } else if (snapshot.hasError) {
                return Text(snapshot.error);
              }
              return SizedBox();
              //Center(child: CircularProgressIndicator());
            })
      ],
    );
    return loginWidget;
  }

  checkUserCredentials(UserCredential authCredential) {
    try {
      if (authCredential.user != null) {
        Timer(Duration(microseconds: 300), () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => AquaBotHome()));
        });
      }
    } on FirebaseAuthException catch (e) {}
  }

  Widget getVerifyBtn(String title) {
    var btn = RaisedButton(
      onPressed: () {
        (!kIsWeb) ? verifyPhoneNumber(title) : signInWithPhoneNumber(title);

        // verifyPhoneNumber(title);
      },
      color: Color(0xFF0170d0),
      // Color.fromRGBO(36, 182, 174, 1),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(title,
              style: TextStyle(
                  color: Colors.white, fontFamily: 'BrandonReg', fontSize: 16)),
        ],
      ),
    );
    return btn;
  }

  verifyPhoneNumber(String title) {
    FireBaseUtils fUtils = FireBaseUtils();

    if (title == 'Verify phone number') {
      fUtils.verifyNumber(txt.text);
    } else if (title == 'Verify OTP' && phModel != null) {
      fUtils.verifyOtp(otpTxtField.text, phModel, context);
    }
  }

  /* void signInWithPhoneAuthCredential(AuthCredential phoneAuthCredential) async {
    setState(() {
     // showLoading = true;
    });

    try {
      final authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);

      setState(() {
      //  showLoading = false;
      });

      if (authCredential.user != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomeScreen()));
      }
    } on FirebaseAuthException catch (e) {
      setState(() {
       // showLoading = false;
      });

    /*  _scaffoldKey.currentState!
          .showSnackBar(SnackBar(content: Text(e.message!)));*/
    }
  }*/
  signInWithPhoneNumber(String title) {
    FireBaseUtils fUtils = FireBaseUtils();

    if (title == 'Verify phone number') {
      fUtils.signInWithPhoneNumber(txt.text);
    } else if (title == 'Verify OTP' && phModel != null) {
      fUtils.verifyOtp(otpTxtField.text, phModel, context);
    }
  }

  otpalert() {
    return WidgetsBinding.instance.addPostFrameCallback((_) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return Alert("OTP Sent SucessFully", Colors.green, "Resend");
          });
    });
  }

  message(error) {
    return WidgetsBinding.instance.addPostFrameCallback((_) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return Alert("$error", Colors.red, "Login Error");
          });
    });
  }
}
