import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Route_Controller extends GetxController {
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{}.obs;
  Set<Polyline> polylines = <Polyline>{}.obs;
  bool sent = false;

  var allpoints = {}.obs;
  var labelpoint = {}.obs;
  addpoints(key, value) {
    allpoints[key] = value;
  }

  addlable(key, value) {
    labelpoint[key] = value;
  }

  addmarkers(key, value) {
    markers[key] = value;
  }

  addpolyline(value) {
    polylines.clear();
    polylines.add(value);
  }

  removemarkers(name) {
    markers.removeWhere((key, value) => key.value == name);
    print(markers);
  }

  removepoint(name) {
    allpoints.removeWhere((key, value) => key == name);
    // markers.removeWhere((key, value) => key.value == name);

    //  markers.clear();
    //  polylines.clear();
    var newpoints = {};
    allpoints.forEach((key, value) {
      newpoints[key] = value;
    });
    // allpoints.clear();
    print(allpoints);

    int i = 0;
    allpoints.clear();
    print(allpoints);

    newpoints.forEach((key, value) {
      allpoints["$i"] = value;
      i++;
    });
    allpoints["49"] = {'m_lat': "0", 'm_lng': "0"};

    print(allpoints);
  }

  updatealerttrue() {
    sent = true;
    print(sent);
    update();
  }

  updatealertfalse() {
    sent = false;
    print(sent);
    update();
  }

  /* removepolyline(name) {
    polylines.removeWhere((key, value) => key.value == name);
  }*/
  clearonly() {
    markers.clear();
    polylines.clear();
  }

  clearAll() {
    markers.clear();
    polylines.clear();
    allpoints.clear();
    labelpoint.clear();
  }
}
