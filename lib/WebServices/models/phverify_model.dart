import 'package:firebase_auth/firebase_auth.dart';

class PhVerifyModel {
  String verificationId;
  FirebaseAuthException authException;
  PhVerifyModel({this.verificationId, this.authException});
}
