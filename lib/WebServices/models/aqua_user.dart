import 'dart:collection';

import 'package:aquabot/WebServices/blocs/homedata_bloc.dart';
import 'package:aquabot/WebServices/models/gateway.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

class AquaUser {
  String address;
  String userName;
  String idStr;
  String userRole;
  List gateways;
  Map userData;
  String phoneNumber;
  var todayFeedTotal = 0.0;
  var totalFeedTotal = 0.0;
  var feedData;
  SplayTreeMap<String, Gateway> gatewaysMap = null;
  //Map<String, Gateway> gatewaysMap = null;

  Gateway selGateway = null;

  AquaUser._internal();
  static AquaUser _instance;

  static AquaUser getInstance() {
    if (_instance == null) {
      _instance = AquaUser._internal();
    }
    return _instance;
  }

  updateDetails(Map userDetails) {
    userData = userDetails;
    address =
        userDetails.keys.contains('address') ? userDetails['address'] : '';
    userName =
        userDetails.keys.contains('userName') ? userDetails['userName'] : '';
    idStr = userDetails.keys.contains('id') ? userDetails['id'] : '';
    userRole =
        userDetails.keys.contains('userRole') ? userDetails['userRole'] : '';
    gateways =
        userDetails.keys.contains('gateways') ? userDetails['gateways'] : [];
    phoneNumber = userDetails.keys.contains('phoneNumber')
        ? userDetails['phoneNumber']
        : '';
  }

  Tuple2<Gateway, bool> addProfileDataToGateway(Map profileData) {
    String gateWayMacId = profileData.keys.contains('gateWayMacId')
        ? profileData['gateWayMacId']
        : '';

    bool isAquabot = profileData.keys.contains('isAquabot')
        ? profileData['isAquabot']
        : false;
    // if (isAquabot == false) {
    // return Tuple2<Gateway, bool>(null, false);
    //  }

    Map aquaBotList = profileData.keys.contains('aquaBotList')
        ? profileData['aquaBotList']
        : {};
    if (aquaBotList == null && aquaBotList.length <= 0) {
      return Tuple2<Gateway, bool>(null, false);
    }

    if (gatewaysMap == null) {
      // gatewaysMap = {};
      gatewaysMap = SplayTreeMap<String, Gateway>(); //{};
    }
    Gateway gWay = null;
    //Gateway Object
    if (gateWayMacId != null && gateWayMacId.length > 0) {
      if (gatewaysMap.keys.contains(gateWayMacId)) {
        //  //print(gatewaysMap.values);
        gWay = gatewaysMap[gateWayMacId];
        gWay.updateProfileDetails(profileData);
      } else {
        //  //print(gatewaysMap.values);
        gWay = Gateway.updateProfileDetails(profileData);
        gatewaysMap[gateWayMacId] = gWay;
      }
    }
    refreshUI(gWay);
    return Tuple2<Gateway, bool>(gWay, true);
  }

  getFeedTotalFromFirebase(data) {
    if (data['af_calibration_data'] != null) {
      Map afData = data['af_calibration_data'];
      List localFeedData = [];
      afData.entries.map((entry) => localFeedData.add(entry.value)).toList();

      feedData = localFeedData;

      for (var i = 0; i < feedData.length; i++) {
        if (feedData[i]['todayFeed'] != null) {
          todayFeedTotal +=
              double.parse("${(feedData[i]['todayFeed']).toStringAsFixed(2)}");
        }
        if (feedData[i]['totalFeed'] != null) {
          totalFeedTotal +=
              double.parse("${(feedData[i]['totalFeed']).toStringAsFixed(2)}");
        }
      }
    }
  }

  addLiveDataToGateway(Map liveData) {
    String gateWayMacId =
        liveData.keys.contains('gtwayMACid') ? liveData['gtwayMACid'] : '';
    if (gateWayMacId == '') {}
    if (gatewaysMap == null) {
      // gatewaysMap = {};
      gatewaysMap = SplayTreeMap<String, Gateway>(); //{};
    }
    Gateway gWay = null;
    //Gateway Object
    if (gateWayMacId != null && gateWayMacId.length > 0) {
      if (gatewaysMap.keys.contains(gateWayMacId)) {
        gWay = gatewaysMap[gateWayMacId];
        gWay.updateLiveDetails(liveData);
      } else {
        gWay = Gateway.updateLiveDetails(liveData);
        gatewaysMap[gateWayMacId] = gWay;
      }
    }
    refreshUI(gWay);
    return Tuple2<Gateway, bool>(gWay, true);
  }

  refreshUI(Gateway gWay) {
    if (gWay.liveData != null && gWay.profileData != null) {
      homeBloc.refreshHomeList();
    }
    if (this.selGateway == gWay) {
      homeBloc.refreshBotDetailsView();
    }
  }

  releseAllData() {
    address = null;
    userName = null;
    idStr = null;
    userRole = null;
    gateways = null;
    userData = null;
    phoneNumber = null;
    gatewaysMap = null;
    selGateway = null;
  }
}
