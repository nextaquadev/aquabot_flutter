class Gateway {
        String gatewayMacId, gatewaySiteName, farmName, farmerSiteId, layoutJson, lutMsg = " ", starterMotorFaultMsg = " ", currentLivePktType = " ", siteName, pfFaultMsg = " ", generatorFualtMsg = " ", linemonRefValFaultMsg = " ", starterPhaseFaultsMsg = " ", nrmlStartersMsg = " ", gcloud_id = " ";
        List linemonV2Devices, linemonV3Devices, aquabotlist, feeds, starterDevices;
        int lut = 0, linemonTotalVoltage = 0, linemonPfFaultCount = 0, linemonCountRunningOnGenerator = 0, linemonsCountWhichAreLessthanRefVal = 0, linemonsTotalCurrnet = 0, totalPfFaults = 0, totalRefValFaults = 0, totalGenFaults = 0, staterCount, starterRunningNormalCount = 0, starterRunningWithFaultCount = 0, numberOfStartersOn = 0, numberOfflineDevices = 0, numberOfMotorFaults = 0;
      Gateway(gatewayMacId, gatewaySiteName, linemonV2Devices, linemonV3Devices, starterDevices, farmerSiteId, layoutJson, siteName, aquabotlist, feeds) {
        this.gatewayMacId = gatewayMacId;
        this.gatewaySiteName = gatewaySiteName;
        this.linemonV2Devices = linemonV2Devices;
        this.linemonV3Devices = linemonV3Devices;
        this.farmName = gatewaySiteName;
        this.aquabotlist = aquabotlist;
        this.feeds = feeds;
        this.starterDevices = starterDevices;
        this.farmerSiteId = farmerSiteId;
        this.layoutJson = layoutJson;
        this.staterCount = this.starterDevices.length;
      }
    

    setGcloudId(gcloud_id) {
        gcloud_id = gcloud_id;
    }

    setLastUpdatedTime(lut) {
        lut = lut;
    }

    setCurrentLivePktType(pktType) {
        currentLivePktType = pktType;
    }


    getOfflineMessage() {
        return numberOfflineDevices.toString() + " devices offline";
    }

}