import 'package:google_maps_flutter/google_maps_flutter.dart';

class BotMarker {
  double latitude;
  double longitude;
  LatLng latlang;

  BotMarker.toJson(Map route) {
    try {
      if (route != null) {
        this.longitude =
            route.keys.contains('longitude') && route['longitude'] != 0
                ? route['longitude']
                : 0.0;
        this.latitude =
            route.keys.contains('latitude') && route['latitude'] != 0
                ? route['latitude']
                : 0.0;
        latlang = LatLng(this.latitude, this.longitude);
      }
    } catch (e) {
      // //print(e);
    }
  }
  BotMarker.toMJson(Map route) {
    try {
      if (route != null) {
        this.longitude = route.keys.contains('m_lng')
            ? double.parse(route['m_lng'].toString())
            : 0.0;
        this.latitude = route.keys.contains('m_lat')
            ? double.parse(route['m_lat'].toString())
            : 0.0;
        latlang = LatLng(this.latitude, this.longitude);
      }
    } catch (e) {
      //print('BotMarker' + e);
    }
  }

  BotMarker.toLocation(LatLng route) {
    try {
      if (route != null) {
        this.latitude = route.latitude;
        this.longitude = route.longitude;
        latlang = route;
      }
    } catch (e) {
      //  //print('BotMarker' + e);
    }
  }
}
