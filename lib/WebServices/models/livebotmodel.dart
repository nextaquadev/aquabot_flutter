import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LiveBotModel {
  String name;
  String deviceId;
  String version;
  var epochTime;
  var voltage;
  var tripId;
  var weight_to_disburse;
  var current_Weight;
  String deviceType;
  String siteId;
  String gpsLong = '';
  String gpsLat = '';
  List<LatLng> liveLatLongs = [];
  List<LatLng> notliveLatLongs = [];

  var tripStatus;
  String distanceBetweenPosts = '0';
  String distanceFrmPost = '0';
  var currentPost;
  var rudderMotorSpeed;
  var bearingBtnStations;
  var botHeading;
  var cog;
  var sog;
  var m_fCurrent;
  var accuracy;
  var tripTime;
  var feederStatus;
  var distanceStep;
  var rudderDecision;
  var rudderMotorDirection;
  var desired_sog;
  var nextTripTime;
  var waitTimeCounter;
  var spinCounter;

  var isInside;
  var perpHeight;
  var etestState;

  var driveMotorSpeed;
  var driveForwardMax;
  var driveBackwardMax;
  var rudderSpeedMax;

  LiveBotModel.toJson(Map devicedata) {
    updateLiveData(devicedata);
  }

  updateLiveData(Map devicedata) {
    this.version =
        devicedata.keys.contains('FW_Version') ? devicedata['FW_Version'] : '';
    this.epochTime =
        devicedata.keys.contains('epochTime') ? devicedata['epochTime'] : '';
    this.voltage =
        devicedata.keys.contains('m_fVoltage') ? devicedata['m_fVoltage'] : '';
    this.tripId =
        devicedata.keys.contains('tripID') ? devicedata['tripID'] : '';
    this.feederStatus = devicedata.keys.contains('feederStatus')
        ? devicedata['feederStatus']
        : '';

    this.weight_to_disburse = devicedata.keys.contains('weightToDisburse')
        ? devicedata['weightToDisburse']
        : '';
    this.current_Weight = devicedata.keys.contains('currentWeight')
        ? devicedata['currentWeight']
        : '';
    this.driveMotorSpeed = devicedata.keys.contains('driveMotorSpeed')
        ? devicedata['driveMotorSpeed']
        : '';
    this.spinCounter = devicedata.keys.contains('spinCounter')
        ? devicedata['spinCounter']
        : '';
    this.distanceStep = devicedata.keys.contains('distanceStep')
        ? devicedata['distanceStep']
        : '';
    this.desired_sog =
        devicedata.keys.contains('desiredSOG') ? devicedata['desiredSOG'] : '';
    this.rudderMotorSpeed = devicedata.keys.contains('rudderMotorSpeed')
        ? devicedata['rudderMotorSpeed']
        : '';
    this.bearingBtnStations = devicedata.keys.contains('bearingBtnStations')
        ? devicedata['bearingBtnStations']
        : '';
    this.m_fCurrent =
        devicedata.keys.contains('m_fCurrent') ? devicedata['m_fCurrent'] : '';
    this.tripTime =
        devicedata.keys.contains('tripTime') ? devicedata['tripTime'] : '';
    this.nextTripTime = devicedata.keys.contains('nextTripTime')
        ? devicedata['nextTripTime']
        : '';
    this.etestState =
        devicedata.keys.contains('etestState') ? devicedata['etestState'] : '';

    this.botHeading =
        devicedata.keys.contains('botHeading') ? devicedata['botHeading'] : '';
    this.cog = devicedata.keys.contains('cog') ? devicedata['cog'] : '';
    this.accuracy =
        devicedata.keys.contains('accuracy') ? devicedata['accuracy'] : '';
    this.sog = devicedata.keys.contains('sog') ? devicedata['sog'] : '';
    this.rudderMotorDirection = devicedata.keys.contains('rudderMotorDirection')
        ? devicedata['rudderMotorDirection']
        : '';
    this.rudderDecision = devicedata.keys.contains('rudderDecision')
        ? devicedata['rudderDecision']
        : '';
    this.isInside =
        devicedata.keys.contains('isInside') ? devicedata['isInside'] : '';
    this.perpHeight =
        devicedata.keys.contains('perpHeight') ? devicedata['perpHeight'] : '';

    this.name = devicedata.keys.contains('Name') ? devicedata['Name'] : '';
    this.deviceId =
        devicedata.keys.contains('deviceId') ? devicedata['deviceId'] : '';
    this.deviceType =
        devicedata.keys.contains('deviceType') ? devicedata['deviceType'] : '';
    this.siteId =
        devicedata.keys.contains('siteId') ? devicedata['siteId'] : '';
    this.waitTimeCounter = devicedata.keys.contains('waitTimeCounter')
        ? devicedata['waitTimeCounter']
        : '';

    this.gpsLong =
        devicedata.keys.contains('gpsLong') ? devicedata['gpsLong'] : '';
    this.gpsLat =
        devicedata.keys.contains('gpsLat') ? devicedata['gpsLat'] : '';
    if (this.gpsLat == '0.00000000' || this.gpsLong == '0.00000000') {
    } else {
      // //print(this.gpsLat + ' ' + this.gpsLong);
      liveLatLongs
          .add(LatLng(double.parse(this.gpsLat), double.parse(this.gpsLong)));

      /*  this.tripStatus == 3 || this.tripStatus == 6
          ? liveLatLongs.add(
              LatLng(double.parse(this.gpsLat), double.parse(this.gpsLong)))
          : notliveLatLongs.add(
              LatLng(double.parse(this.gpsLat), double.parse(this.gpsLong)));*/
    }
    this.tripStatus =
        devicedata.keys.contains('tripStatus') ? devicedata['tripStatus'] : 0;
    this.distanceBetweenPosts = devicedata.keys.contains('distanceBetweenPosts')
        ? devicedata['distanceBetweenPosts'].toString()
        : '';
    this.distanceFrmPost = devicedata.keys.contains('distanceFrmPost')
        ? devicedata['distanceFrmPost'].toString()
        : '0';
    this.currentPost = devicedata.keys.contains('currentPost')
        ? devicedata['currentPost']
        : '';
    this.driveForwardMax = devicedata.keys.contains('driveForwardMax')
        ? devicedata['driveForwardMax']
        : 0;
    this.driveBackwardMax = devicedata.keys.contains('driveBackwardMax')
        ? devicedata['driveBackwardMax']
        : 0;
    this.rudderSpeedMax = devicedata.keys.contains('rudderSpeedMax')
        ? devicedata['rudderSpeedMax']
        : 0.0;
  }
}
