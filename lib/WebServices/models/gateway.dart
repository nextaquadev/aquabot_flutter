import 'package:aquabot/WebServices/models/botmodel.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:flutter/material.dart';

class Gateway {
  String gateWayMacId;
  String farmName;
  Map aquaBotList;
  bool isSlot;
  bool isWifi;
  bool isAutofeeder;
  bool isBot;
  String gpsLocation;
  bool isHarvest;
  List<BotModel> botDevices = null;

  Map<String, LiveBotModel> liveDeviceMap = {};

  Map profileData = null;
  Map liveData = null;
  var feeds;

  Gateway.updateProfileDetails(Map profileData) {
    updateProfileDetails(profileData);
  }

  Gateway.updateLiveDetails(Map liveData) {
    updateLiveDetails(liveData);
  }

  updateProfileDetails(Map profileData) {
    this.profileData = profileData;
    this.gateWayMacId = profileData.keys.contains('gateWayMacId')
        ? profileData['gateWayMacId']
        : '';
    this.farmName =
        profileData.keys.contains('farmName') ? profileData['farmName'] : '';

    this.aquaBotList = profileData.keys.contains('aquaBotList')
        ? profileData['aquaBotList']
        : {};

    this.isSlot =
        profileData.keys.contains('isSlot') ? profileData['isSlot'] : false;

    this.isWifi =
        profileData.keys.contains('isWifi') ? profileData['isWifi'] : false;
    this.feeds = profileData.keys.contains('feeds') ? profileData['feeds'] : [];

    this.isAutofeeder = profileData.keys.contains('isAutofeeder')
        ? profileData['isAutofeeder']
        : false;

    this.isBot =
        profileData.keys.contains('isBot') ? profileData['isBot'] : false;
    
    this.isBot =
        profileData.keys.contains('isHarvest') ? profileData['isHarvest'] : false;

    this.gpsLocation = profileData.keys.contains('gpsLocation')
        ? profileData['gpsLocation']
        : '';

    this.botDevices = this
        .aquaBotList
        .entries
        .map((entry) => BotModel.toJson(entry.value))
        .toList();
  }

  updateLiveDetails(Map liveData) {
    this.liveData = liveData;

    Map devicesList =
        liveData.keys.contains('devicesList') ? liveData['devicesList'] : {};

    //Gateway Object
    if (devicesList != null && devicesList.length > 0) {
      for (int i = 0; i < devicesList.keys.length; i++) {
        String keyStr = devicesList.keys.elementAt(i);
        LiveBotModel lModel = null;
        if (liveDeviceMap.keys.contains(keyStr)) {
          lModel = liveDeviceMap[keyStr];
          lModel.updateLiveData(devicesList[keyStr]);
        } else {
          lModel = LiveBotModel.toJson(devicesList[keyStr]);
          liveDeviceMap[keyStr] = lModel;
        }
        if (lModel != null) updateLiveDataToBotDevice(lModel);
      }
    }
  }

  updateLiveDataToBotDevice(LiveBotModel lModel) {
    for (int i = 0; i < botDevices.length; i++) {
      BotModel element = botDevices.elementAt(i);
      if (element.macId == lModel.deviceId) {
        element.liveModel = lModel;
        break;
      }
    }
    ;
  }
}
