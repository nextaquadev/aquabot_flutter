//import 'dart:html';
import 'dart:io';

import 'package:excel/excel.dart';

import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart';

class Excel_sheet {
  static Future<void> createExcel(data) async {
    final Workbook workbook = Workbook();
    final Worksheet sheet = workbook.worksheets[0];
    /* var farmlist = [];
    var name = [];
    var last = [];
    var recharge = [];
    var expiry = [];
    data.map((i) {
      farmlist.add(i[1]);
      name.add(i[2]);
      last.add(i[3]);
      recharge.add(i[4]);
      expiry.add(i[5]);
    }).toList();
    var newlist = [];
    newlist.addAll([farmlist]);
    newlist.addAll([name]);
    newlist.addAll([last]);
    newlist.addAll([recharge]);
    newlist.addAll([expiry]);
    print(newlist);*/

    var i = 0;
    data.map((index) {
      i = i + 1;
      sheet.importList(index, i, 1, false);
    }).toList();

    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();
    final String path = (await getApplicationSupportDirectory()).path;
    final String fileName = '$path/Output.xlsx';
    final File file = File(fileName);
    await file.writeAsBytes(bytes, flush: true);
    OpenFile.open(fileName);
  }
}
