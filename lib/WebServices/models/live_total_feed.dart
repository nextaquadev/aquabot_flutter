class LiveTotalFeed {
  // var todayFeedTotal = 0.0;
  var feedData;
  var gatewaysFeedDetails = {};
  LiveTotalFeed._internal();
  static LiveTotalFeed _instance;

  static LiveTotalFeed getInstance() {
    if (_instance == null) {
      _instance = LiveTotalFeed._internal();
    }
    return _instance;
  }
  getFeedTotalFromFirebase(data, gatewayId){
    var todayFeedTotal = 0.0;
    var totalFeedTotal = [];
    if(data['af_calibration_data'] != null){
      Map afData = data['af_calibration_data'];
        List localFeedData = [];
        afData.entries.map((entry) => localFeedData.add(entry.value)).toList();
       

        feedData = localFeedData;
        
        for(var i = 0; i < feedData.length; i++){
            if(feedData[i]['todayFeed'] != null){
              todayFeedTotal += double.parse("${(feedData[i]['todayFeed']).toStringAsFixed(2)}");
            }
            if(feedData[i]['tripWiseFeedObjs'] != null){
                  totalFeedTotal.add((feedData[i]));
            }
            
        }
        gatewaysFeedDetails = {...gatewaysFeedDetails, "${"$gatewayId"}": {'todayFeed': todayFeedTotal, 'totalFeed': totalFeedTotal}};
    }
  }
}