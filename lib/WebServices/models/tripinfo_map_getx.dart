import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Trip_Map extends GetxController {
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{}.obs;
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{}.obs;
  addmarkers(key, value) {
    markers[key] = value;
  }

  addpolyline(key, value) {
    polylines[key] = value;
  }

  removemarkers(name) {
    markers.removeWhere((key, value) => key.value == name);
  }

  removepolyline(name) {
    polylines.removeWhere((key, value) => key.value == name);
  }

  clearAll() {
    markers.clear();
    polylines.clear();
  }
}
