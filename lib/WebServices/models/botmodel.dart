import 'dart:collection';

import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:aquabot/WebServices/models/livebotmodel.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sortedmap/sortedmap.dart';

class BotModel {
  String macId;
  bool haveSlots;
  String afVersion;
  String afType;
  String deviceName;
  double lat;
  double long;
  bool isExpand = false;
  bool isHarvested;
  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};

  // LatLng initialcameraposition = LatLng(17.448227813051307, 78.36215291172266);
  double zoomposition = 19;

  List<LatLng> livepoints = [];
  Map<String, dynamic> regions;
  List<BotMarker> routePoints;
  LiveBotModel liveModel;

  BotModel.toJson(Map devicedata) {
    this.macId = devicedata.keys.contains('macId') ? devicedata['macId'] : '';
    this.afVersion =
        devicedata.keys.contains('afVersion') ? devicedata['afVersion'] : '';
    this.afType =
        devicedata.keys.contains('afType') ? devicedata['afType'] : '';
    this.deviceName =
        devicedata.keys.contains('deviceName') ? devicedata['deviceName'] : '';
    this.lat = devicedata.keys.contains('lat') ? devicedata['lat'] : 0.0;
    this.long = devicedata.keys.contains('long') ? devicedata['long'] : 0.0;
    this.isHarvested = devicedata.keys.contains('isHarvested')
        ? devicedata['isHarvested']
        : false;

    this.regions =
        devicedata.keys.contains('regions') ? devicedata['regions'] : {};
    // //print(regions);

    if (this.regions != null && this.regions.keys.contains(this.macId)) {
      Map<String, dynamic> routeMap = this.regions[this.macId];
      /* var map = SortedMap(Ordering.byKey());
      map.addAll(routeMap);
      //print(map);*/

      var data = routeMap.keys.toList();
      List<int> lint = data.map(int.parse).toList()..sort();
      var newlist = [];
      lint.map((index) {
        var key = "$index";
        if (routeMap[key]['latitude'] != 0 ||
            routeMap[key]['latitude'] != 0.0) {
          newlist.add(routeMap[key]);
        }
      }).toList();

      this.routePoints = newlist.isNotEmpty
          ? newlist.map((entry) => BotMarker.toJson(entry)).toList()
          : [];

      /* this.routePoints = routeMap.isNotEmpty
          ? routeMap.entries
              .map((entry) => BotMarker.toJson(entry.value))
              .toList()
          : [];*/
      // //print(routePoints);
    }
    this.haveSlots =
        devicedata.keys.contains('haveSlots') ? devicedata['haveSlots'] : false;
  }
}
