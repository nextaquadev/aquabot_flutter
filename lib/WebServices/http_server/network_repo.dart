import 'package:aquabot/WebServices/http_server/http_server.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Network_Repo {
  HttpServer webServer = HttpServer();

  Future<LatLng> getCurrentLocation() => webServer.getUserLocation();
}
