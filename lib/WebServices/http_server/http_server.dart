import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

class HttpServer {
  Future<LatLng> getUserLocation() async {
    var position = await GeolocatorPlatform.instance
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    return LatLng(position.latitude, position.longitude);
  }

  // Future<List<HelpModel>> getHelpVideosJson() async {
  //   try {
  //     var videoJson = await http.get(Uri.parse(
  //         'https://liquid.semanoor.com/IJST/Liquid_Static/semanoorhelp.json'));
  //     String body = utf8.decode(videoJson.bodyBytes);

  //     return (json.decode(body)['Assets'] as List)
  //         .map((e) => HelpModel.fromJson(e))
  //         .toList();
  //   } catch (e) {
  //     throw Exception(e);
  //   }
  // }
}
