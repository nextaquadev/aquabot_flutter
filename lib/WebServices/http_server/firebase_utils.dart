import 'package:aquabot/WebServices/blocs/login_bloc.dart';
import 'package:aquabot/WebServices/models/aqua_user.dart';
import 'package:aquabot/WebServices/models/gateway.dart';
import 'package:aquabot/WebServices/models/live_total_feed.dart';
import 'package:aquabot/WebServices/models/phverify_model.dart';
import 'package:aquabot/screens/homescreen/gateway_details/bottom_sheets/alert.dart';
import 'package:aquabot/utils/aqua_utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tuple/tuple.dart';
import 'package:flutter/material.dart';

class FireBaseUtils {
  FirebaseAuth _auth = FirebaseAuth.instance;

/*
Firebase - phone number verification 
*/
  signInWithPhoneNumber(String phoneNumber) async {
    return _auth
        .signInWithPhoneNumber("+91" + phoneNumber)
        .then((confirmresult) {
      // //print(confirmresult.verificationId);
      PhVerifyModel model = PhVerifyModel(
          verificationId: confirmresult.verificationId, authException: null);
      loginBlock.fetchPhVerifyData(model);
    });
  }

  verifyNumber(String phoneNumber) async {
    await _auth.verifyPhoneNumber(
      phoneNumber: '+91' + phoneNumber,
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {
        if (e.code == 'invalid-phone-number') {
          // //print('The provided phone number is not valid.');
        }
        PhVerifyModel model =
            PhVerifyModel(verificationId: null, authException: e);
        loginBlock.fetchPhVerifyData(model);
      },
      codeSent: (String verificationId, int resendToken) {
        PhVerifyModel model =
            PhVerifyModel(verificationId: verificationId, authException: null);
        loginBlock.fetchPhVerifyData(model);
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  verifyOtp(String otp, PhVerifyModel model, context) {
    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
        verificationId: model.verificationId, smsCode: otp);
    checkUser(phoneAuthCredential, context);
  }

  checkUser(PhoneAuthCredential phoneAuthCredential, context) async {
    try {
      UserCredential authCredential =
          await _auth.signInWithCredential(phoneAuthCredential);
      loginBlock.updateUserCredentials(authCredential);
      // setState(() {
      //   showLoading = false;
      // });

      if (authCredential.user != null) {
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute(builder: (context) => HomeScreen()));
      }
    } catch (e) {
      message(e.code, context);

      // setState(() {
      //   showLoading = false;
      // });
    }
  }

  message(error, context) {
    return WidgetsBinding.instance.addPostFrameCallback((_) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return Alert("Wrong OTP entered", Colors.red, "Login Error");
          });
    });
  }

/*
Firebase - fetch user details 
*/
  fetchUserProfile() {
    if (_auth.currentUser == null) {
      //No user
    }
    var phoneNumber =
        AquaUtils.replaceText('+91', '', _auth.currentUser.phoneNumber);

    FirebaseFirestore.instance
        .collection('users')
        .doc(phoneNumber)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        // //print('Document data: ${documentSnapshot.data()}');
        AquaUser.getInstance().updateDetails(documentSnapshot.data());
        fetchGateways();
      }
    });
  }

  fetchGateways() {
    AquaUser user = AquaUser.getInstance();

    for (var gatewayId in user.gateways) {
      if (gatewayId != null) {
        fetchProfileData(gatewayId);
        // fetchLiveData(gatewayId);
      }
    }
  }

  fetchProfileData(gatewayId) async {
    AquaUser user = AquaUser.getInstance();

    DocumentReference dr = await FirebaseFirestore.instance
        .collection("gateways")
        .doc(gatewayId)
        .collection("info")
        .doc('profiledata');
    // dr.get().then((DocumentSnapshot documentSnapshot) async {
    dr.snapshots().listen((documentSnapshot) async {
      if (documentSnapshot.exists) {
        //  //print('Document data: ${documentSnapshot.data()}');
        Tuple2<Gateway, bool> gatewayStatus =
            user.addProfileDataToGateway(documentSnapshot.data());
        // if (gatewayStatus.item2 == true)

        Gateway gWay = await gatewayStatus.item1;
        fetchLiveData(gWay.gateWayMacId);
      }
    });
    // dr.snapshots().listen((querySnapshot) {
    //   //print(querySnapshot.data());
    // });
  }

  fetchLiveData(gatewayId) async {
    AquaUser user = AquaUser.getInstance();
    LiveTotalFeed liveFeedDetails = LiveTotalFeed.getInstance();

    ///movingfeeder/DEFAULTSITE/info/livedata
    DocumentReference dr = await FirebaseFirestore.instance
        .collection("movingfeeder")
        .doc(gatewayId)
        .collection("info")
        .doc('livedata');
    // dr.get().then((DocumentSnapshot documentSnapshot) async {
    // if (documentSnapshot.exists) {
    dr.snapshots().listen((documentSnapshot) async {
      //  //print("This is live data:${documentSnapshot.data()}");
      if (documentSnapshot.exists) {
        Tuple2<Gateway, bool> gatewayStatus =
            await user.addLiveDataToGateway(documentSnapshot.data());
      }
      // });
      // }
    });
    try {
      DocumentReference trip_info_data = await FirebaseFirestore.instance
          .collection("wifi_af_ver03_feeder_data")
          .doc("$gatewayId")
          .collection("info")
          .doc('livedata');
      trip_info_data.snapshots().listen((documentSnapshot) async {
        if (documentSnapshot.exists) {
          //print(documentSnapshot.data());
          liveFeedDetails.getFeedTotalFromFirebase(
              documentSnapshot.data(), gatewayId);
          //print(liveFeedDetails);
          ////print(documentSnapshot.data());
        }
      });
    } catch (error) {
      //print("No gateway");
    }

    // CollectionReference cr = FirebaseFirestore.instance
    //     .collection('movingfeeder')
    //     .doc(gatewayId)
    //     .collection('data');
    // Query qr = cr.orderBy('epochTime', descending: true).limit(20);
    // qr.snapshots().listen((querySnapshot) {
    //   querySnapshot.docs.forEach((element) {
    //     Tuple2<Gateway, bool> gatewayStatus =
    //         user.addLivesDataToGateway(element.data());
    //   });
    // });
  }
}
