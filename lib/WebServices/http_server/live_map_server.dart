import 'package:aquabot/WebServices/models/botmarker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Live_server {
  fetch_live_map(tripid, deviceid, gatewayid) async {
    //  //print("This is live map tripid:$tripid");
    final url = "http://34.69.73.157:9242/aquabot/_search";

    var client = http.Client();

    Map<String, String> headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    var body = jsonEncode({
      "sort": [
        {
          "epochTime": {"order": "desc"}
        }
      ],
      "size": 2000,
      "query": {
        "bool": {
          "must": [
            {
              "match_phrase": {"tripID": "$tripid"}
            },
            {
              "match_phrase": {"deviceId": "$deviceid"}
            },
            {
              "match_phrase": {"siteId": "$gatewayid"}
            },
            {
              "match_phrase": {"tripStatus": "3"}
            }
          ]
        }
      }
    });
    var points_data;

    try {
      var response =
          await client.post(Uri.parse(url), body: body, headers: headers);

      if (response != null) {
        var data = jsonDecode(response.body);
        //print(data['hits']);
        points_data = data["hits"];
        // preparePath(data["hits"]);
      }
    } catch (e) {}
    return points_data;
  }

  /* preparePath(data) {
    // //print(data);
    //  //print(data.hits);
    if (data["hits"].length > 0) {
      List<LatLng> pathsArr = [];
      for (var i = 0; i < data["hits"].length - 1; i++) {
        var val = data["hits"][i]['_source'];
        // //print(val);

        var path = {
          'latitude': val['gpsLat1'],
          'longitude': val['gpsLong1'],
        };
        pathsArr.add(LatLng(
            double.parse(path['latitude']), double.parse(path['longitude'])));
        //print(pathsArr);
      }
      
    }
  }*/
}
