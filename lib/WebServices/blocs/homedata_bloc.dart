import 'package:aquabot/WebServices/http_server/network_repo.dart';
import 'package:aquabot/WebServices/models/gateway.dart';
import 'package:aquabot/WebServices/models/phverify_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rxdart/subjects.dart';

class HomeBloc {
  final networkRepo = Network_Repo();

  final _homeRefreshFetcher = PublishSubject<bool>();
  Stream<bool> get homeRefreshStream => _homeRefreshFetcher.stream;

  final _locationFetcher = PublishSubject<LatLng>();
  Stream<LatLng> get locationRefreshStream => _locationFetcher.stream;

  final _botDetailsRefreshFetcher = PublishSubject<bool>();
  Stream<bool> get botDetailsRefreshStream => _botDetailsRefreshFetcher.stream;

  refreshHomeList() async {
    _homeRefreshFetcher.add(true);
  }

  refreshBotDetailsView() async {
    _botDetailsRefreshFetcher.add(true);
  }

  refreshCurrentLocation() async {
    LatLng location = await networkRepo.getCurrentLocation();
    _locationFetcher.add(location);
  }

  dispose() {
    _homeRefreshFetcher.close();
    _locationFetcher.close();
    _botDetailsRefreshFetcher.close();
  }
}

final homeBloc = HomeBloc();
