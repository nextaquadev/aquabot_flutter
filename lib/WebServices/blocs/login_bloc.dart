import 'package:aquabot/WebServices/http_server/network_repo.dart';
import 'package:aquabot/WebServices/models/phverify_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/subjects.dart';

class LoginBlock {
  final networkRepo = Network_Repo();

  final _phVerifyFetcher = PublishSubject<PhVerifyModel>();
  Stream<PhVerifyModel> get phVerifyStream => _phVerifyFetcher.stream;

  final _userCredFetcher = PublishSubject<UserCredential>();
  Stream<UserCredential> get userCredStream => _userCredFetcher.stream;

  fetchPhVerifyData(PhVerifyModel model) async {
    _phVerifyFetcher.add(model);
  }

  updateUserCredentials(UserCredential userCred) async {
    _userCredFetcher.add(userCred);
  }

  dispose() {
    _phVerifyFetcher.close();
    _userCredFetcher.close();
  }
}

final loginBlock = LoginBlock();
