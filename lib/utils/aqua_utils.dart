import 'dart:ui';
import 'dart:math';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AquaUtils {
  Random _rnd = Random();

  static String replaceText(@required String find, @required String replaceWith,
      @required String text) {
    return text.replaceAll(find, replaceWith);
  }

  static Color getColorForIndex(int index) {
    switch (index % 6) {
      case 0:
        return Color(0xFFF6BB42);
        break;
      case 1:
        return Color(0xFF4A89DC);
        break;
      case 2:
        return Color(0xFF967ADC);
        break;
      case 3:
        return Color(0xFF3BAFDA);
        break;
      case 4:
        return Color(0xFF37BC9B);
        break;
      case 5:
        return Color(0xFF8CC152);
        break;
      default:
        return Color(0xFFF6BB42);
        break;
    }
  }

  static const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<BitmapDescriptor> getClusterMarker(
      final title, Color textColor, double radius, Color clusterColor
      /* int clusterSize,
    int width,*/
      ) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Paint paint = Paint()
      ..color = clusterColor
      ..strokeWidth = 3
      ..style = PaintingStyle.stroke;
    final TextPainter textPainter = TextPainter(
      textDirection: TextDirection.ltr,
    );
    // final double radius = width / 2;
    /*  canvas.drawCircle(
        Offset(
          radius - textPainter.width / 2,
          radius - textPainter.height / 2,
        ),
        60,
        paint);*/
    kIsWeb
        ? canvas.drawCircle(
            Offset(
              20,
              20,
            ),
            10,
            paint,
          )
        : canvas.drawCircle(
            Offset(
              radius,
              radius,
            ),
            15,
            paint,
          );

    textPainter.text = TextSpan(
      text: title,
      style: TextStyle(
        fontSize: 25,
        fontWeight: FontWeight.bold,
        color: textColor,
      ),
    );
    /* canvas.drawCircle(
        Offset(
          radius - textPainter.width / 2,
          radius - textPainter.height / 2,
        ),
        radius,
        Paint());*/
    textPainter.layout();
    textPainter.paint(
      canvas,
      Offset(
        radius - textPainter.width / 2,
        radius - textPainter.height / 2,
      ),
    );
    /*  final image = await pictureRecorder.endRecording().toImage(
          radius.toInt() * 2,
          radius.toInt() * 2,
        );
    final data = await image.toByteData(format: ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());*/
    final image = await pictureRecorder.endRecording().toImage(
          radius.toInt() * 2,
          radius.toInt() * 2,
        );

    final data = await image.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

/*  Future<Uint8List> getBytesFromCanvas(
  int width, int height, Uint8List dataBytes) async {
final PictureRecorder pictureRecorder = PictureRecorder();
final Canvas canvas = Canvas(pictureRecorder);
final Paint paint = Paint()..color = Colors.transparent;
final Radius radius = Radius.circular(20.0);
canvas.drawRRect(
    RRect.fromRectAndCorners(
      Rect.fromLTWH(0.0, 0.0, width.toDouble(), height.toDouble()),
      topLeft: radius,
      topRight: radius,
      bottomLeft: radius,
      bottomRight: radius,
    ),
    paint);

var imaged = await loadImage(dataBytes.buffer.asUint8List());
canvas.drawImageRect(imaged,
  Rect.fromLTRB(
      0.0, 0.0, imaged.width.toDouble(), imaged.height.toDouble()),
  Rect.fromLTRB(0.0, 0.0, width.toDouble(), height.toDouble()),
  new Paint(),
);

    final img = await pictureRecorder.endRecording().toImage(width, height);
    final data = await img.toByteData(format: ImageByteFormat.png);
    return data.buffer.asUint8List();
 }

    Future<Image> loadImage(List<int> img) async {
    final Completer<Image> completer = new Completer();
    decodeImageFromList(img, (Image img) {
  return completer.complete(img);
});
return completer.future;
}*/

  Future<BitmapDescriptor> getClusterMarkerName(
      final title, Color textColor, double radius
      /* int clusterSize,
    int width,*/
      ) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    //  final Paint paint = Paint()..color = clusterColor;
    final TextPainter textPainter = TextPainter(
      textDirection: TextDirection.ltr,
    );
    // final double radius = width / 2;
    /*  canvas.drawCircle(
        Offset(
          radius - textPainter.width / 2,
          radius - textPainter.height / 2,
        ),
        60,
        paint);*/
    /*  canvas.drawCircle(
      Offset(
        radius,
        radius,
      ),
      20,
      paint,
    );*/

    textPainter.text = TextSpan(
      text: title,
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.bold,
        color: textColor,
      ),
    );
    /* canvas.drawCircle(
        Offset(
          radius - textPainter.width / 2,
          radius - textPainter.height / 2,
        ),
        radius,
        Paint());*/
    textPainter.layout();
    textPainter.paint(
      canvas,
      Offset(
        radius - textPainter.width / 2,
        radius - textPainter.height / 2,
      ),
    );

    /*  final image = await pictureRecorder.endRecording().toImage(
          radius.toInt() * 2,
          radius.toInt() * 2,
        );
    final data = await image.toByteData(format: ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());*/
    final image = await pictureRecorder.endRecording().toImage(
          radius.toInt() * 2,
          radius.toInt() * 2,
        );

    final data = await image.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  LatLng computeCentroid(List<LatLng> points) {
    double latitude = 0;
    double longitude = 0;
    int n = points.length;

    for (LatLng point in points) {
      latitude += point.latitude;
      longitude += point.longitude;
    }

    return new LatLng(latitude / n, longitude / n);
  }
}
